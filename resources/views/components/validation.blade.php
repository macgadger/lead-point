@if($errors->count())
    @foreach($errors->all() as $message)
        <div class="alert alert-solid alert-warning" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>{{ $message }}</strong>
        </div><!-- alert -->
    @endforeach
@endif