<div class="col-md-4">
    <div id="slWrapperCountry" class="parsley-select">
        <div class="form-group">
            <label class="form-control-label">Country: <span class="text-danger">*</span></label>
            <select class="form-control select2" name="country"
                data-parsley-class-handler="#slWrapperCountry"
                data-parsley-errors-container="#slErrorContainerCountry"
                onchange="getLocations('state',this.value)"
                required>
                    <option value=""> ---Select Country--- </option>
                    @foreach($countries as $country)
                        @if($model != null)
                            <option @if(ucfirst($model['country']) == $country) selected @endif value="{{ $country }}">{{ $country }}</option>
                        @else
                            <option value="{{ $country }}" @if($country == 'India') selected @endif >{{ $country }}</option>
                        @endif
                    @endforeach
            </select>
            <div id="slErrorContainerCountry"></div>
        </div>
    </div><!-- col-4 -->
</div>

<div class="col-md-4">
    <div id="slWrapperState" class="parsley-select">
        <div class="form-group">
            <label class="form-control-label">State: <span class="text-danger">*</span></label>
            <select class="form-control select2" 
                name="state"
                id="state-dropdown"
                data-parsley-class-handler="#slWrapperState"
                data-parsley-errors-container="#slErrorContainerState"
                {{-- onchange="getLocations('city',this.value)" --}}
                required>
                @foreach($states as $state)
                    @if($model != null)
                        <option value="{{ $state }}" @if( ucfirst($model['state']) == $state) selected @endif >{{ $state }}</option>
                    @else
                        <option value="{{ $state }}" @if($state == 'Punjab') selected @endif >{{ $state }}</option>
                    @endif
                @endforeach
            </select>
            <div id="slErrorContainerState"></div>
        </div>
    </div><!-- col-4 -->
</div>

 <div class="col-md-4">
    <div class="form-group">
        <label class="form-control-label">City: <span class="text-danger">*</span></label>
        @if($model != null)
            <input class="form-control" type="text" name="city" style="text-transform:capitalize" value="{{ $model['city'] }}"  placeholder="Enter City" required>
        @else
            <input class="form-control" type="text" name="city" style="text-transform:capitalize" value="{{ old('city') }}"  placeholder="Enter City" required>
        @endif
    </div>
</div>

<script>
    function getLocations(type, value){
        $('#preloader').show();
        //
        if(type == 'state'){
            let state_dropdown = $('#state-dropdown');
            state_dropdown.empty();
            state_dropdown.append('<option selected="true" disabled>Choose State/Province</option>');
            state_dropdown.prop('selectedIndex', 0);
            const url = '/app/admin/get-location/state/'+value;

            $.getJSON(url, function(result)
            {
                $.each(result, function (key, entry) {
                    state_dropdown.append($('<option></option>').attr('value', entry.state_name).text(entry.state_name));
                })
                $('#preloader').hide();
            });
        }
        if(type == 'city'){
            let city_dropdown = $('#city-dropdown');
            city_dropdown.empty();
            city_dropdown.append('<option selected="true" disabled>Choose City</option>');
            city_dropdown.prop('selectedIndex', 0);
            const url = '/app/admin/get-location/city/'+value;
            
            $.getJSON(url, function(result)
            {
                $.each(result, function (key, entry) {
                    city_dropdown.append($('<option></option>').attr('value', entry.city_name).text(entry.city_name));
                })
                $('#preloader').hide();
            });
        }
    }
</script>