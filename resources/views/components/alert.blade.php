@if(session('success'))
    <div class="alert alert-solid alert-success mt-20" role="alert">
        <strong>{{ session('success') }}</strong>
    </div><!-- alert -->
@endif
@if(session('error'))
    <div class="alert alert-solid alert-danger  mt-20" role="alert">
        <strong>{{ session('error') }}</strong>
    </div><!-- alert -->
@endif

