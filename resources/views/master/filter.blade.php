@extends('layouts.app')
@section('title', 'List')
@section('content')
<div class="card-body">
    <x-alert />
        <div class="table-wrapper">
        @if($type == 'trials')
        <table id="datatable" class="table table-centered table-hover dt-responsive nowrap table-card-list" style="border-collapse: collapse; border-spacing: 0 12px; width: 100%;">
            <thead>
                <tr class="bg-transparent">
                    <th class="wd-5p">#</th>
                    <th class="wd-15p">Name</th>
                    <th class="wd-15p">Phone</th>
                    <th class="wd-10p">Email</th>                    
                    <th class="wd-15p">created at</th>
                    <th class="wd-10p">Status</th>                    
                </tr>
            </thead>
            <tbody>
            @if(isset($clients) && $clients->isNotEmpty())
                @foreach($clients as $key => $client)
                <tr>
                    <td>{{ $key+1 }}</td>
                    <td>{{ $client->name }}</td>
                    <td>{{ $client->phone }}</td>                    
                    <td style="text-transform: lowercase !important">{{ $client->email }}</td>                    
                    <td>                        
                        {{ date('M d, Y', strtotime($client->created_at))}}
                    </td>
                    <td>
                        @if($client->status == 1)
                            <span class="btn btn-success btn-sm">Active</span>
                        @else
                            <span class="btn btn-danger btn-sm">Suspended</span>
                        @endif
                    </td>                    
                </tr>
                @endforeach
            @endif
            </tbody>
        </table>
        @else
        <table id="datatable" class="table table-centered table-hover dt-responsive nowrap table-card-list" style="border-collapse: collapse; border-spacing: 0 12px; width: 100%;">
            <thead>
                <tr class="bg-transparent">
                    <th class="wd-5p">#</th>
                    <th class="wd-15p">CompanyID</th>
                    <th class="wd-15p">Company Name</th>
                    <th class="wd-15p">Username</th>
                    <th class="wd-10p">Email</th>
                    <th class="wd-10p">Current Plan</th>
                    <th class="wd-15p">Expiring On</th>
                    <th class="wd-10p">Status</th>
                    <th class="wd-20p">Actions</th>
                </tr>
            </thead>
            <tbody>
            @if(isset($clients) && $clients->isNotEmpty())
                @foreach($clients as $key => $client)
                <tr>
                    <td>{{ $key+1 }}</td>
                    <td>{{ $client->ucid }}</td>
                    <td>{{ $client->company_name }}</td>
                    <td>{{ $client->email }}</td>
                    <td style="text-transform: lowercase !important">{{ $client->email }}</td>
                    <td><span class="badge badge-warning">{{ strtoupper($client->plan->name) }}</span></td>
                    <td>
                        {{ \Carbon\Carbon::parse($client->plan_expiry)->diffForHumans() }}
                        on
                        {{ date('M d, Y', strtotime($client->plan_expiry))}}
                    </td>
                    <td>
                        @if($client->status == 1)
                            <span class="btn btn-success btn-sm">Active</span>
                        @else
                            <span class="btn btn-danger btn-sm">Suspended</span>
                        @endif
                    </td>
                    <td>
                        <div class="btn-group" role="group" aria-label="Controls">
                            <a href="{{ route('client.edit',$client->uid) }}" class="px-2 text-primary" data-toggle="tooltip" data-placement="bottom"
                                title="Edit Details"><i class="uil uil-edit-alt"></i></a>                          
                            <a href="#" class="px-2 text-danger" data-toggle="tooltip" data-placement="bottom"
                                title="Delete User"><i class="uil uil-trash-alt"></i></a>
                        </div>
                    </td>
                </tr>
                @endforeach
            @endif
            </tbody>
        </table>
        @endif
    </div>
</div>
@endsection
@push('scripts')
<script>
    $(function(){
        $('#datatable').DataTable({
        responsive : true,
        sDom : 'Rfrtlip',
        language: {
            searchPlaceholder: 'Search...',
            sSearch: '',
            lengthMenu: '_MENU_ items/page',
        },
            'columnDefs': [ {
            'targets'  : [2,6], /* column index */
            'orderable': false, /* true or false */
        }],
        fixedColumns: true
        });
    });
</script>
@endpush