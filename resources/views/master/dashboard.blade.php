@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="page-title-box">
            <h4 class="mg-b-5">Welcome back, {{ ucwords(Auth::user()->name) }} !</h4>
            <p class="mg-b-0">Today is {{ date('M d, Y') }}</p>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="card">
            <div class="card-body">
                <h5 class="prod-name"><a href="javascript:void(0)">Clients</a></h5>
                <div class="row">
                    <div class="col-5">
                        <h1 class="mb-1 mt-1">{{ $analytics->clients->total ?? '0' }}</h1>
                        <p class="text-muted mb-0">Total</p>
                    </div>
                    <div class="col-7">
                        <h1 class="mb-1 mt-1">{{ $analytics->clients->today ?? '0' }}</h1>
                        <p class="text-muted mb-0">Today</p>
                    </div>
                </div>
                <div class="mt-2 text-right">
                    <a href="{{ route('master.filter','clients') }}" class="text-primary font-size-14 font-weight-medium">View Details <i class="uil uil-angle-double-right"></i></a>
                </div>
            </div>
        </div>
    </div> <!-- end col-->

    <div class="col-md-4">
        <div class="card">
            <div class="card-body">
                <h5 class="prod-name"><a href="javascript:void(0)">Trial Requests</a></h5>
                <div class="row">
                    <div class="col-5">
                        <h1 class="mb-1 mt-1">{{ $analytics->requests->today ?? '0' }}</h1>
                        <p class="text-muted mb-0">Today</p>
                    </div>
                    <div class="col-7">
                        <h1 class="mb-1 mt-1">{{ $analytics->requests->month ?? '0' }}</h1>
                        <p class="text-muted mb-0">Month</p>
                    </div>
                </div>
                <div class="mt-2 text-right">
                    <a href="{{ route('master.filter','trials') }}" class="text-primary font-size-14 font-weight-medium">View Details <i class="uil uil-angle-double-right"></i></a>
                </div>
            </div>
        </div>
    </div> <!-- end col-->

    <div class="col-md-4">
        <div class="card">
            <div class="card-body">
                <h5 class="prod-name"><a href="javascript:void(0)">Plans Expiring</a></h5>
                <div class="row">
                    <div class="col-5">
                        <h1 class="mb-1 mt-1">{{ $analytics->plans->today ?? '0' }}</h1>
                        <p class="text-muted mb-0">Today</p>
                    </div>
                    <div class="col-7">
                        <h1 class="mb-1 mt-1">{{ $analytics->plans->month ?? '0' }}</h1>
                        <p class="text-muted mb-0">Month</p>
                    </div>
                </div>
                <div class="mt-2 text-right">
                    <a href="{{ route('master.filter','expiring') }}" class="text-primary font-size-14 font-weight-medium">View Details <i class="uil uil-angle-double-right"></i></a>
                </div>
            </div>
        </div>
    </div> <!-- end col-->

</div>
@endsection
