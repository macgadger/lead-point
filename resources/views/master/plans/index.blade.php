@extends('layouts.app')
@section('title', 'Plans List')
@section('content')
<div class="card-body">
    <x-alert />
        <div class="table-wrapper">
        <table id="datatable" class="table table-centered table-hover dt-responsive nowrap table-card-list" style="border-collapse: collapse; border-spacing: 0 12px; width: 100%;">
            <thead>
                <tr class="bg-transparent">
                    <th class="wd-5p">#</th>
                    <th class="wd-15p">Plan Name</th>
                    <th class="wd-15p">User Limit</th>
                    <th class="wd-10p">Leads Limit</th>
                    <th class="wd-10p">Status</th>
                    <th class="wd-20p">Actions</th>
                </tr>
            </thead>
            <tbody>
            @if(isset($plans) && $plans->isNotEmpty())
                @foreach($plans as $key => $plan)
                <tr>
                    <td>{{ $key+1 }}</td>
                    <td>{{ strtoupper($plan->name) }}</td>
                    <td>{{ $plan->users_limit }}</td>
                    <td>{{ $plan->leads_limit }}</td>
                    <td>
                        @if($plan->status == 1)
                            <span class="btn btn-success btn-sm">Active</span>
                        @else
                            <span class="btn btn-danger btn-sm">Suspended</span>
                        @endif
                    </td>
                    <td>
                        <div class="btn-group" role="group" aria-label="Controls">
                            <a href="{{ route('plans.edit',$plan->id) }}" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="bottom"
                                title="Edit Details"><i class="uil uil-edit-alt"></i></a>
                            <a href="#" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="bottom"
                                title="Delete User"><i class="uil uil-trash-alt"></i></a>
                        </div>
                    </td>
                </tr>                
                @endforeach
            @endif
            </tbody>
        </table>
    </div>
</div>
@endsection

@push('scripts')
    <script>
            $(function(){            
              $('#datatable').DataTable({
                responsive : true,
                sDom : 'Rfrtlip',
                language: {
                  searchPlaceholder: 'Search...',
                  sSearch: '',
                  lengthMenu: '_MENU_ items/page',
                },
                 'columnDefs': [ {
                    'targets'  : [2,6], /* column index */
                    'orderable': false, /* true or false */
                }],
                fixedColumns: true
              });
            });
      </script>
@endpush