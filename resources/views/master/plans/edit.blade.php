@extends('layouts.app')
@section('title', 'Edit Plan')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <x-validation />
                <!-- Card Body -->
                <form name="companyForm" method="post" action="{{ route('plans.update',$plan->id) }}"
                    data-parsley-validate>
                    @csrf
                    @method('put')
                    <div class="form-layout">
                        <div class="row mg-b-25">

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="form-control-label">Plan Name: <span
                                            class="text-danger">*</span></label>
                                    <input class="form-control" type="text" name="name"
                                        style="text-transform:capitalize" value="{{ $plan->name }}"
                                        placeholder="Enter Plan name" required>
                                </div>
                            </div>
                            <div class="col-md-8">&nbsp;</div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="form-control-label">Users Limit: <span
                                            class="text-danger">*</span></label>
                                    <input class="form-control" type="number" name="users_limit"
                                        style="text-transform:capitalize" value="{{ $plan->users_limit }}"
                                        placeholder="Enter Users Limit" required>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="form-control-label">Leads Limit: <span
                                            class="text-danger">*</span></label>
                                    <input class="form-control" type="number" name="leads_limit"
                                        style="text-transform:capitalize" value="{{ $plan->leads_limit }}"
                                        placeholder="Enter Leads Limit" required>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div id="slWrapperPlan" class="parsley-select">
                                    <div class="form-group">
                                        <label class="form-control-label">Plan: <span
                                                class="text-danger">*</span></label>
                                        <select class="form-control select2" name="status"
                                            data-parsley-class-handler="#slWrapperPlan"
                                            data-parsley-errors-container="#slErrorContainerPlan" required>
                                            <option value="1" @if($plan->status =='1') selected @endif>Active</option>
                                            <option value="0" @if($plan->status =='0') selected @endif>Suspended
                                            </option>
                                        </select>
                                        <div id="slErrorContainerPlan"></div>
                                    </div>
                                </div><!-- col-4 -->
                            </div>

                        </div>

                        <div class="form-layout-footer">
                            <button class="btn btn-primary bd-0">Update Plan</button>
                        </div><!-- form-layout-footer -->
                    </div><!-- form-layout -->
                </form>
                <!-- Card Body -->
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
    <script>
        $('.select2').select2();
    </script>
@endpush


