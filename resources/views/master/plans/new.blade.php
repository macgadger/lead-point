@extends('layouts.app')
@section('title', 'Create Plan')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">

            <div class="card-body">
                <x-validation />
                <!-- Card Body -->
                <form name="companyForm" method="post" action="{{ route('plans.store') }}" data-parsley-validate>
                    @csrf
                    <div class="form-layout">
                        <div class="row mg-b-25">

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="form-control-label">Plan Name: <span
                                            class="text-danger">*</span></label>
                                    <input class="form-control" type="text" name="name"
                                        style="text-transform:capitalize" value="{{ old('name') }}"
                                        placeholder="Enter Plan name" required>
                                </div>
                            </div>
                            <div class="col-md-8">&nbsp;</div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="form-control-label">Users Limit: <span
                                            class="text-danger">*</span></label>
                                    <input class="form-control" type="number" name="users_limit"
                                        style="text-transform:capitalize" value="{{ old('users_limit') }}"
                                        placeholder="Enter Users Limit" required>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="form-control-label">Leads Limit: <span
                                            class="text-danger">*</span></label>
                                    <input class="form-control" type="number" name="leads_limit"
                                        style="text-transform:capitalize" value="{{ old('leads_limit') }}"
                                        placeholder="Enter Leads Limit" required>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div id="slWrapperPlan" class="parsley-select">
                                    <div class="form-group">
                                        <label class="form-control-label">Plan: <span
                                                class="text-danger">*</span></label>
                                        <select class="form-control select2" name="status"
                                            data-parsley-class-handler="#slWrapperPlan"
                                            data-parsley-errors-container="#slErrorContainerPlan" required>
                                            <option value=""> ----Select Status---- </option>
                                            <option value="1">Active</option>
                                            <option value="0">Suspended</option>
                                        </select>
                                        <div id="slErrorContainerPlan"></div>
                                    </div>
                                </div><!-- col-4 -->
                            </div>

                        </div>

                        <div class="form-layout-footer">
                            <button class="btn btn-primary bd-0">Create Plan</button>
                        </div><!-- form-layout-footer -->
                    </div><!-- form-layout -->
                </form>
                <!-- Card Body -->
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
    <script>
        $('.select2').select2();
    </script>
@endpush


