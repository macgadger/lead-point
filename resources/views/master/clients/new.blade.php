@extends('layouts.app')
@section('title', 'New Client')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">

<div class="card-body">
    <x-validation />
    <!-- Card Body -->
    <form name="companyForm" method="post" action="{{ route('client.store') }}" enctype="multipart/form-data" data-parsley-validate>
        @csrf
        <div class="form-layout">
            <div class="row mg-b-25">

                <div class="col-lg-12">
                    <div class="form-group">
                        <label class="form-control-label">Company Name: <span class="text-danger">*</span></label>
                        <input class="form-control" type="text" name="company_name" style="text-transform:capitalize"
                            value="{{ old('company_name') }}" minlength="3" placeholder="Enter company name" required>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="form-group">
                        <label class="form-control-label">Firstname: <span class="text-danger">*</span></label>
                        <input class="form-control" type="text" name="first_name" style="text-transform:capitalize"
                            value="{{ old('first_name') }}" minlength="3" placeholder="Enter firstname" required>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="form-group">
                        <label class="form-control-label">Lastname: <span class="text-danger">*</span></label>
                        <input class="form-control" type="text" name="last_name" style="text-transform:capitalize"
                            value="{{ old('last_name') }}" placeholder="Enter lastname" required>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="form-group">
                        <label class="form-control-label">Phone Number: <span class="text-danger">*</span></label>
                        <input class="form-control" type="phone" name="phone" value="{{ old('phone') }}"
                            minlength="10" maxlength="13" placeholder="Enter company Mobile Number" required>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="form-group">
                        <label class="form-control-label">Email: <span class="text-danger">*</span></label>
                        <input class="form-control" type="email" name="email" value="{{ old('email') }}"
                            placeholder="Enter company Email Address" required>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="form-group">
                        <label class="form-control-label">Website:</label>
                        <input class="form-control" type="text" name="website" value="{{ old('website') }}"
                            placeholder="Enter company website">
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="form-group">
                        <label class="form-control-label">Address: <span class="text-danger">*</span></label>
                        <textarea class="form-control" name="address" rows="2" placeholder="Enter Address"
                            required>{{ old('address') }}</textarea>
                    </div>
                </div>

                <x-locations />

                <div class="col-md-12">
                    <hr>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label class="form-control-label">Profile Pic:</label>
                        <input class="form-control" type="file" name="user_img" value="">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label class="form-control-label">Company Logo:</label>
                        <input class="form-control" type="file" name="user_logo" value="">
                    </div>
                </div>


            </div>
            
            <hr>
            <h2>Account Information</h2>
            <hr>
            <div class="row mg-b-25">

                <div class="col-lg-4">
                    <div class="form-group">
                        <label class="form-control-label">Email/Username: <span class="text-danger">*</span></label>
                        <input class="form-control" type="email" name="username" value="{{ old('username') }}" required
                            placeholder="Enter account email/username">
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="form-group">
                        <label class="form-control-label">Password: <span class="text-danger">*</span></label>
                        <input class="form-control" type="password" name="password" placeholder="Choose Password" required>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="form-group">
                        <label class="form-control-label">Repeat Password: <span class="text-danger">*</span></label>
                        <input class="form-control" type="password" name="password_confirmation" placeholder="Repeat Password" required>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div id="slWrapperPlan" class="parsley-select">
                        <div class="form-group">
                            <label class="form-control-label">Plan: <span class="text-danger">*</span></label>
                            <select class="form-control select2" name="plan_id"
                                data-parsley-class-handler="#slWrapperPlan"
                                data-parsley-errors-container="#slErrorContainerPlan" required>
                                <option value=""> ----Select Plan---- </option>
                                @foreach($plans as $plan)
                                    <option value="{{ $plan->id }}">{{ strtoupper($plan->name) }}</option>
                                @endforeach
                            </select>
                            <div id="slErrorContainerPlan"></div>
                        </div>
                    </div><!-- col-4 -->
                </div>

                <div class="col-lg-4">
                    <div id="slWrapperDuration" class="parsley-select">
                        <div class="form-group">
                            <label class="form-control-label">Plan Durations: <span class="text-danger">*</span></label>
                            <select class="form-control select2" name="plan_duration"
                                data-parsley-class-handler="#slWrapperDuration"
                                data-parsley-errors-container="#slErrorContainerDuration" required>
                                <option value=""> ----Select Duration---- </option>
                                @for($i = 0; $i < 12; $i++)
                                    <option value="{{ $i+1 }}">{{ $i+1 }} Months</option>
                                @endfor
                            </select>
                            <div id="slErrorContainerDuration"></div>
                        </div>
                    </div><!-- col-4 -->
                </div>
                

                <div class="col-lg-4">
                    <div id="slWrapperAgent" class="parsley-select">
                        <div class="form-group">
                            <label class="form-control-label">Status: <span class="text-danger">*</span></label>
                            <select class="form-control select2" name="status"
                                data-parsley-class-handler="#slWrapperAgent"
                                data-parsley-errors-container="#slErrorContainerAgent" required>
                                <option value=""> ----Select Status---- </option>
                                <option value="1">Active</option>
                                <option value="0">Suspended</option>
                            </select>
                            <div id="slErrorContainerAgent"></div>
                        </div>
                    </div><!-- col-4 -->
                </div>

                <div class="col-md-12">
                    <hr>
                </div>

            </div><!-- row -->

            <div class="form-layout-footer">
                <button class="btn btn-primary bd-0">Save Company</button>
            </div><!-- form-layout-footer -->
        </div><!-- form-layout -->
    </form>
    <!-- Card Body -->
</div>
</div>
</div>
</div>
@endsection
@push('scripts')
    <script>
        $('.select2').select2();
    </script>
@endpush


