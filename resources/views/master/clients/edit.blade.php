@extends('layouts.app')
@section('title', 'Edit Client')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">            
<div class="card-body">
    <x-validation />
    <!-- Card Body -->
    <form name="companyForm" method="post" action="{{ route('client.update',$client->id) }}" enctype="multipart/form-data" data-parsley-validate>
        @csrf
        @method('put')
        <div class="form-layout">
            <div class="row mg-b-25">

                <div class="col-lg-12">
                    <div class="form-group">
                        <label class="form-control-label">Company Name: <span class="text-danger">*</span></label>
                        <input class="form-control" type="text" name="company_name" style="text-transform:capitalize"
                            value="{{ $client->profile->company_name }}" minlength="3" placeholder="Enter company name" required>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="form-group">
                        <label class="form-control-label">Firstname: <span class="text-danger">*</span></label>
                        <input class="form-control" type="text" name="first_name" style="text-transform:capitalize"
                            value="{{ $client->profile->first_name }}" minlength="3" placeholder="Enter firstname" required>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="form-group">
                        <label class="form-control-label">Lastname: <span class="text-danger">*</span></label>
                        <input class="form-control" type="text" name="last_name" style="text-transform:capitalize"
                            value="{{ $client->profile->last_name }}" placeholder="Enter lastname" required>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="form-group">
                        <label class="form-control-label">Phone Number: <span class="text-danger">*</span></label>
                        <input class="form-control" type="phone" name="phone" value="{{ $client->profile->phone }}"
                            minlength="10" maxlength="13" placeholder="Enter company Mobile Number" required>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="form-group">
                        <label class="form-control-label">Email: <span class="text-danger">*</span></label>
                        <input class="form-control" type="email" name="email" value="{{ $client->profile->email }}"
                            placeholder="Enter company Email Address" required>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="form-group">
                        <label class="form-control-label">Website:</label>
                        <input class="form-control" type="text" name="website" value="{{ $client->profile->website }}"
                            placeholder="Enter company website">
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="form-group">
                        <label class="form-control-label">Address: <span class="text-danger">*</span></label>
                        <textarea class="form-control" name="address" rows="2" placeholder="Enter Address"
                            required>{{ $client->profile->address }}</textarea>
                    </div>
                </div>

                <x-locations :model="$client->profile" />

                <div class="col-md-12">
                    <hr>
                </div>

                <div class="col-md-2">
                    @if($client->profile->avatar === 'default')                        
                        <img src="{{ Avatar::create($client->profile->full_name)->toBase64() }}" class="img-thumbnail rounded-circle avatar-xl ml-5" />
                    @else
                        <img src="{{ $client->profile->asset_avatar }}" class="img-thumbnail rounded-circle avatar-xl ml-5" />
                    @endif                    
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label class="form-control-label">Profile Pic:</label>
                        <input class="form-control" type="file" name="user_img" value="">
                    </div>
                </div>

                <div class="col-md-2">
                    @if($client->profile->company_logo === 'default')                        
                        <img src="{{ Avatar::create($client->profile->company_name)->toBase64() }}" class="img-thumbnail rounded-circle avatar-xl ml-5" />
                    @else
                        <img src="{{ $client->profile->asset_logo }}" class="img-thumbnail rounded-circle avatar-xl ml-5" />
                    @endif
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label class="form-control-label">Company Logo:</label>
                        <input class="form-control" type="file" name="user_logo" value="">
                    </div>
                </div>

            </div>
            
            <hr>
            <h2>Account Information</h2>
            <hr>
            <div class="row mg-b-25">

                <div class="col-lg-4">
                    <div class="form-group">
                        <label class="form-control-label">Email/Username: <span class="text-danger">*</span></label>
                        <input class="form-control" type="email" name="username" value="{{ $client->email }}" required
                            placeholder="Enter account email/username" readonly>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="form-group">
                        <label class="form-control-label">Password: <span class="text-danger">*</span></label>
                        <input class="form-control" type="password" name="password" placeholder="Choose Password">
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="form-group">
                        <label class="form-control-label">Repeat Password: <span class="text-danger">*</span></label>
                        <input class="form-control" type="password" name="password_confirmation" placeholder="Repeat Password">
                    </div>
                </div>

                <div class="col-lg-4">
                    <div id="slWrapperPlan" class="parsley-select">
                        <div class="form-group">
                            <label class="form-control-label">Plan: <span class="text-danger">*</span></label>
                            <select class="form-control select2" name="plan_id"
                                data-parsley-class-handler="#slWrapperPlan"
                                data-parsley-errors-container="#slErrorContainerPlan" required>
                                <option value=""> ----Select Plan---- </option>
                                @foreach($plans as $plan)
                                    <option @if($client->profile->plan_id == $plan->id) selected @endif value="{{ $plan->id }}">{{ strtoupper($plan->name) }}</option>
                                @endforeach
                            </select>
                            <div id="slErrorContainerPlan"></div>
                        </div>
                    </div><!-- col-4 -->
                </div>

                <div class="col-lg-4">
                    <div id="slWrapperDuration" class="parsley-select">
                        <div class="form-group">
                            <label class="form-control-label">Plan Durations: <span class="text-danger">*</span></label>
                            <select class="form-control select2" name="plan_duration"
                                data-parsley-class-handler="#slWrapperDuration"
                                data-parsley-errors-container="#slErrorContainerDuration" required>
                                <option value=""> ----Select Duration---- </option>
                                @for($i = 0; $i < 12; $i++)
                                    <option @if($client->profile->plan_duration == $i+1) selected @endif value="{{ $i+1 }}">{{ $i+1 }} Months</option>
                                @endfor
                            </select>
                            <div id="slErrorContainerDuration"></div>
                        </div>
                    </div><!-- col-4 -->
                </div>
                

                <div class="col-lg-4">
                    <div id="slWrapperAgent" class="parsley-select">
                        <div class="form-group">
                            <label class="form-control-label">Status: <span class="text-danger">*</span></label>
                            <select class="form-control select2" name="status"
                                data-parsley-class-handler="#slWrapperAgent"
                                data-parsley-errors-container="#slErrorContainerAgent" required>
                                <option @if($client->status == 1) selected @endif value="1">Active</option>
                                <option @if($client->status == 2) selected @endif value="0">Suspended</option>
                            </select>
                            <div id="slErrorContainerAgent"></div>
                        </div>
                    </div><!-- col-4 -->
                </div>

                <div class="col-md-12">
                    <hr>
                </div>

            </div><!-- row -->

            <div class="form-layout-footer">
                <button class="btn btn-primary bd-0">Save Company</button>
            </div><!-- form-layout-footer -->
        </div><!-- form-layout -->
    </form>
    <!-- Card Body -->
</div>
</div>
</div>
</div>
@endsection
@push('scripts')
    <script>
        $('.select2').select2();
    </script>
@endpush


