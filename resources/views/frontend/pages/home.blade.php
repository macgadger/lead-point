@extends('layouts.web')
@section('title','Home')
@section('content')
<section class="banner banner-one">
    <div class="circle-shape" data-parallax='{"y" : 230}'><img src="{{ asset('web/media/banner/circle-shape.png') }}" alt="circle">
    </div>
    <div class="container">
        <div class="banner-content-wrap">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="banner-content">
                        <h1 class="banner-title wow pixFadeUp" data-wow-delay="0.3s">BDM.
                            <span>Lead<br>Management</span> is<br>the Best Ever
                        </h1>
                        <p class="description wow pixFadeUp" data-wow-delay="0.5s">“It’s much easier to double your business<br>by doubling your conversion rate”.
                        </p>
                        <a href="#" class="pxs-btn banner-btn wow pixFadeUp" data-wow-delay="0.6s">Get Started</a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="promo-mockup wow pixFadeLeft"><img src="{{ asset('web/media/banner/macbook.png') }}" alt="mpckup">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-shape"><img src="{{ asset('web/media/banner/shape-bg.png') }}" alt=""></div>
</section>

         <section class="featured">
            <div class="container">
               <div class="section-title text-center wow pixFade">
                  <h3 class="sub-title">Working Process</h3>
                  <h2 class="title">The Best CRM Software for Growing Businesses</h2>
               </div>
               <div class="row">
                  <div class="col-md-4">
                     <div class="saaspik-icon-box-wrapper style-one wow pixFadeLeft" data-wow-delay="0.3s">
                        <div class="saaspik-icon-box-icon"><img src="{{ asset('web/media/feature/1.png') }}" alt=""></div>
                        <div class="pixsass-icon-box-content">
                           <h3 class="pixsass-icon-box-title"><a href="#">Close More Deals With <br>Less Work</a></h3>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="saaspik-icon-box-wrapper style-one wow pixFadeLeft" data-wow-delay="0.5s">
                        <div class="saaspik-icon-box-icon"><img src="{{ asset('web/media/feature/2.png') }}" alt=""></div>
                        <div class="pixsass-icon-box-content">
                           <h3 class="pixsass-icon-box-title"><a href="#">Easy to organize, track, and <br>grow your sales</a>
                           </h3>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="saaspik-icon-box-wrapper style-one wow pixFadeLeft" data-wow-delay="0.7s">
                        <div class="saaspik-icon-box-icon"><img src="{{ asset('web/media/feature/3.png') }}" alt=""></div>
                        <div class="pixsass-icon-box-content">
                           <h3 class="pixsass-icon-box-title"><a href="#">Thousand of features and<br>Custom
                              option.</a>
                           </h3>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>

         <section class="genera-informes">
            <div class="container">
               <div class="row">
                  <div class="col-lg-6 pix-order-one">
                     <div class="section-title style-two">
                        <h2 class="title wow pixFadeUp">Single Platform for Leads <br>Tracking, Reporting and
Work Flow Automation</h2>
                        <p class="wow pixFadeUp" data-wow-delay="0.3s">If you're still not Automate your business leads,<br>you're likely leaving money on the table.!
                        </p>
                     </div>
                     <ul class="list-items wow pixFadeUp" data-wow-delay="0.4s">
                        <li>Quick Access</li>
                        <li>Easily Manage</li>
                        <li>7/24h Support</li>
                     </ul>
                     <a href="#" class="pix-btn btn-outline wow pixFadeUp" data-wow-delay="0.5s">Discover
                     More</a>
                  </div>
                  <div class="informes-feature-image">
                     <div class="image-one" data-parallax='{"y" : 20}'><img src="{{ asset('web/media/feature/5.png') }}"
                        class="wow pixFadeDown" alt="informes"></div>
                     <div class="image-two" data-parallax='{"y" : -20}'><img src="{{ asset('web/media/feature/51.png') }}"
                        class="mw-none wow pixFadeDown" data-wow-delay="0.3s" alt="informes"></div>
                  </div>
               </div>
            </div>
            <div class="shape-bg"><img src="{{ asset('web/media/background/shape.png') }}" class="wow fadeInRight" alt="shape-bg"></div>
         </section>


                  <section class="revolutionize">
            <div class="bg-angle"></div>
            <div class="container">
               <div class="section-title dark-title text-center">
                  <h3 class="sub-title wow pixFadeUp">The Problems</h3>
                  <h2 class="title wow pixFadeUp" data-wow-delay="0.3s">Manual processes are painfully slow,<br>unreliable and plain inefficient
                  </h2>
               </div>
               <div id="pix-tabs" class="wow pixFadeUp" data-wow-delay="0.5s">
                  <ul id="pix-tabs-nav">
                     <li><a href="#tab1">Quick Access</a></li>
                     <li><a href="#tab2">Easily Manage</a></li>
                     <li><a href="#tab3">Leads Automation</a></li>
                     <li><a href="#tab4">7/24h Support</a></li>
                  </ul>
                  <div id="pix-tabs-content">
                     <div id="tab1" class="content">
                        <img src="{{ asset('web/media/revolutionize/2.jpg') }}" alt="revolutionize">
                        <div class="shape-shadow"></div>
                     </div>
                     <div id="tab2" class="content">
                        <img src="{{ asset('web/media/revolutionize/1.jpg') }}" alt="revolutionize">
                        <div class="shape-shadow"></div>
                     </div>
                     <div id="tab3" class="content">
                        <img src="{{ asset('web/media/revolutionize/3.jpg') }}" alt="revolutionize">
                        <div class="shape-shadow"></div>
                     </div>
                     <div id="tab4" class="content">
                        <img src="{{ asset('web/media/revolutionize/4.jpg') }}" alt="revolutionize">
                        <div class="shape-shadow"></div>
                     </div>
                  </div>
               </div>
            </div>
         </section>

         <section class="testimonials swiper-init">
            <div class="container">
               <div class="section-title text-center">
                  <h3 class="sub-title wow pixFadeUp">Testiimonial</h3>
                  <h2 class="title wow pixFadeUp" data-wow-delay="0.3s">What our client say about us</h2>
               </div>
               <div class="testimonial-wrapper wow pixFadeUp" data-wow-delay="0.5s">
                  <div class="swiper-container" id="testimonial" data-speed="700" data-autoplay="5000">
                     <div class="swiper-wrapper">
                        <div class="swiper-slide">
                           <div class="testimonial">
                              <div class="single-bio-thumb"><img src="{{ asset('web/media/testimonial/1.jpg') }}" alt="testimonial">
                              </div>
                              <div class="testimonial-content">
                                 <p>“BDM Live CRM has played an important role in growing to our business. 
                                 We now have more than 150 employees using A2IT services. 
                                 The functionalities we've added have evolved BDM CRM beyond 
                                 just lead and contact management to more of a complete operational platform."
                                 </p>
                              </div>
                              <div class="bio-info">
                                 <h3 class="name">Arvind Goyal</h3>
                                 <span class="job">CEO, Amaron India </span>
                              </div>
                           </div>
                        </div>
                        <div class="swiper-slide">
                           <div class="testimonial">
                              <div class="single-bio-thumb"><img src="{{ asset('web/media/testimonial/1.jpg') }}" alt="testimonial">
                              </div>
                              <div class="testimonial-content">
                                 <p>We switched to BDM CRM from Saleforce at the beginning of the 2020. 
                                 It's used track both prospects, clients, sales, referrals, etc. 
                                 It's used by Lead Managements, Reporting, and Account Management. 
                                 It helps with tracking and analytics for our Sales. 
                                 It automates some processes such email campaigns, Whatsapp and SMS templates.
                                 </p>
                              </div>
                              <div class="bio-info">
                                 <h3 class="name">Tosin Olugbenga</h3>
                                 <span class="job">Chief Stategist, ONES Inc</span>
                              </div>
                           </div>
                        </div>
                        <div class="swiper-slide">
                           <div class="testimonial">
                              <div class="single-bio-thumb"><img src="{{ asset('web/media/testimonial/1.jpg') }}" alt="testimonial">
                              </div>
                              <div class="testimonial-content">
                                 <p>I think the positive impact it has on the business is that it truly keeps tracks of all the leads / visits, reports and revenue. Our management can look at trends and what we need to improve on to make the business grow. 
                                 All of our data from leads follow-ups are store in BDM.  BDM pretty much puts it all in one.
                                 </p>
                              </div>
                              <div class="bio-info">
                                 <h3 class="name">Forest Smith</h3>
                                 <span class="job">Founder, CMC Inc Canada</span>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="shape-shadow"></div>
                  <div class="slider-nav wow pixFadeUp" data-wow-delay="0.3s">
                     <div id="slide-prev" class="swiper-button-prev"><i class="ei ei-arrow_left"></i></div>
                     <div id="slide-next" class="swiper-button-next"><i class="ei ei-arrow_right"></i></div>
                  </div>
               </div>
            </div>
            <div class="scroll-circle wow pixFadeDown"><img src="{{ asset('web/media/background/circle9.png') }}"
               data-parallax='{"y" : 250}' alt="circle"></div>
         </section>
         <section class="pricing">
            <div class="container">
               <div class="section-title text-center">
                  <h3 class="sub-title wow pixFadeUp">Pricing Plan</h3>
                  <h2 class="title wow pixFadeUp" data-wow-delay="0.3s">No Hidden Charges! Choose<br>your Plan.</h2>
               </div>
               <nav class="pricing-tab wow pixFadeUp" data-wow-delay="0.4s"><span
                  class="tab-btn monthly_tab_title">Monthly </span><span class="pricing-tab-switcher"></span>
                  <span class="tab-btn annual_tab_title">Annual</span>
               </nav>
               <div class="row advanced-pricing-table no-gutters wow pixFadeUp" data-wow-delay="0.5s">
                  <div class="col-lg-4">
                     <div class="pricing-table br-left">
                        <div class="pricing-header pricing-amount">
                           <div class="annual_price">
                              <h2 class="price">Rs.14.500</h2>
                           </div>
                           <div class="monthly_price">
                              <h2 class="price">Rs.2000</h2>
                           </div>
                           <h3 class="price-title">STARTER</h3>
                           <p>Perfect for small teams</p>
                        </div>
                        <ul class="price-feture">
                           <li class="have">3000 Leads</li>
                           <li class="have">5 Users</li>
                           <li class="have">Android app</li>
                           <li class="have">Lead Management</li>
                           <li class="have">Team Management</li>
                           <li class="have">Live Team Report</li>
                           <li class="have">SMS Integration</li>
                           <li class="have">E-Mail Integration</li>
                           <li class="have">Whatsapp Integration</li>
                             <li class="have">Calling from app</li
                        </ul>
                        <div class="action text-center"><a href="#" class="pix-btn btn-outline">Get Started</a>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-4">
                     <div class="pricing-table color-two">
                        <div class="pricing-header pricing-amount">
                           <div class="annual_price">
                              <h2 class="price">Rs.20.500</h2>
                           </div>
                           <div class="monthly_price">
                              <h2 class="price">Rs.3000</h2>
                           </div>
                           <h3 class="price-title">GROWTH</h3>
                           <p>Best value for growing teams</p>
                        </div>
                        <ul class="price-feture">
                           <li class="have">5000 Leads</li>
                           <li class="have">10 Users</li>
                           <li class="have">Android app</li>
                           <li class="have">Lead Management</li>
                           <li class="have">Team Management</li>
                           <li class="have">Live Team Report</li>
                           <li class="have">SMS Integration</li>
                           <li class="have">E-Mail Integration</li>
                           <li class="have">Whatsapp Integration</li>
                           <li class="have">Calling from app</li
                        </ul>
                        <div class="action text-center"><a href="#" class="pix-btn btn-outline">Get Started</a>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-4">
                     <div class="pricing-table color-three">
                        <div class="pricing-header pricing-amount">
                           <div class="annual_price">
                              <h2 class="price">Rs.28.500</h2>
                           </div>
                           <div class="monthly_price">
                              <h2 class="price">Rs.4000</h2>
                           </div>
                           <h3 class="price-title">BOOST</h3>
                           <p>To boost unstoppable teams</p>
                        </div>
                        <ul class="price-feture">
                           <li class="have">10.000 Leads</li>
                           <li class="have">20 Users</li>
                           <li class="have">Android app</li>
                           <li class="have">Lead Management</li>
                           <li class="have">Team Management</li>
                           <li class="have">Live Team Report</li>
                           <li class="have">SMS Integration</li>
                           <li class="have">E-Mail Integration</li>
                           <li class="have">Whatsapp Integration</li>
                           <li class="have">Calling from app</li
                        </ul>
                        <div class="action text-center"><a href="#" class="pix-btn btn-outline">Get Started</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            </section>

        <section class="call-to-action">
            <div class="overlay-bg"><img src="{{ asset('web/media/background/ellipse.png') }}" alt="bg"></div>
            <div class="container">
               <div class="action-content text-center wow pixFadeUp">
                  <h2 class="title">With a amazingly affordable CRM,<br>Take our 15 days risk-free trial!</h2>
                  <p>Build New and Lasting Customer Relationships
                  </p>
                  <a href="#" class="pix-btn btn-light">Book Live Demo</a>
               </div>
            </div>
            <div class="scroll-circle"><img src="{{ asset('web/media/background/circle13.png') }}" data-parallax='{"y" : -130}'
               alt="circle"></div>
         </section>

@endsection