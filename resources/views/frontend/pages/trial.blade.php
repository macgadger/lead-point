@extends('layouts.web')
@section('title','Book a free Trial')
@section('content')
<section class="signin signup">
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="signin-from-wrapper">
                    <div class="signin-from-inner">
                        <h2 class="title">Book your Free Trial Now!</h2>
                        
                        @if(session('success'))
                            <div class="alert alert-solid alert-success  mt-20" role="alert">
                                <strong>{{ session('success') }}</strong>
                            </div><!-- alert -->
                        @else
                            @if($errors->count())
                                @foreach ($errors->all() as $message)
                                    <div class="alert alert-danger" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        {{ $message }}
                                    </div>
                                @endforeach
                            @endif

                            <form method="POST" action="{{ route('trial.book') }}" class="singn-form mt-2">
                                @csrf
                                <input type="text" name="name" placeholder="Your Name"> 
                                <input type="text" name="email" placeholder="Your Company Email"> 
                                <input type="text" name="phone" placeholder="Your Contact Number"> 
                                
                                <div class="forget-link">
                                    <div class="condition"><input class="styled-checkbox" id="styled-checkbox-1"
                                            type="checkbox" value="value1"> <label for="styled-checkbox-1"></label>
                                        <span>I wish to recieve newsletters, promotions and news from.</span></div>
                                </div><button type="submit" class="pix-btn">Book Now</button>
                            </form>
                        @endif
                    </div>
                    <ul class="animate-ball">
                        <li class="ball"></li>
                        <li class="ball"></li>
                        <li class="ball"></li>
                        <li class="ball"></li>
                        <li class="ball"></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="signin-banner signup-banner">
        <div class="animate-image-inner">
            <div class="image-one"><img src="{{ asset('web/media/animated/signup.png') }}" alt="" class="wow pixFadeLeft"></div>
            <div class="image-two"><img src="{{ asset('web/media/animated/signup2.png') }}" alt="" class="wow pixFadeRight"></div>
        </div>
    </div>
</section>
@endsection