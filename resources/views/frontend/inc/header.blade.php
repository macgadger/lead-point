<header class="site-header header-dark header_trans-fixed" data-top="992">
    <div class="container">
        <div class="header-inner">
            <div class="toggle-menu"><span class="bar"></span> <span class="bar"></span> <span class="bar"></span></div>
            <div class="site-mobile-logo"><a href="index.html" class="logo"><img src="{{ asset('web/media/bdm.png') }}"
                        alt="site logo" class="main-logo"> <img src="{{ asset('web/media/bdm.png') }}" alt="site logo"
                        class="sticky-logo"></a></div>
            <nav class="site-nav">
                <div class="close-menu"><span>Close</span> <i class="ei ei-icon_close"></i></div>
                <div class="site-logo"><a href="index.html" class="logo"><img src="{{ asset('web/media/bdm.png') }}"
                            alt="site logo" class="main-logo"> <img src="{{ asset('web/media/bdm.png') }}" alt="site logo"
                            class="sticky-logo"></a></div>
                <div class="menu-wrapper" data-top="992">
                    <ul class="site-main-menu">
                        
                        @if(request()->routeIs('home'))
                            <li><a href="{{ route('home') }}" >Home</a></li>
                            <li><a href="{{ route('about') }}" >About Us</a></li>
                            <li><a href="{{ route('features') }}" >Features</a></li>
                            <li><a href="{{ route('pricing') }}" >Pricing</a></li>
                            <li><a href="{{ route('contact') }}">Contact</a></li>
                        @else
                            <li><a href="{{ route('home') }}" style="color: #2b2350">Home</a></li>
                            <li><a href="{{ route('about') }}" style="color: #2b2350">About Us</a></li>
                            <li><a href="{{ route('features') }}" style="color: #2b2350">Features</a></li>
                            <li><a href="{{ route('pricing') }}" style="color: #2b2350">Pricing</a></li>
                            <li><a href="{{ route('contact') }}" style="color: #2b2350">Contact</a></li>
                        @endif
                    </ul>
                    <div class="nav-right"><a href="{{ route('trial') }}" class="nav-btn">Free Trial</a></div>
                    <div class="nav-right"><a href="{{ route('account.login') }}" class="nav-btn">Login</a></div>
                </div>
            </nav>
        </div>
    </div>
</header>