@extends('layouts.web')
@section('title','Reset Password')
@section('content')
<section class="signin">
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="signin-from-wrapper">
                    <div class="signin-from-inner">
                        <h2 class="title">Forgot your Password</h2>
                        <p>
                            Enter your email to reset your password
                        </p>

                        @if(session('error'))
                            <div class="alert alert-solid alert-danger  mt-20" role="alert">
                                <strong>{{ session('error') }}</strong>
                            </div><!-- alert -->
                        @endif

                        @if($errors->count())
                            @foreach ($errors->all() as $message)
                                <div class="alert alert-danger" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    {{ $message }}
                                </div>
                            @endforeach
                        @endif
                        
                        @if(session('success'))
                            <div class="alert alert-solid alert-success mt-20" role="alert">
                                <strong>{{ session('success') }}</strong>
                            </div><!-- alert -->
                        @else
                            <form method="POST" action="{{ route('account.forgot') }}" class="singn-form">
                                @csrf
                                <input type="text" placeholder="Enter your Email" name="email" autofocus required>
                                <button type="submit" class="pix-btn">Reset Password</button>
                            </form>
                        @endif
                    </div>
                    <!-- /.signin-from-inner -->

                    <ul class="animate-ball">
                        <li class="ball"></li>
                        <li class="ball"></li>
                        <li class="ball"></li>
                        <li class="ball"></li>
                        <li class="ball"></li>
                    </ul>
                </div>
                <!-- /.signin-from-wrapper -->
            </div>
            <!-- /.col-lg-7 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->

    <div class="signin-banner">
        <img src="{{ asset('web/media/animated/lock.png') }}" alt="" class="image-one wow pixFadeDown">
        <img src="{{ asset('web/media/animated/lock2.png') }}" alt="" class="image-two wow pixFadeUp">

    </div>
    <!-- /.signin-banner -->
</section>
@endsection