@extends('layouts.web')
@section('title','Login')
@section('content')
<section class="signin">
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="signin-from-wrapper">
                    <div class="signin-from-inner">
                        <h2 class="title">Sign In</h2>
                        <p>
                            Close More Deals With Less Work!
                        </p>

                        @if(session('error'))
                            <div class="alert alert-solid alert-danger  mt-20" role="alert">
                                <strong>{{ session('error') }}</strong>
                            </div><!-- alert -->
                        @endif

                        @if($errors->count())
                            @foreach ($errors->all() as $message)
                                <div class="alert alert-danger" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    {{ $message }}
                                </div>
                            @endforeach
                        @endif

                        <form method="POST" action="{{ route('account.auth') }}" class="singn-form">

                            @csrf
                            <input type="text" placeholder="Email" name="email" autofocus required>
                            <input type="password" placeholder="Password" name="password" required >

                            <div class="forget-link">
                                <div class="condition">
                                    <input class="styled-checkbox" id="styled-checkbox-1" type="checkbox"
                                        value="value1">
                                    <label for="styled-checkbox-1"></label>
                                    <span>Remember Me</span>

                                </div>

                                <a href="{{ route('account.forgot') }}" class="forget">Forget Password</a>
                            </div>

                            <button type="submit" class="pix-btn">Sign In</button>
                        </form>
                    </div>
                    <!-- /.signin-from-inner -->

                    <ul class="animate-ball">
                        <li class="ball"></li>
                        <li class="ball"></li>
                        <li class="ball"></li>
                        <li class="ball"></li>
                        <li class="ball"></li>
                    </ul>
                </div>
                <!-- /.signin-from-wrapper -->
            </div>
            <!-- /.col-lg-7 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->

    <div class="signin-banner">
        <img src="{{ asset('web/media/animated/lock.png') }}" alt="" class="image-one wow pixFadeDown">
        <img src="{{ asset('web/media/animated/lock2.png') }}" alt="" class="image-two wow pixFadeUp">

    </div>
    <!-- /.signin-banner -->
</section>
@endsection