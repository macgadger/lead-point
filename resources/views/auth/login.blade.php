<!doctype html>
<html lang="en">

<head>

    <meta charset="utf-8" />
    <title>{{ env('APP_NAME','Curvet') }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="assets/images/favicon.ico') }}">

    <!-- Bootstrap Css -->
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://unicons.iconscout.com/release/v3.0.6/css/line.css">
    <!-- App Css-->
    <link href="{{ asset('assets/css/app.min.css') }}" rel="stylesheet" type="text/css" />

</head>

<body class="authentication-bg">

    <div class="home-btn d-none d-sm-block">
        <a href="/" class="text-dark"><i class="mdi mdi-home-variant h2"></i></a>
    </div>
    <div class="account-pages my-5 pt-sm-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="text-center">
                        <a href="/" class="mb-5 d-block auth-logo">
                            <img src="{{ asset('web/media/bdm.png') }}" alt="" height="42" class="logo logo-dark">
                            <img src="{{ asset('web/media/bdm.png') }}" alt="" height="42" class="logo logo-light">
                        </a>
                    </div>
                </div>
            </div>
            <div class="row align-items-center justify-content-center">
                <div class="col-md-8 col-lg-6 col-xl-5">
                    <div class="card">

                        <div class="card-body p-4">
                            <div class="text-center mt-2">
                                <h5 class="text-primary">Welcome Back !</h5>
                                <p class="text-muted">Sign in to continue to your account.</p>

                                @if(session('error'))
                                    <div class="alert alert-solid alert-danger  mt-20" role="alert">
                                        <strong>{{ session('error') }}</strong>
                                    </div><!-- alert -->
                                @endif

                                @if($errors->count())
                                    @foreach ($errors->all() as $message)
                                        <div class="alert alert-danger" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            {{ $message }}
                                        </div>
                                    @endforeach
                                @endif

                            </div>
                            <div class="p-2 mt-4">
                                <form method="POST" action="{{ route('master.auth') }}">
                                    @csrf
                                    <div class="form-group">
                                        <label for="username">Email</label>
                                        <input type="text" class="form-control" name="email"
                                            placeholder="Enter email"
                                            autofocus />
                                    </div>

                                    <div class="form-group">
                                        <div class="float-right">
                                            <a href="#" class="text-muted">Forgot password?</a>
                                        </div>
                                        <label for="userpassword">Password</label>
                                        <input type="password" class="form-control" name="password"
                                            placeholder="Enter password">
                                    </div>

                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="auth-remember-check">
                                        <label class="custom-control-label" for="auth-remember-check">Remember
                                            me</label>
                                    </div>

                                    <div class="mt-3 text-right">
                                        <button class="btn btn-primary w-sm waves-effect waves-light" type="submit">Log In</button>
                                    </div>

                                    <div class="mt-4 text-center">
                                        <p class="mb-0">Don't have an account ? <a href="auth-register.html"
                                                class="font-weight-medium text-primary"> Signup now </a> </p>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>

                    <div class="mt-5 text-center">
                        <p>© 2020 {{ env('APP_NAME') }}.
                        Crafted with <i class="uil uil-heart text-danger"></i> by <a href="https://overleap.co.in/" target="_blank" class="text-reset">Overleap</a>
                    </div>

                </div>
            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </div>

    <!-- JAVASCRIPT -->
    <script src="{{ asset('assets/libs/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/libs/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/libs/metismenu/metisMenu.min.js') }}"></script>
    <script src="{{ asset('assets/libs/simplebar/simplebar.min.js') }}"></script>
    <script src="{{ asset('assets/libs/node-waves/waves.min.js') }}"></script>
    <script src="{{ asset('assets/libs/waypoints/lib/jquery.waypoints.min.js') }}"></script>
    <script src="{{ asset('assets/libs/jquery.counterup/jquery.counterup.min.js') }}"></script>

    <script src="{{ asset('assets/js/app.js') }}"></script>

</body>

</html>
