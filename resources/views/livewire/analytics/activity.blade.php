<div>
    
    <div class="card">
        <div class="card-body">
            <h4 class="card-title mb-4">Recent Activities</h4>
            <div data-simplebar style="display: block; height: 338px; max-height: 338px; overflow-y: scroll;">
                @isset($activities)
                <table class="table table-borderless table-sm">
                    <tbody>
                        @foreach($activities as $activity)
                        <tr>
                            <td style="width: 20px;">
                                @if( $activity->createdBy->avatar === 'default')
                                    <img src="{{ Avatar::create($activity->lead->createdBy->full_name)->toBase64() }}" class="avatar-xs rounded-circle" alt="...">
                                @else
                                    <img src="{{ $activity->createdBy->asset_avatar }}" class="avatar-xs rounded-circle" alt="...">
                                @endif
                            </td>
                            <td style="width: 150px;">
                                <h6 class="font-size-15 mb-1 font-weight-normal">{{ $activity->createdBy->full_name }}
                                </h6>
                                <p class="text-muted font-size-13 mb-0">
                                    <i class="uil uil-clock"></i>
                                    {{ \Carbon\Carbon::parse($activity->created_at)->diffForHumans()  }}
                                </p>
                            </td>
                            <td>
                                <span
                                    class="badge badge-soft-danger font-size-12">{{ ucfirst($activity->comment) }}</span><br>
                                <p class="text-muted font-size-13 mb-0">Remarks: {{ ucfirst($activity->notes) }}</p>
                            </td>
                            <td>
                                <div class="btn-group" role="group" aria-label="Controls">
                                    <button class="btn btn-sm btn-warning rounded-10 modal-link" data-toggle="tooltip"
                                        data-placement="bottom" data-title="Lead Details"
                                        data-href="{{ route('leads.show', $activity->lead->id) }}">
                                        <i class="uil uil-info-circle"></i>
                                    </button>
                                    <button class="btn btn-sm btn-danger rounded-10 modal-link" data-toggle="tooltip"
                                        data-placement="bottom" data-title="Activity Timeline"
                                        data-href="{{ route('leads.timeline', $activity->lead->id) }}">
                                        <i class="uil uil-history"></i>
                                    </button>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                @else
                    <h4 class="mt-3">No Recent Activity found</h4>
                @endisset
            </div>
        </div>
    </div>

</div>