<div>
    <div class="card">
        <div class="card-body">
            <h4 class="card-title mb-4">Agents Summary</h4>
            <ul class="nav nav-tabs nav-tabs-custom" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#leads">Leads</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#reminders">Reminders</a>
                </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane container active" id="leads" style="height: 300px;">
                    @isset($summary)
                    <div class="table-responsive">
                        <table class="table tx-13 table-centered table-nowrap table-hover mb-0" style="display: block; max-height: 300px; overflow-y: scroll;">
                            <thead>
                                <tr>
                                    <th>Agent</th>
                                    <th>Today</th>
                                    <th>Hot</th>
                                    <th>This Month</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($summary as $agent)
                                <tr>
                                    <td>
                                        <a href="" class="tx-inverse tx-14 tx-medium d-block">
                                            @if( $agent->agent->avatar === 'default')
                                                <img src="{{ Avatar::create($agent->agent->full_name)->toBase64() }}" class="img-thumbnail align-self-start rounded-circle avatar-sm" alt="...">
                                            @else
                                                <img src="{{ $agent->agent->asset_avatar }}" class="img-thumbnail align-self-start rounded-circle avatar-sm" alt="...">
                                            @endif
                                            {{ ucwords($agent->agent->full_name) }}
                                        </a>
                                    </td>
                                    <td class="tx-12">
                                        <span class="tx-14 d-block">{{ $agent->today }}</span>
                                    </td>
                                    <td class="tx-12">
                                        <span class="tx-14 d-block">{{ $agent->hot }}</span>
                                    </td>
                                    <td class="tx-12">
                                        <span class="tx-14 d-block">{{ $agent->month }}</span>
                                    </td>
                                </tr>
                                @endforeach
                                
                            </tbody>
                        </table>
                    </div><!-- table-responsive -->
                    @else
                        <h4 class="mt-3">No Data found</h4>
                    @endisset
                </div>

                <div class="tab-pane container fade" id="reminders">
                    
                        <div class="table-responsive">
                        <table class="table table-borderless table-centered table-nowrap">
                            <tbody class="mb-5">
                            @isset($reminders['leads'])
                                @foreach ($reminders['leads'] as $lead)
                                    <tr>
                                        <td style="width: 20px;"><img src="{{ Avatar::create($lead->name)->toBase64() }}" class="avatar-xs rounded-circle " alt="..."></td>
                                        <td>
                                            <h6 class="font-size-15 mb-1 fw-normal">{{ ucwords($lead->name) }}</h6>
                                            <p class="text-muted font-size-13 mb-0"><i class="uil uil-gift me-2 text-success"></i> Lead</p>
                                        </td>
                                        <td class="text-muted fw-semibold text-end">
                                            {{ $lead->dob->format('d M ,Y') }}
                                        </td>
                                    </tr>
                                @endforeach
                            @endisset

                            @isset($reminders['agents'])
                                @foreach ($reminders['agents'] as $agent)
                                    <tr>
                                        <td style="width: 20px;"><img src="{{ Avatar::create($agent->name)->toBase64() }}" class="avatar-xs rounded-circle " alt="..."></td>
                                        <td>
                                            <h6 class="font-size-15 mb-1 fw-normal">{{ $agent->first_name.' '.$agent->last_name }}</h6>
                                            <p class="text-muted font-size-13 mb-0"><i class="uil uil-gift me-2 text-success"></i> Employee</p>
                                        </td>
                                        <td class="text-muted fw-semibold text-end">
                                            {{ $agent->dob->format('d M ,Y') }}
                                        </td>
                                    </tr>
                                @endforeach
                            @endisset


                            </tbody>
                            </table>
                        </div> <!-- enbd table-responsive-->
                    
                </div>

            </div>

        </div>
    </div>
</div>
