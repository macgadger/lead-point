<div>
    <div class="card">
        <div class="card-body">
            <div class="float-right">
                <div class="spinner-border text-primary" wire:loading role="status">
                    <span class="sr-only">Loading...</span>
                </div>
                <button wire:loading.remove class="btn text-primary font-size-16 btn-xs" title="Refresh Feed" wire:click="refresh"><i class="uil uil-sync"></i></button>
            </div>
            <h5 class="prod-name"><a href="javascript:void(0)">Sources</a></h5>
            <div class="row">
                @foreach($sources as $source)
                <div class="col">
                    <div class="text-center mb-1 mt-4">
                        <h5 class="font-size-18">{{ $source->sourceCount($source->id) }}</h5>
                        <p class="text-muted mb-0">
                            {{ ucwords($source->name) }}
                        </p>
                    </div>
                </div>
                @endforeach
            </div>
            
        </div>
    </div>
</div>
