<div>
    <div class="card">
        <div class="card-body">
            <div class="float-right">
                <div class="spinner-border text-primary" wire:loading role="status">
                    <span class="sr-only">Loading...</span>
                </div>
                <button wire:loading.remove class="btn text-primary font-size-16 btn-xs" title="Refresh Feed" wire:click="refresh"><i class="uil uil-sync"></i></button>
            </div>
            <h5 class="prod-name"><a href="javascript:void(0)">Followup</a></h5>
            <div class="row">
                <div class="col-5">
                    <h1 class="mb-1 mt-1">{{ $followup[0]->today ?? '0' }}</h1>
                    <p class="text-muted mb-0">Today</p>
                </div>
                <div class="col-7">
                    <h1 class="mb-1 mt-1">{{ $followup[0]->pending ?? '0' }}</h1>
                    <p class="text-muted mb-0">Pending</p>
                </div>
            </div>
            <div class="mt-2 text-right">
                <a href="{{ route('filters.index','followup') }}" class="text-primary font-size-14 font-weight-medium">View Details <i class="uil uil-angle-double-right"></i></a>
            </div>
        </div>
    </div>
</div>
