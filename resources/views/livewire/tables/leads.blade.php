<div>
    <div class="row mb-3" x-data="{ tab: 'active' }" data-turbolinks="true">
        <div class="col-md-10">
            <!-- Nav tabs -->
            <ul class="nav nav-pills nav-justified-sm bg-light mt-1" role="tablist">
                <li class="nav-item waves-effect waves-light">
                    <a class="nav-link" href="javascript:void(0)" role="tab" 
                        :class="{ 'active': tab === 'active' }"
                        wire:click='active' 
                        @click="tab = 'active'">
                        <span class="d-block d-sm-none"><i class="uil uil-filter"></i></span>
                        <span class="d-none d-sm-block"><i class="uil uil-filter text-warning"></i> Active Leads</span>
                    </a>
                </li>
                <li class="nav-item waves-effect waves-light">
                    <a class="nav-link" href="javascript:void(0)" role="tab" 
                        :class="{ 'active': tab === 'hot' }"
                        wire:click='hot'
                        @click="tab = 'hot'"> 
                        <span class="d-block d-sm-none"><i class="uil uil-bell"></i></span>
                        <span class="d-none d-sm-block"><i class="uil uil-bell text-danger"></i> Hot</span>
                    </a>
                </li>
                <li class="nav-item waves-effect waves-light">
                    <a class="nav-link" href="javascript:void(0)" role="tab" 
                        :class="{ 'active': tab === 'pending' }"
                        wire:click='pending'
                        @click="tab = 'pending'">
                        <span class="d-block d-sm-none"><i class="uil uil-clock"></i></span>
                        <span class="d-none d-sm-block"><i class="uil uil-clock text-dark"></i> Pending</span>
                    </a>
                </li>
                <li class="nav-item waves-effect waves-light">
                    <a class="nav-link" href="javascript:void(0)" role="tab"
                        :class="{ 'active': tab === 'converted' }"
                        wire:click='converted'
                        @click="tab = 'converted'">
                        <span class="d-block d-sm-none"><i class="uil uil-check-circle"></i></span>
                        <span class="d-none d-sm-block"><i class="uil uil-check-circle text-success"></i> Converted</span>
                    </a>
                </li>
                <li class="nav-item waves-effect waves-light">
                    <a class="nav-link" href="javascript:void(0)" role="tab"
                        :class="{ 'active': tab === 'dead' }"
                        wire:click='dead'
                        @click="tab = 'dead'">
                        <span class="d-block d-sm-none"><i class="uil uil-filter-slash"></i></span>
                        <span class="d-none d-sm-block"><i class="uil uil-filter-slash text-danger"></i> Dead</span>
                    </a>
                </li>
            </ul>
        </div>
    
        <div class="col-md-2">
            @can('create leads')
                <a href="{{ route('leads.create') }}" class="btn btn-success btn-rounded waves-effect waves-light float-right btn-block mt-1" data-turbolinks="true" >
                    <i class="uil uil-plus-circle me-1"></i> Create New Lead
                </a>
            @else
                &nbsp;
            @endcan
        </div>
    
    </div>
    <hr>
    
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="spinner-border text-primary" wire:loading role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>

        <div class="col-lg-12" wire:loading.remove>
            <table class="table table-centered table-hover datatable dt-responsive nowrap table-card-list" style="border-collapse: collapse; border-spacing: 0 12px; width: 100%;">
                <thead>
                    <tr class="bg-transparent">
                        <th style="width: 160px;">Name</th>
                        <th>Inquiry for</th>
                        <th>Next Followup Date</th>
                        <th>Last Followup</th>
                        <th>Agent</th>
                        <th>Opportunity Cost</th>
                        <th>Stage</th>
                        <th style="width: 120px;">Action</th>
                    </tr>
                </thead>
                <tbody>
                @if($leads->isNotEmpty())
                    @foreach($leads as $key => $lead)
                    <tr>
                        <td>
                            <div class="d-flex align-items-center">
                                <div><img src="{{ Avatar::create($lead->profile->full_name)->toBase64() }}" class="img-thumbnail rounded-circle avatar-sm mr-1" /> </div>
                                <div class="">
                                  <p class="font-weight-bold mb-0">{{ $lead->profile->full_name }}</p>
                                  <p class="text-muted mb-0">{{ $lead->profile->phone }}</p>
                                </div>
                            </div>
                        </td>
                        <td>{{ $lead->inquiry_for }}</td>
                        <td>
                            <a href="javascript:void(0)">
                            {{ date('M d, Y h:i A', strtotime($lead->next_followup)) }}
                            </a>
                        </td>
                        <td>
                            {{ \Carbon\Carbon::parse($lead->last_followup)->diffForHumans() }}
                            on
                            {{ date('M d, Y', strtotime($lead->last_followup))}}
                        </td>
                        <td>
                            <div class="avatar-group avatar-group-sm avatar-group-overlapped mr-40 mt-25">
                                    <a href="javascript:void(0)" title="{{ ucwords($lead->agent->full_name) }}" data-toggle="tooltip" data-placement="bottom">
                                        <img src="{{ Avatar::create($lead->agent->full_name)->toBase64() }}" alt="user" class="img-thumbnail align-self-start rounded-circle avatar-sm">
                                    </a>
                            </div>
                        </td>
                        <td><i class="uil uil-rupee-sign"></i>{{ $lead->price_quoted }}.00</td>
                        <td>
                            <button
                                @if($lead->status == 'Follow Up')
                                    class="btn btn-warning modal-link rounded-10 btn-sm"
                                @elseif($lead->status == 'Dead')
                                    class="btn btn-danger modal-link rounded-10  btn-sm"
                                @else
                                    class="btn btn-success modal-link rounded-10 btn-sm"
                                @endif
                                    data-title="Change Lead Stage/Status" data-href="{{ route('leads.status', $lead->id) }}"
                                    title="Change Lead Stage/Status" data-toggle="tooltip">
                                    {{ $lead->status }} <i class="uil uil-angle-right"></i>
                            </button>
                        </td>
                        <td>
                            <div class="btn-group" role="group" aria-label="Controls">
                                @if($lead->status !== 'Converted')
                                    <button class="btn text-success modal-link" data-toggle="tooltip" data-placement="bottom"
                                        data-title="Add Followup" data-href="{{ route('leads.followup', $lead->id) }}"
                                        title="Add Followup"><i class="uil uil-plus font-size-18"></i>
                                    </button>
                                @else
                                    <button class="btn text-warning modal-link" data-toggle="tooltip" data-placement="bottom"
                                        data-title="Lead Activity Timeline" data-href="{{ route('leads.show', $lead->id) }}"
                                        title="Lead Activity Timeline"><i class="uil uil-history font-size-18"></i>
                                    </button>
                                @endif
                                
                                @can('edit leads')
                                <a href="{{ route('leads.edit', $lead->id ) }}" class="btn text-warning" data-toggle="tooltip" data-placement="bottom"
                                    title="Edit Details" data-turbolinks="true"><i class="uil uil-edit-alt font-size-18"></i></a>
                                @endcan

                                <button class="btn text-primary  modal-link" data-toggle="tooltip" data-placement="bottom"
                                    data-title="Lead Details" data-href="{{ route('leads.show', $lead->id) }}"
                                    title="Lead Details"><i class="uil uil-info-circle font-size-18"></i>
                                </button>
    
                                @if(Auth::user()->hasRole('admin'))
                                <a href="{{ route('leads.destroy', $lead->id) }}" class="btn text-danger" data-toggle="tooltip" data-placement="bottom"
                                    title="Delete Lead Data"><i class="uil uil-trash-alt font-size-18"></i></a>
                                @endif
                            </div>
                        </td>
                        </tr>
                        @endforeach
                    @else
                        <h2>No Data found!</h2>
                    @endif
                </tbody>
            </table>
        </div>
    </div>

</div>
