<div>
    <div class="card">
        <div class="card-body">

            <ul class="nav nav-tabs nav-tabs-custom" id="myTab" role="tablist">
                <li class="nav-item" role="presentation">
                    <a class="nav-link tx-bold active" id="followup-tab" data-toggle="tab" href="#followup" role="tab"
                        aria-controls="details" aria-selected="true">Today</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link tx-bold" id="details-tab" data-toggle="tab" href="#details" role="tab"
                        aria-controls="followup" aria-selected="false">Reminders</a>
                </li>
            </ul>

            <div class="tab-content" id="myTabContent">

                <div class="tab-pane fade show active" id="followup" role="tabpanel" aria-labelledby="followup-tab">
                <x-alert />
                <table class="table table-centered table-hover datatable dt-responsive nowrap table-card-list mt-1"
                    style="border-collapse: collapse; border-spacing: 0 12px; width: 100%;">
                    <thead>
                        <tr class="bg-transparent">
                            <th style="width: 220px;">Name</th>
                            <th>Inquiry for</th>
                            <th>Next Followup Date</th>
                            <th>Last Followup</th>
                            <th>Opportunity Cost</th>
                            <th>Stage</th>
                            <th style="width: 120px;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($leads as $key => $lead)
                        <tr>
                            <td>
                                <div class="d-flex align-items-center">
                                    <div><img src="{{ Avatar::create($lead->profile->full_name)->toBase64() }}"
                                            class="img-thumbnail rounded-circle avatar-sm mr-1" /> </div>
                                    <div class="">
                                        <p class="font-weight-bold mb-0">{{ $lead->profile->full_name }}</p>
                                        <p class="text-muted mb-0">{{ $lead->profile->phone }}</p>
                                    </div>
                                </div>
                            </td>
                            <td>{{ $lead->inquiry_for }}</td>
                            <td>
                                <a href="javascript:void(0)">
                                    {{ date('M d, Y h:i A', strtotime($lead->next_followup)) }}
                                </a>
                            </td>
                            <td>
                                {{ \Carbon\Carbon::parse($lead->last_followup)->diffForHumans() }}
                                on
                                {{ date('M d, Y', strtotime($lead->last_followup))}}
                            </td>
                            <td><i class="uil uil-rupee-sign"></i>{{ $lead->price_quoted }}.00</td>
                            <td>
                                <button @if($lead->status == 'Follow Up')
                                    class="btn btn-warning modal-link rounded-10 btn-sm"
                                    @elseif($lead->status == 'Dead')
                                    class="btn btn-danger modal-link rounded-10 btn-sm"
                                    @else
                                    class="btn btn-success modal-link rounded-10 btn-sm"
                                    @endif
                                    data-title="Change Lead Stage/Status"
                                    data-href="{{ route('leads.status', $lead->id) }}"
                                    title="Change Lead Stage/Status" data-toggle="tooltip">
                                    {{ $lead->status }} <i class="uil uil-angle-right"></i>
                                </button>
                            </td>
                            <td>
                                <div class="btn-group" role="group" aria-label="Controls">
                                    @if($lead->status !== 'Converted')
                                    <button class="btn text-success modal-link" data-toggle="tooltip"
                                        data-placement="bottom" data-title="Add Followup"
                                        data-href="{{ route('leads.followup', $lead->id) }}" title="Add Followup"><i
                                            class="uil uil-plus font-size-18"></i>
                                    </button>
                                    @else
                                    <button class="btn text-warning modal-link" data-toggle="tooltip"
                                        data-placement="bottom" data-title="Lead Activity Timeline"
                                        data-href="{{ route('leads.show', $lead->id) }}"
                                        title="Lead Activity Timeline"><i class="uil uil-history font-size-18"></i>
                                    </button>
                                    @endif

                                    @can('edit leads')
                                    <a href="{{ route('leads.edit', $lead->id ) }}" class="btn text-warning"
                                        data-toggle="tooltip" data-placement="bottom" title="Edit Details"
                                        data-turbolinks="true"><i class="uil uil-edit-alt font-size-18"></i></a>
                                    @endcan

                                    <button class="btn text-primary  modal-link" data-toggle="tooltip"
                                        data-placement="bottom" data-title="Lead Details"
                                        data-href="{{ route('leads.show', $lead->id) }}" title="Lead Details"><i
                                            class="uil uil-info-circle font-size-18"></i>
                                    </button>

                                    @if(Auth::user()->hasRole('admin'))
                                    <a href="{{ route('leads.destroy', $lead->id) }}" class="btn text-danger"
                                        data-toggle="tooltip" data-placement="bottom" title="Delete Lead Data"><i
                                            class="uil uil-trash-alt font-size-18"></i></a>
                                    @endif
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>

                <div class="tab-pane fade" id="details" role="tabpanel" aria-labelledby="details-tab">
                    @if($reminders->isNotEmpty())
                    <div class="table-responsive">
                        <table class="table table-borderless table-centered mt-2">
                            <tbody class="mb-5">
                                @foreach ($reminders as $remind)
                                    <tr>
                                        <td style="width: 20px;"><img src="{{ Avatar::create($remind->name)->toBase64() }}" class="avatar-xs rounded-circle " alt="..."></td>
                                        <td>
                                            <h6 class="font-size-15 mb-1 fw-normal">{{ ucwords($remind->name) }}</h6>
                                            <p class="text-muted font-size-13 mb-0"><i class="uil uil-gift me-2 text-success"></i> Lead</p>
                                        </td>
                                        <td class="text-muted fw-semibold text-end">
                                            {{ $remind->dob->format('d M ,Y') }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    @else
                    <h3 class="mt-2">No Reminders</h3>
                    @endif
                </div>

            </div>


        </div>
    </div>
</div>
