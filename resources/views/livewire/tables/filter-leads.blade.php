<div>
    <hr>    
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="spinner-border text-primary" wire:loading role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>

        <div class="col-lg-12" wire:loading.remove>
            <table class="table table-centered table-hover datatable dt-responsive nowrap table-card-list" style="border-collapse: collapse; border-spacing: 0 12px; width: 100%;">
                <thead>
                    <tr class="bg-transparent">
                        <th style="width: 160px;">Name</th>
                        <th>Inquiry for</th>
                        <th>Next Followup Date</th>
                        <th>Last Followup</th>
                        <th>Agent</th>
                        <th>Opportunity Cost</th>
                        <th>Stage</th>
                        <th style="width: 120px;">Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
