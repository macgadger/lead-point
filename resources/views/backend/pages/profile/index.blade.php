@extends('layouts.app')
@section('title', 'Your Account')
@section('turbolink','no-cache')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">

            @role('admin')
            <div class="row mb-4">
                <div class="col-xl-4">
                    <div class="card-body">
                        <div class="card h-100">
                            <div class="card-body">
                                <div class="text-center">
                                    <div>
                                        @if($profile->avatar === 'default')
                                        <img src="{{ Avatar::create(Auth::user()->profile->full_name)->toBase64() }}"
                                            alt="" class="avatar-lg rounded-circle img-thumbnail">
                                        @else
                                        <img src="{{ $profile->asset_avatar }}" alt=""
                                            class="avatar-lg rounded-circle img-thumbnail">
                                        @endif
                                    </div>
                                    <h5 class="mt-3 mb-1">{{ $profile->full_name }}</h5>
                                </div>
                                <hr class="my-4">
                                <div class="table-responsive mt-4 text-center">
                                    <div>
                                        <p class="mb-1">Plan Type</p>
                                        <h5 class="font-size-16">{{ strtoupper($profile->plan->name) }}</h5>
                                    </div>
                                    <div>
                                        <p class="mb-1">Users Limit</p>
                                        <h5 class="font-size-16">{{ $profile->plan->users_limit }}</h5>
                                    </div>
                                    <div>
                                        <p class="mb-1">Leads Limit</p>
                                        <h5 class="font-size-16">{{ $profile->plan->leads_limit }}</h5>
                                    </div>
                                </div>
                                <div class="mt-4">
                                    <button type="button" class="btn btn-warning btn-sm btn-block"><i
                                            class="uil uil-rocket mr-2"></i></i> Upgrade Plan</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-8">
                @endrole

                    <div class="card-body">
                        <x-validation />
                        <!-- Card Body -->
                        <form name="profileForm" method="post" action="{{ route('profile.update', $profile->id ) }}"
                            enctype="multipart/form-data" data-parsley-validate>
                            @csrf
                            <div class="form-layout">
                                <div class="row mg-b-25">

                                    @role('agent')
                                    <div class="col-lg-2 text-center">
                                        @if($profile->avatar === 'default')
                                        <img src="{{ Avatar::create(Auth::user()->profile->full_name)->toBase64() }}"
                                            alt="" class="img-thumbnail rounded-circle avatar-xl">
                                        @else
                                        <img src="{{ $profile->asset_avatar }}" alt=""
                                            class="img-thumbnail rounded-circle avatar-xl">
                                        @endif
                                    </div>
                                    @endrole

                                    <div class="col-lg-10">
                                        <div class="form-group">
                                            <label class="form-control-label">Update Profile Picture:</label>
                                            <input class="form-control" type="file" name="user_img" value="">
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <hr>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label">Email: <span
                                                    class="text-danger">*</span></label>
                                            <input class="form-control" type="email" name="email"
                                                value="{{ $profile->email }}" placeholder="Enter Email/Username"
                                                required>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">&nbsp;</div>

                                    @role('admin')
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label">Company Name:</label>
                                            <input class="form-control" type="text" name="company_name"
                                                value="{{ $profile->company_name }}" placeholder="Enter Company Name">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label">Website:</label>
                                            <input class="form-control" type="text" name="website"
                                                value="{{ $profile->website }}" placeholder="Enter Company Website">
                                        </div>
                                    </div>
                                    @endrole

                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label">Firstname: <span
                                                    class="text-danger">*</span></label>
                                            <input class="form-control" type="text" name="first_name"
                                                style="text-transform:capitalize" value="{{ $profile->first_name }}"
                                                minlength="3" placeholder="Enter firstname" required>
                                        </div>
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label">Lastname:</label>
                                            <input class="form-control" type="text" name="last_name"
                                                style="text-transform:capitalize" value="{{ $profile->last_name }}"
                                                placeholder="Enter lastname" required>
                                        </div>
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label">Mobile Number: <span
                                                    class="text-danger">*</span></label>
                                            <input class="form-control" type="phone" name="phone"
                                                value="{{ $profile->phone }}" minlength="10" maxlength="13"
                                                placeholder="Enter Mobile Number" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="form-control-label">Address:</label>
                                            <textarea class="form-control" name="address"
                                                placeholder="Enter Address">{{ $profile->address }}</textarea>
                                        </div>
                                    </div>

                                    <x-locations :model="$profile" />

                                </div><!-- row -->

                                <div class="form-layout-footer">
                                    <button type="submit" class="btn btn-primary bd-0">Update Profile</button>
                                </div><!-- form-layout-footer -->
                                <hr>
                            </div><!-- form-layout -->
                        </form>

                        <h2>Update Password</h2>
                        <hr>
                        <form name="updatePassword" method="POST" action="{{ route('profile.password',$profile->uid) }}"
                            data-parsley-validate>
                            @csrf
                            <div class="form-layout">
                                <div class="row mg-b-25">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label">Password:</label>
                                            <input class="form-control" type="password" name="password"
                                                placeholder="Enter New Password" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label">Repeat Password:</label>
                                            <input class="form-control" type="password" name="password_confirmation"
                                                placeholder="Repeat Password" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-danger bd-0">Update Password</button>
                                </div><!-- form-layout-footer -->
                            </div>
                        </form>
                    </div>
                    @role('admin')
                </div>
            </div>
            @endrole
        </div>
    </div>
</div>

@endsection

@push('scripts')
    <script>
        $('.select2').select2();
    </script>
@endpush