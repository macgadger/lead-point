@extends('layouts.app')
@section('title', 'Quick Leads')
@section('turbolink','no-cache')
@section('content')

<div class="row">
    <div class="col-12">

        <table id="datatable" class="table table-centered table-hover dt-responsive nowrap table-card-list"
            style="border-collapse: collapse; border-spacing: 0 12px; width: 100%;">
            <thead>
                <tr class="bg-transparent">
                    <th>Name</th>
                    <th>Phone</th>
                    <th>Email</th>
                    <th>Inquiry for</th>
                    <th>Price Quoted</th>
                    <th>Agent</th>
                    <th style="width: 120px;">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($quickLeads as $lead)
                <tr>
                    <td>
                        <img src="{{ Avatar::create($lead->name)->toBase64() }}"
                            class="img-thumbnail rounded-circle avatar-sm mr-1" />
                        {{ ucwords($lead->name) }}
                    </td>
                    <td>{{ $lead->phone }}</td>
                    <td><span class="">{{ $lead->email ?? '---' }}</span></td>
                    <td><span class="">{{ $lead->inquiry_for ?? '---' }}</span></td>
                    <td><span class=""><i class="uil uil-rupee-sign"></i>{{ $lead->price_quoted ?? '---' }}.00</span></td>
                    <td>
                        <div class="avatar-group avatar-group-sm avatar-group-overlapped mr-40 mt-25">
                            <a href="javascript:void(0)" title="{{ ucwords($lead->createdBy->full_name) }}" data-toggle="tooltip" data-placement="bottom">
                                <img src="{{ Avatar::create($lead->createdBy->full_name)->toBase64() }}" alt="user" class="img-thumbnail align-self-start rounded-circle avatar-sm">
                            </a>
                        </div>
                    </td>
                    <td>
                        <a class="btn text-primary" data-toggle="tooltip" 
                            title="Move Quick Lead to Active Lead"
                            href="{{ route('leads.move',$lead->id) }}">
                            <i class="uil uil-repeat font-size-18"></i>
                        </a>
                        @role('admin')
                        <a class="btn text-danger"  href="{{ route('quick-leads.destroy',$lead->id) }}">
                            <i class="uil uil-trash-alt font-size-18"></i>
                        </a>
                        @endrole
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<!-- end row -->

@endsection

@push('scripts')
    <script>
        $(function () {
            $('#datatable').DataTable({
                'columnDefs': [ {
                    'targets'  : [1,2,3,5,6], /* column index */
                    'orderable': false, /* true or false */
                }],
            });
        });
    </script>
@endpush