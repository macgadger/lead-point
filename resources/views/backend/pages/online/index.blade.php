@extends('layouts.app')
@section('title', 'Online Users')
@section('turbolink','no-cache')
@section('content')
<div class="row">
    <div class="col-md-12">
       <h1>Your IP Address: {{ \Request::getClientIp() }}
        <hr>
    </div>
</div>
<div class="row">
@if(isset($employees) && $employees->isNotEmpty())
    @foreach($employees as $key => $employee)
        @if($employee->user->hasRole('agent'))
        <div class="col-xl-2 col-sm-6">
            <div class="card text-center">
                <div class="card-body">
                    <div class="clearfix"></div>
                    <div class="mb-4">
                        @if($employee->avatar === 'default')
                            <img src="{{ Avatar::create($employee->full_name)->toBase64() }}"  alt="" class="avatar-lg rounded-circle img-thumbnail">
                        @else
                        <img src="{{ $employee->asset_avatar }}"  alt="" class="avatar-lg rounded-circle img-thumbnail">
                        @endif
                    </div>
                    <h5 class="font-size-16 mb-1"><a href="#" class="text-dark">{{ $employee->full_name }}</a></h5>
                    @if(Cache::has('user-is-online-' . $employee->user->id))
                        <span class="badge rounded-pill bg-success text-light">Online</span>
                    @else
                        <span class="badge rounded-pill bg-danger text-light">Offline</span>
                    @endif
                    <p class="text-muted mb-2 mt-1">IP Address: {{ $employee->user->last_login_ip ?? 'Not available'}}</p>
                    <p class="text-muted mb-2 mt-1">Location: Not Available</p>
                    <p class="text-muted mb-2">Last seen: {{ \Carbon\Carbon::parse($employee->user->last_seen)->diffForHumans() ?? 'Not' }}</p>
                </div>

                @if($employee->user->status == 2)
                    <a href="{{ route('profile.employee.access',['id' => $employee->user->id ,'type' => 'unblock']) }}" data-turbolinks="false" class="btn btn-outline-success text-truncate btn-block">
                        <i class="uil uil-signout me-1"></i> Unblock User
                    </a>
                @else
                    <a href="{{ route('profile.employee.access',['id' => $employee->user->id ,'type' => 'block']) }}" data-turbolinks="false" class="btn btn-outline-danger text-truncate btn-block">
                        <i class="uil uil-signout me-1"></i> Block User
                    </a>
                @endif
                
            </div>
        </div>
        @endif
    @endforeach
@endif
</div>
@endsection

@push('scripts')
    <script>
        $(function () {
            $('#datatable').DataTable({
                retrieve: true,
                'columnDefs': [ {
                    'targets'  : [1,2,3,5], /* column index */
                    'orderable': false, /* true or false */
                }],
            });
        });
    </script>
@endpush
