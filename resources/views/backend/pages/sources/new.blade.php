<div class="card p-3">
    <form name="leadsourceForm" method="post" action="{{ route('lead-sources.store') }}" data-parsley-validate>
        @csrf
        <div class="form-layout">
            <div class="row mg-b-25">
                
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="form-control-label">Source Name:</label>
                        <input class="form-control" type="text" name="name" value="{{ old('name') }}"
                            minlength="3" placeholder="Enter New Source Name" required>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label class="form-control-label">Status:</label>
                        <select class="form-control" name="status" required>
                            <option value="1" selected>Active</option>
                            <option value="0">Inactive</option>
                        </select>
                    </div>
                </div>
                
            </div>
        </div>
        <div class="form-layout-footer mg-l-15 float-right">
            <button class="btn btn-primary bd-0">Add Source</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div><!-- form-layout-footer -->

    </form>
</div>