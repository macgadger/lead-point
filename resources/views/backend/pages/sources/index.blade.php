@extends('layouts.app')
@section('title', 'Lead Sources')
@section('turbolink','no-cache')
@section('content')
<div class="row">
    <div class="col-md-12">
        <button class="btn btn-success btn-rounded waves-effect waves-light mb-3 modal-link"
        data-href="{{ route('lead-sources.create') }}" data-title="Add Lead Source">
            <i class="uil uil-plus-circle me-1"></i> Add New Source
        </button>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="table-wrapper">
            <table id="datatable"
                class="table table-condensed table-centered table-hover datatable dt-responsive nowrap table-card-list"
                style="border-collapse: collapse; border-spacing: 0 12px; width: 100%;">
                <thead>
                    <tr class="bg-transparent">
                        <th class="wd-15p">#</th>
                        <th class="wd-15p">Name</th>
                        <th class="wd-10p">Status</th>
                        <th class="wd-20p">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($sources as $key => $source)
                    <tr>
                        <td>{{ $key+1 }}</td>
                        <td>{{ ucfirst($source->name) }}</td>
                        <td>
                            @if($source->status)
                                <div class="badge bg-soft-success font-size-12">Active</div>
                            @else
                                <div class="badge bg-soft-danger font-size-12">Inactive</div>
                            @endif
                        </td>
                        <td>
                            <button class="btn px-3 text-primary modal-link"
                                data-href="{{ route('lead-sources.edit',$source->id) }}" data-title="Edit Lead Source">
                                <i class="uil uil-pen font-size-18"></i>
                            </button>
                            <button class="btn px-3 text-danger">
                                <i class="uil uil-trash-alt font-size-18"></i>
                            </button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function () {
            $('#datatable').DataTable({
                'columnDefs': [ {
                    'targets'  : [2,3], /* column index */
                    'orderable': false, /* true or false */
                }],
            });
        });
    </script>
@endpush