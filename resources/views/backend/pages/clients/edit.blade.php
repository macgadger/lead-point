@extends('layouts.app')
@section('title', 'Edit Client')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">

                <x-validation />
                <!-- Card Body -->
                <form name="leadForm" method="post" action="{{ route('clients.update',$client->id) }}" data-parsley-validate>
                    @csrf
                    @method('PATCH')

                    <div class="form-layout">
                        <div class="row mg-b-25">

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="form-control-label">Full Name: <span
                                            class="text-danger">*</span></label>
                                    <input class="form-control" type="text" name="name"
                                        style="text-transform:capitalize" value="{{ $client->name }}" minlength="3"
                                        placeholder="Enter Full name" required>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="form-control-label">Mobile Number: <span
                                            class="text-danger">*</span></label>
                                    <input class="form-control" type="phone" name="phone" value="{{ $client->phone }}"
                                        minlength="10" maxlength="13" placeholder="Enter Mobile Number" required>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="form-control-label">Alt Phone Number:</label>
                                    <input class="form-control" type="tel" name="alt_phone"
                                        value="{{ $client->alt_phone }}" minlength="10" maxlength="13"
                                        placeholder="Enter Alternate Phone Number">
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="form-control-label">Company Name:</label>
                                    <input class="form-control" type="text" name="company"
                                        style="text-transform:capitalize" value="{{ $client->company }}"
                                        minlength="3" placeholder="Enter company name">
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div id="slWrapperBirth" class="parsley-select">
                                    <div class="form-group">
                                        <label class="form-control-label">Date of Birth: <span
                                                class="text-danger">*</span></label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">
                                                    <i class="uil uil-schedule text-16 lh-0 op-6"></i>
                                                </div>
                                            </div>
                                            <input type="text" data-parsley-class-handler="#slWrapperBirth"
                                                data-parsley-errors-container="#slErrorContainerBirth"
                                                class="form-control fc-dobpicker" value="{{ date('d-m-Y',strtotime($client->dob)) }}" name="dob"
                                                placeholder="Date of Birth" autocomplete="off" required>
                                        </div><!-- wd-200 -->
                                        <div id="slErrorContainerBirth"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="form-control-label">Email:</label>
                                    <input class="form-control" type="email" name="email" value="{{ $client->email }}"
                                        placeholder="Enter Email Address">
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="form-control-label">Address: <span class="text-danger">*</span></label>
                                    <textarea class="form-control" name="address" rows="2" placeholder="Enter Address"
                                        required>{{ $client->address }}</textarea>
                                </div>
                            </div>

                            <x-locations :model="$client"  />


                            <div class="col-md-12">
                                <hr>
                            </div>

                        </div><!-- row -->

                        <div class="form-layout-footer">
                            <button class="btn btn-primary bd-0">Add Client</button>
                        </div><!-- form-layout-footer -->
                    </div><!-- form-layout -->
                </form>
                <!-- Card Body -->
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    $(function () {
        // $('.parsley-validate').parsley();
        $('.select2').select2();
        $('.fc-dobpicker').datetimepicker({
            timepicker: false,
            format:'d-m-Y',
            formatDate:'Y-m-d',
            formatTime:'H:i',
            maxDate:'{{ date("Y/m/d") }}',
        });
    });
</script>
@endpush
