@extends('layouts.app')
@section('title', 'Your Clients')
@section('turbolink','no-cache')
@section('content')
<div class="row">
    <div class="col-12">
        <a href="{{ route('clients.create') }}" class="btn btn-success btn-rounded waves-effect waves-light mb-3 float-right">
            <i class="uil uil-plus-circle me-1"></i> Add New
        </a>
    </div>
</div>

<div class="row">
    <div class="col-12">

        <table id="datatable" class="table table-centered table-hover dt-responsive nowrap table-card-list"
            style="border-collapse: collapse; border-spacing: 0 12px; width: 100%;">
            <thead>
                <tr class="bg-transparent">
                    <th>Name</th>
                    <th>Phone</th>
                    <th>Email</th>
                    <th>City</th>
                    <th>Status</th>
                    <th style="width: 120px;">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($clients as $client)
                <tr>
                    <td>
                        <img src="{{ Avatar::create($client->name)->toBase64() }}"
                            class="img-thumbnail rounded-circle avatar-sm mr-1" />
                        {{ ucwords($client->name) }}
                    </td>
                    <td>{{ $client->phone }}</td>
                    <td><span class="">{{ $client->email ?? 'None' }}</span></td>
                    <td>{{ ucwords($client->city) }}</td>
                    <td>
                        @if($client->status == 1)
                        <div class="badge bg-soft-success font-size-12">Active</div>
                        @else
                        <div class="badge bg-soft-success font-size-12">Suspended</div>
                        @endif
                    </td>
                    <td>
                        <a class="btn text-primary" href="{{ route('clients.edit',$client->id) }}">
                            <i class="uil uil-pen font-size-18"></i>
                        </a>
                        @role('admin')
                        <a class="btn text-danger"  href="{{ route('clients.destroy',$client->id) }}">
                            <i class="uil uil-trash-alt font-size-18"></i>
                        </a>
                        @endrole
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<!-- end row -->

@endsection

@push('scripts')
    <script>
        $(function () {
            $('#datatable').DataTable({
                'columnDefs': [ {
                    'targets'  : [2,3,4,5], /* column index */
                    'orderable': false, /* true or false */
                }],
            });
        });
    </script>
@endpush