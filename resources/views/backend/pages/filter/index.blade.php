@extends('layouts.app')
@section('title', 'Leads')
@section('content')
<div class="card-body">

    @include('backend.pages.filter.tab',['type' => $type])

    <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade show active" id="details" role="tabpanel" aria-labelledby="details-tab">
            <div class="table-wrapper" style="margin-top: 20px">
                <table class="table table-bordered dt-responsive nowrap datatable"
                    style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                    <thead>
                        <tr>                                                        
                            <th class="wd-15p">Name</th>
                            <th class="wd-20p">Inquiry for</th>
                            <th class="wd-10p">Next Followup Date</th>
                            <th class="wd-10p">Agent</th>
                            <th class="wd-10p">Opportunity Cost</th>
                            <th class="wd-10p">Stage</th>
                            <th class="wd-20p">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($today->isNotEmpty())
                        @foreach($today as $key => $lead)
                        <tr>                                                        
                            <td>
                                <div class="d-flex align-items-center">
                                    <div><img src="{{ Avatar::create($lead->profile->full_name)->toBase64() }}" class="img-thumbnail rounded-circle avatar-sm mr-1" /> </div>
                                    <div class="">
                                      <p class="font-weight-bold mb-0">{{ $lead->profile->full_name }}</p>
                                      <p class="text-muted mb-0">{{ $lead->profile->phone }}</p>
                                    </div>
                                </div>
                            </td>
                            <td>{{ $lead->inquiry_for }}</td>
                            <td>
                                <a href="#">
                                    {{ date('M d, Y h:i A', strtotime($lead->next_followup)) }}
                                </a>
                            </td>
                            <td>
                                <div class="avatar-group avatar-group-sm avatar-group-overlapped mr-40 mt-25">
                                    <a href="javascript:void(0)" title="{{ ucwords($lead->agent->full_name) }}" data-toggle="tooltip" data-placement="bottom">
                                        <img src="{{ Avatar::create($lead->agent->full_name)->toBase64() }}" alt="user" class="img-thumbnail align-self-start rounded-circle avatar-sm">
                                    </a>
                                </div>
                            </td>
                            <td>Rs {{ $lead->price_quoted }}.00</td>
                            <td>
                                <button
                                @if($lead->status == 'Follow Up')
                                    class="btn btn-warning modal-link rounded-10 btn-sm"
                                @elseif($lead->status == 'Dead')
                                    class="btn btn-danger modal-link rounded-10  btn-sm"
                                @else
                                    class="btn btn-success modal-link rounded-10 btn-sm"
                                @endif
                                    data-title="Change Lead Stage/Status" data-href="{{ route('leads.status', $lead->id) }}"
                                    title="Change Lead Stage/Status" data-toggle="tooltip">
                                    {{ $lead->status }} <i class="uil uil-angle-right"></i>
                                </button>
                            </td>
                            <td>
                                <div class="btn-group" role="group" aria-label="Controls">
                                    @if($lead->status !== 'Converted')
                                        <button class="btn btn-success btn-sm modal-link" data-toggle="tooltip" data-placement="bottom"
                                            data-title="Add Followup" data-href="{{ route('leads.followup', $lead->id) }}"
                                            title="Add Followup"><i class="uil uil-plus"></i>
                                        </button>
                                    @else
                                        <button class="btn btn-warning btn-sm modal-link" data-toggle="tooltip" data-placement="bottom"
                                            data-title="Lead Activity Timeline" data-href="{{ route('leads.timeline', $lead->id) }}"
                                            title="Lead Activity Timeline"><i class="uil uil-history"></i>
                                        </button>
                                    @endif
                                    <button class="btn btn-primary btn-sm modal-link" data-toggle="tooltip"
                                        data-placement="bottom" data-title="Lead Details"
                                        data-href="{{ route('leads.show', $lead->id) }}" title="Lead Details"><i
                                            class="uil uil-info-circle"></i>
                                    </button>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>

        <div class="tab-pane fade" id="followup" role="tabpanel" aria-labelledby="followup-tab">
            <div class="table-wrapper" style="margin-top: 20px">
                <table class="table table-centered table-hover datatable dt-responsive nowrap table-card-list" style="border-collapse: collapse; border-spacing: 0 12px; width: 100%;">
                    <thead>
                        <tr>                                                        
                            <th class="wd-15p">Name</th>
                            <th class="wd-20p">Inquiry for</th>
                            <th class="wd-10p">Next Followup Date</th>
                            <th class="wd-10p">Agent</th>
                            <th class="wd-10p">Opportunity Cost</th>
                            <th class="wd-10p">Stage</th>
                            <th class="wd-20p">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($month->isNotEmpty())
                        @foreach($month as $key => $lead)
                        <tr>                            
                            <td>
                                <div class="d-flex align-items-center">
                                    <div><img src="{{ Avatar::create($lead->profile->full_name)->toBase64() }}" class="img-thumbnail rounded-circle avatar-sm mr-1" /> </div>
                                    <div class="">
                                      <p class="font-weight-bold mb-0">{{ $lead->profile->full_name }}</p>
                                      <p class="text-muted mb-0">{{ $lead->profile->phone }}</p>
                                    </div>
                                </div>
                            </td>
                            <td>{{ $lead->inquiry_for }}</td>
                            <td>
                                <a href="#">
                                    {{ date('M d, Y h:i A', strtotime($lead->next_followup)) }}
                                </a>
                            </td>
                            <td>
                                <div class="avatar-group avatar-group-sm avatar-group-overlapped mr-40 mt-25">
                                    <a href="javascript:void(0)" title="{{ ucwords($lead->agent->full_name) }}" data-toggle="tooltip" data-placement="bottom">
                                        <img src="{{ Avatar::create($lead->agent->full_name)->toBase64() }}" alt="user" class="img-thumbnail align-self-start rounded-circle avatar-sm">
                                    </a>
                                </div>
                            </td>
                            <td>Rs {{ $lead->price_quoted }}.00</td>
                            <td>
                                <button
                                @if($lead->status == 'Follow Up')
                                    class="btn btn-warning modal-link rounded-10 btn-sm"
                                @elseif($lead->status == 'Dead')
                                    class="btn btn-danger modal-link rounded-10  btn-sm"
                                @else
                                    class="btn btn-success modal-link rounded-10 btn-sm"
                                @endif
                                    data-title="Change Lead Stage/Status" data-href="{{ route('leads.status', $lead->id) }}"
                                    title="Change Lead Stage/Status" data-toggle="tooltip">
                                    {{ $lead->status }} <i class="uil uil-angle-right"></i>
                                </button>
                            </td>
                            <td>
                                <div class="btn-group" role="group" aria-label="Controls">
                                    @if($lead->status !== 'Converted')
                                        <button class="btn btn-success btn-sm modal-link" data-toggle="tooltip" data-placement="bottom"
                                            data-title="Add Followup" data-href="{{ route('leads.followup', $lead->id) }}"
                                            title="Add Followup"><i class="uil uil-plus"></i>
                                        </button>
                                    @else
                                        <button class="btn btn-warning btn-sm modal-link" data-toggle="tooltip" data-placement="bottom"
                                            data-title="Lead Activity Timeline" data-href="{{ route('leads.timeline', $lead->id) }}"
                                            title="Lead Activity Timeline"><i class="uil uil-history"></i>
                                        </button>
                                    @endif
                                    <button class="btn btn-primary btn-sm modal-link" data-toggle="tooltip"
                                        data-placement="bottom" data-title="Lead Details"
                                        data-href="{{ route('leads.show', $lead->id) }}" title="Lead Details"><i
                                            class="uil uil-info-circle"></i>
                                    </button>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@push('styles')
    <link href="{{ asset('assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/libs/avatars/avatars.min.css') }}" rel="stylesheet">
@endpush

@push('scripts')
    <script src="{{ asset('assets/libs/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>

    <script src="{{ asset('assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') }}"></script>
    <script>
    $(function () {
        $('.datatable').DataTable({
            responsive: true,
            sDom: 'Rfrtlip',
            language: {
                searchPlaceholder: 'Search...',
                sSearch: '',
                lengthMenu: '_MENU_ items/page',
            },
            /*order: [
                [ 4, "desc" ]
            ],*/
            /*columnDefs: [{
                'targets': [6, 8],
                'orderable': false,
            }],*/
            fixedColumns: true
        });
    });
    </script>
@endpush