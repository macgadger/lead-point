@if($type == 'leads')
    <ul class="nav nav-tabs nav-tabs-custom" id="myTab" role="tablist">
        <li class="nav-item" role="presentation">
            <a class="nav-link tx-bold active" id="details-tab" data-toggle="tab" href="#details" role="tab"
                aria-controls="details" aria-selected="true">Today</a>
        </li>
        <li class="nav-item" role="presentation">
            <a class="nav-link tx-bold" id="followup-tab" data-toggle="tab" href="#followup" role="tab"
                aria-controls="followup" aria-selected="false">All</a>
        </li>
    </ul>
@endif

@if($type == 'followup')
    <ul class="nav nav-tabs nav-tabs-custom" id="myTab" role="tablist">
        <li class="nav-item" role="presentation">
            <a class="nav-link tx-bold active" id="details-tab" data-toggle="tab" href="#details" role="tab"
                aria-controls="details" aria-selected="true">Today Followup</a>
        </li>
        <li class="nav-item" role="presentation">
            <a class="nav-link tx-bold" id="followup-tab" data-toggle="tab" href="#followup" role="tab"
                aria-controls="followup" aria-selected="false">Pending</a>
        </li>
    </ul>
@endif

@if($type == 'converted')
    <ul class="nav nav-tabs nav-tabs-custom" id="myTab" role="tablist">
        <li class="nav-item" role="presentation">
            <a class="nav-link tx-bold active" id="details-tab" data-toggle="tab" href="#details" role="tab"
                aria-controls="details" aria-selected="true">Today</a>
        </li>
        <li class="nav-item" role="presentation">
            <a class="nav-link tx-bold" id="followup-tab" data-toggle="tab" href="#followup" role="tab"
                aria-controls="followup" aria-selected="false">This Month</a>
        </li>
    </ul>
@endif

@if($type == 'walkin')
    <ul class="nav nav-tabs nav-tabs-custom" id="myTab" role="tablist">
        <li class="nav-item" role="presentation">
            <a class="nav-link tx-bold active" id="details-tab" data-toggle="tab" href="#details" role="tab"
                aria-controls="details" aria-selected="true">Today</a>
        </li>
        <li class="nav-item" role="presentation">
            <a class="nav-link tx-bold" id="followup-tab" data-toggle="tab" href="#followup" role="tab"
                aria-controls="followup" aria-selected="false">This Month</a>
        </li>
    </ul>
@endif

@if($type == 'payment')
    <ul class="nav nav-tabs nav-tabs-custom" id="myTab" role="tablist">
        <li class="nav-item" role="presentation">
            <a class="nav-link tx-bold active" id="details-tab" data-toggle="tab" href="#details" role="tab"
                aria-controls="details" aria-selected="true">Today</a>
        </li>
        <li class="nav-item" role="presentation">
            <a class="nav-link tx-bold" id="followup-tab" data-toggle="tab" href="#followup" role="tab"
                aria-controls="followup" aria-selected="false">This Month</a>
        </li>
    </ul>
@endif
