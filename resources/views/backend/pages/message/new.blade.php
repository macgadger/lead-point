<form method="POST" action="{{ route('messages.send') }}" data-parsley-validate>
    @csrf
    <div class="form-group row mb-4">
        <label class="col col-form-label">From:</label>
        <div class="col-md-10">
            <input type="text" class="form-control" value="{{ Auth::user()->profile->full_name }}" required readonly />
        </div>
    </div>

    <div class="form-group row mb-4">
        <label class="col col-form-label">To:</label>
        <div class="col-md-10">
            <select class="form-control" name="agent_id" required>
                <option value=""> Select Agent..</option>
                @foreach($agents as $agent)
                    <option value="{{ $agent->id }}" @if($id == $agent->id) selected @endif>{{ $agent->full_name }}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="form-group">
        <input type="text" name="subject" class="form-control" placeholder="Subject" required>
    </div>
    
    <div class="form-group">
        <textarea class="form-control" id="classic-editor" name="message" rows="5" placeholder="Type your message here" required></textarea>
    </div>

    <div class="float-right">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Send <i class="uil uil-telegram-alt ml-1"></i></button>
    </div>

</form>