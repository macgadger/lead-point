<div class="card p-3">

    <div class="table-responsive">
        <div>
            <p class="mb-1 text-info">Subject :</p>
            <h5 class="font-size-16">{{ ucfirst($message->subject) }}</h5>
        </div>
        <div class="mt-2">
            <p class="mb-1 text-info">Message :</p>
            <h5 class="font-size-16">{{ ucfirst($message->message) }}</h5>
        </div>
        
    </div>

</div>

<div class="float-right">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
</div>