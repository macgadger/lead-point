@extends('layouts.app')
@section('title', 'Messages')
@section('turbolink','no-cache')
@section('content')
<div class="row">
    <div class="col-12">

        <div class="card-body bg-primary text-white mailbox-widget pb-0">
            <ul class="nav nav-tabs custom-tab border-bottom-0 mt-4" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="inbox-tab" data-toggle="tab" aria-controls="inbox" href="#inbox"
                        role="tab" aria-selected="true">
                        <span class="d-block d-md-none"><i class="ti-email"></i></span>
                        <span class="d-none d-md-block"> INBOX</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="sent-tab" data-toggle="tab" aria-controls="sent" href="#sent" role="tab"
                        aria-selected="false">
                        <span class="d-block d-md-none"><i class="ti-export"></i></span>
                        <span class="d-none d-md-block">SENT</span>
                    </a>
                </li>
            </ul>
        </div>
        <div class="tab-content" id="myTabContent">

            <div class="tab-pane fade active show" id="inbox" aria-labelledby="inbox-tab" role="tabpanel">
                <div>
                    <div class="row p-4 no-gutters align-items-center">
                        <div class="col-sm-12 col-md-6">
                            <h3 class="font-light mb-0"><i class="ti-email mr-2"></i>{{ $inbox->count() }} messages</h3>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <ul class="list-inline dl mb-0 float-left float-md-right">
                                <li class="list-inline-item text-info mr-3">
                                    <a class="modal-link" href="javascript:void(0)" data-title="Message"
                                        data-href="{{ route('messages.create') }}">
                                        <button class="btn btn-circle btn-success text-white modal-link"
                                            data-title="Message" data-href="{{ route('messages.create') }}">
                                            <i class="uil uil-plus"></i>
                                        </button>
                                        <span class="ml-2 font-normal text-dark">Compose</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- Mail list-->
                    <div class="table-responsive">
                        <table class="table email-table no-wrap table-hover v-middle mb-0 font-14">
                            <tbody>
                                @foreach($inbox as $message)
                                <tr>
                                    <!-- label -->
                                    <td class="pl-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="cst1" />
                                            <label class="custom-control-label" for="cst1">&nbsp;</label>
                                        </div>
                                    </td>
                                    <!-- star -->
                                    <td>
                                        @if($message->is_read == 0)
                                        <span class="badge badge-pill text-white font-bold badge-danger mr-2">New</span>
                                        @else
                                        <span class="badge badge-pill text-white badge-success mr-2">Seen</span>
                                        @endif
                                    </td>
                                    <td>
                                        <span
                                            class="mb-0 text-muted font-medium">{{ $message->sentBy->full_name }}</span>
                                    </td>
                                    <!-- Message -->
                                    <td>
                                        <a class="link modal-link" href="javascript: void(0)" data-title="Message"
                                            data-href="{{ route('messages.show', $message->id) }}">
                                            <span
                                                class="badge badge-pill text-white font-bold badge-info mr-2">Subject</span>
                                            <span class="text-dark"
                                                style="border-bottom: 2px dotted black">{{ $message->subject }}</span>
                                        </a>
                                    </td>
                                    <!-- Attachment -->
                                    <td><i class="fa fa-paperclip text-muted"></i></td>
                                    <!-- Time -->
                                    <td class="text-muted">{{ $message->created_at->format('d M, y') }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="tab-pane fade" id="sent" aria-labelledby="sent-tab" role="tabpanel">
                <div>
                    <div class="row p-4 no-gutters align-items-center">
                        <div class="col-sm-12 col-md-6">
                            <h3 class="font-light mb-0"><i class="ti-email mr-2"></i>{{ $sent->count() }} messages</h3>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <ul class="list-inline dl mb-0 float-left float-md-right">
                                <li class="list-inline-item text-info mr-3">
                                    <a class="modal-link" href="javascript:void(0)" data-title="Message"
                                        data-href="{{ route('messages.create') }}">
                                        <button class="btn btn-circle btn-success text-white modal-link"
                                            data-title="Message" data-href="{{ route('messages.create') }}">
                                            <i class="uil uil-plus"></i>
                                        </button>
                                        <span class="ml-2 font-normal text-dark">Compose</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- Mail list-->
                    <div class="table-responsive">
                        <table class="table email-table no-wrap table-hover v-middle mb-0 font-14">
                            <tbody>
                                @foreach($sent as $message)
                                <tr>
                                    <!-- label -->
                                    <td class="pl-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="cst1" />
                                            <label class="custom-control-label" for="cst1">&nbsp;</label>
                                        </div>
                                    </td>
                                    <!-- star -->
                                    <td>
                                        @if($message->is_read == 0)
                                        <span class="badge badge-pill text-white font-bold badge-danger mr-2">New</span>
                                        @else
                                        <span class="badge badge-pill text-white badge-success mr-2">Seen</span>
                                        @endif
                                    </td>
                                    <td>
                                        <span
                                            class="mb-0 text-muted font-medium">{{ $message->sentTo->full_name }}</span>
                                    </td>
                                    <!-- Message -->
                                    <td>
                                        <a class="link modal-link" href="javascript: void(0)" data-title="Message"
                                            data-href="{{ route('messages.show', $message->id) }}">
                                            <span
                                                class="badge badge-pill text-white font-bold badge-info mr-2">Subject</span>
                                            <span class="text-dark"
                                                style="border-bottom: 2px dotted black">{{ $message->subject }}</span>
                                        </a>
                                    </td>
                                    <!-- Attachment -->
                                    <td><i class="fa fa-paperclip text-muted"></i></td>
                                    <!-- Time -->
                                    <td class="text-muted">{{ $message->created_at->format('d M, y') }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>

@endsection
@push('styles')
<style>
    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 0 solid transparent;
        border-radius: 0;
    }
    .mailbox-widget .custom-tab .nav-item .nav-link {
        border: 0;
        color: #fff;
        border-bottom: 3px solid transparent;
    }
    .mailbox-widget .custom-tab .nav-item .nav-link.active {
        background: 0 0;
        color: #fff;
        border-bottom: 3px solid #2cd07e;
    }
    .no-wrap td, .no-wrap th {
        white-space: nowrap;
    }
    .table td, .table th {
        padding: .9375rem .4rem;
        vertical-align: top;
        border-top: 1px solid rgba(120,130,140,.13);
    }
    .font-light {
        font-weight: 300;
    }
</style>
@endpush

@push('scripts')
    <script>
    $(function () {
        $('#datatable').DataTable({
            responsive: true,
            sDom: 'Rfrtlip',
            language: {
                searchPlaceholder: 'Search...',
                sSearch: '',
                lengthMenu: '_MENU_ items/page',
            },
            /*order: [
                [ 4, "desc" ]
            ],*/
            columnDefs: [{
                'targets': [6, 8],
                'orderable': false,
            }
            ],
            fixedColumns: true
        });
    });
    </script>
@endpush