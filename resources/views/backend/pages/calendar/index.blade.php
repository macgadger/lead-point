@extends('layouts.app')
@section('title','Calendar')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div id='calendar'></div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
    
    <script>
        document.addEventListener('DOMContentLoaded', function() {
        var calendarEl = document.getElementById('calendar');
        var calendar = new fullcalendar(calendarEl, {
          editable: true,
          droppable: true,
          selectable: true,
          initialView: 'dayGridMonth'
        });
        calendar.render();
      });
    </script>
@endpush