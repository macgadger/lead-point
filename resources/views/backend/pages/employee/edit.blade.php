@extends('layouts.app')
@section('title', 'Edit Employee')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">

            <div class="card-body">
                <x-validation />
                <!-- Card Body -->
                <form name="profileForm" method="post" action="{{ route('employee.update', $employee->id ) }}"
                    enctype="multipart/form-data" data-parsley-validate>
                    @csrf
                    @method('patch')
                    <div class="form-layout">
                        <div class="row mg-b-25">

                            <div class="col-lg-2 text-center">
                                @if($employee->avatar === 'default')
                                <img src="{{ Avatar::create($employee->full_name)->toBase64() }}" class="img-thumbnail rounded-circle avatar-xl" />
                                @else
                                    <img src="{{ $employee->asset_avatar }}" class="img-thumbnail rounded-circle avatar-xl" />
                                @endif
                            </div>

                            <div class="col-lg-10">
                                <div class="form-group">
                                    <label class="form-control-label">Update Profile Picture:</label>
                                    <input class="form-control" type="file" name="user_img" value="">
                                </div>
                            </div>

                            <div class="col-md-12">
                                <hr>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-control-label">Email: <span class="text-danger">*</span></label>
                                    <input class="form-control" type="email" name="email" value="{{ $employee->email }}"
                                        placeholder="Enter Email/Username" required>
                                </div>
                            </div>
                            <div class="col-lg-6">&nbsp;</div>


                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label class="form-control-label">Firstname: <span
                                            class="text-danger">*</span></label>
                                    <input class="form-control" type="text" name="first_name"
                                        style="text-transform:capitalize" value="{{ $employee->first_name }}"
                                        minlength="3" placeholder="Enter firstname" required>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label class="form-control-label">Lastname:</label>
                                    <input class="form-control" type="text" name="last_name"
                                        style="text-transform:capitalize" value="{{ $employee->last_name }}"
                                        placeholder="Enter lastname" required>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label class="form-control-label">Mobile Number: <span
                                            class="text-danger">*</span></label>
                                    <input class="form-control" type="phone" name="phone" value="{{ $employee->phone }}"
                                        minlength="10" maxlength="13" placeholder="Enter Mobile Number" required>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div id="slWrapperBirth" class="parsley-select">
                                    <div class="form-group">
                                        <label class="form-control-label">Date of Birth: <span
                                            class="text-danger">*</span></label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">
                                                    <i class="uil uil-schedule tx-16 lh-0 op-6"></i>
                                                </div>
                                            </div>
                                            <input type="text" data-parsley-class-handler="#slWrapperBirth"
                                                data-parsley-errors-container="#slErrorContainerBirth"
                                                class="form-control fc-dobpicker" value="{{ date('d-m-Y',strtotime($employee->dob)) }}" name="dob"
                                                placeholder="Date of Birth" autocomplete="off">
                                        </div><!-- wd-200 -->
                                        <div id="slErrorContainerBirth"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="form-control-label">Address:</label>
                                    <textarea class="form-control" name="address"
                                        placeholder="Enter Address">{{ $employee->address }}</textarea>
                                </div>
                            </div>

                            <x-locations :model="$employee" />

                        </div><!-- row -->

                        <div class="form-layout-footer">
                            <button type="submit" class="btn btn-primary bd-0">Update Profile</button>
                        </div><!-- form-layout-footer -->
                        <hr>
                    </div><!-- form-layout -->
                </form>

                <h2>Update Credentials</h2>
                <hr>
                <form name="updatePassword" method="POST"
                    action="{{ route('profile.employee.password',$employee->uid) }}" data-parsley-validate>
                    @csrf
                    <div class="form-layout">
                        <div class="row mg-b-25">

                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-control-label">Email/Username:</label>
                                    <input class="form-control" type="email" name="email"
                                        value="{{ $employee->user->email }}" readonly required>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                &nbsp;
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-control-label">Password:</label>
                                    <input class="form-control" type="password" name="password"
                                        placeholder="Enter New Password" required>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-control-label">Repeat Password:</label>
                                    <input class="form-control" type="password" name="password_confirmation"
                                        placeholder="Repeat Password" required>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div id="slWrapperStatus" class="parsley-select">
                                    <div class="form-group">
                                        <label class="form-control-label">Profile Status: <span
                                                class="tx-danger">*</span></label>
                                        <select class="form-control select2" name="status"
                                            data-parsley-class-handler="#slWrapperStatus"
                                            data-parsley-errors-container="#slErrorContainerStatus" required>
                                            <option value="1" @if($employee->user->status =='1') selected @endif>Active
                                            </option>
                                            <option value="0" @if($employee->user->status =='0') selected @endif>Suspend
                                            </option>
                                        </select>
                                        <div id="slErrorContainerStatus"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <hr>
                                <div class="form-group">
                                    <label class="form-control-label">Permissions: </label>
                                </div>
                                
                                @foreach ($permissions as $key => $permission)
                                    <div class="form-group">
                                        <div class="custom-control custom-switch custom-switch-lg mb-10" dir="ltr">
                                            <input type="checkbox" class="custom-control-input" name="permissions[]" value="{{ $permission->name }}"
                                                @if(in_array($permission->name,Arr::flatten($employee->user->getPermissionNames())))
                                                    checked
                                                @endif
                                                id="permissionSwitch{{ $key }}">
                                            <label class="custom-control-label" for="permissionSwitch{{ $key }}">{{ ucwords($permission->name) }}</label>
                                        </div>
                                    </div>
                                @endforeach
                        </div>
                        <div class="form-group">
                            <hr>
                            <button type="submit" class="btn btn-danger bd-0">Update Credentials</button>
                        </div><!-- form-layout-footer -->
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
    <script>
        $('.fc-dobpicker').datetimepicker({
            timepicker: false,
            format:'d-m-Y',
            formatDate:'Y-m-d',
            formatTime:'H:i',
            maxDate:'{{ date("Y/m/d") }}',
        });

        $('.select2').select2();
    </script>
@endpush