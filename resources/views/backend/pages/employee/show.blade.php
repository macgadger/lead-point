@extends('app.layout.module')
@section('title', 'View Employee')
@section('content')
<div class="row">
<div class="col-md-12">
    <div class="card">
        <div class="card-header bg-primary d-flex align-items-center justify-content-between pd-y-5 bd-b">
            <h6 class="mg-b-0 tx-20 tx-white"><a href="{{ route('employee.index') }}" style="color:white"><i class="fa fa-arrow-left"></i></a> Employee</h6>
            <div class="card-option tx-24">
                <a href="" class="tx-white-8 hover-white mg-l-10"><i class="icon ion-android-more-vertical lh-0"></i></a>
            </div>
        </div>
        <div class="card-body">
         
                <div class="table-wrapper">
                        <table id="datatable" class="table display responsive nowrap">
                          <tbody>
                            <tr>
                                <th class="wd-15p">Date Joined</th> <td>{{ date('d, M Y', strtotime($emp->joining_date)) }}</td>
                            </tr>
                            <tr>
                                <th class="wd-15p">Salary</th> <td> ₹{{ $emp->salary }}</td>
                            </tr>
                            <tr>
                                <th class="wd-15p">Username/Email</th> <td>{{ $emp->users->email }}</td>
                            </tr>
                            <tr>
                              <th class="wd-15p">Name</th>  <td>{{ ucwords($emp->full_name) }}</td>
                            </tr>
                            <tr>
                              <th class="wd-20p">Phone Number</th>  <td>{{ $emp->phone_number }}</td>
                            </tr>
                            <tr>
                              <th class="wd-20p">Address</th>  <td>{{ $emp->address }}</td>
                            </tr>
                            <tr>
                              <th class="wd-20p">City</th>  <td>{{ ucwords($emp->city) }}</td>
                            </tr>
                            <tr>
                                <th class="wd-20p">State</th>  <td>{{ ucwords($emp->state) }}</td>
                            </tr>
                            <tr>
                                <th class="wd-20p">Country</th>  <td>{{ ucwords($emp->country) }}</td>
                            </tr>
                            
                          </tbody>
                         
                        </table>
                </div>

        
        </div>
    </div>
</div>
</div>

              <div class="card-body">

                <table class="table user-view-table m-0">
                  <tbody>
                    <tr>
                      <td>Username:</td>
                      <td>nmaxwell</td>
                    </tr>
                    <tr>
                      <td>Name:</td>
                      <td>Nelle Maxwell</td>
                    </tr>
                    <tr>
                      <td>E-mail:</td>
                      <td>nmaxwell@mail.com</td>
                    </tr>
                    <tr>
                      <td>Company:</td>
                      <td>Company Ltd.</td>
                    </tr>
                  </tbody>
                </table>

                <h6 class="mt-4 mb-3">Social links</h6>

                <table class="table user-view-table m-0">
                  <tbody>
                    <tr>
                      <td>Twitter:</td>
                      <td><a href="javascript:void(0)">https://twitter.com/user</a></td>
                    </tr>
                    <tr>
                      <td>Facebook:</td>
                      <td><a href="javascript:void(0)">https://www.facebook.com/user</a></td>
                    </tr>
                    <tr>
                      <td>Instagram:</td>
                      <td><a href="javascript:void(0)">https://www.instagram.com/user</a></td>
                    </tr>
                  </tbody>
                </table>

                <h6 class="mt-4 mb-3">Personal info</h6>

                <table class="table user-view-table m-0">
                  <tbody>
                    <tr>
                      <td>Birthday:</td>
                      <td>May 3, 1995</td>
                    </tr>
                    <tr>
                      <td>Country:</td>
                      <td>Canada</td>
                    </tr>
                    <tr>
                      <td>Languages:</td>
                      <td>English</td>
                    </tr>
                  </tbody>
                </table>

                <h6 class="mt-4 mb-3">Contacts</h6>

                <table class="table user-view-table m-0">
                  <tbody>
                    <tr>
                      <td>Phone:</td>
                      <td>+0 (123) 456 7891</td>
                    </tr>
                  </tbody>
                </table>

                <h6 class="mt-4 mb-3">Interests</h6>

                <table class="table user-view-table m-0">
                  <tbody>
                    <tr>
                      <td>Favorite music:</td>
                      <td>
                        Rock,
                        Alternative,
                        Electro,
                        Drum &amp; Bass,
                        Dance
                      </td>
                    </tr>
                    <tr>
                      <td>Favorite movies:</td>
                      <td>
                        The Green Mile,
                        Pulp Fiction,
                        Back to the Future,
                        WALL·E,
                        Django Unchained,
                        The Truman Show,
                        Home Alone,
                        Seven Pounds
                      </td>
                    </tr>
                  </tbody>
                </table>

              </div>
@endsection


