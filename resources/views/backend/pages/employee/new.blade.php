@extends('layouts.app')
@section('title', 'New Employee')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">

            <div class="card-body">
                <x-validation />
                <!-- Card Body -->
                <form name="userForm" method="post" action="{{ route('employee.store') }}"
                    enctype="multipart/form-data" data-parsley-validate>
                    @csrf
                    <div class="form-layout">
                        <div class="row mg-b-25">

                            <div class="col-lg-2 text-center">
                                <img src="{{ asset('assets/avatars/default.jpg') }}"
                                    class="img-thumbnail rounded-circle avatar-xl" />
                            </div>

                            <div class="col-lg-10">
                                <div class="form-group">
                                    <label class="form-control-label">Profile Picture:</label>
                                    <input class="form-control" type="file" name="user_img"
                                        value="{{ old('user_img') }}">
                                </div>
                            </div>

                            <div class="col-md-12">
                                <hr>
                            </div>

                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label class="form-control-label">Firstname: <span
                                            class="text-danger">*</span></label>
                                    <input class="form-control" type="text" name="first_name"
                                        style="text-transform:capitalize" value="{{ old('first_name') }}" minlength="3"
                                        placeholder="Enter firstname" required>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label class="form-control-label">Lastname:</label>
                                    <input class="form-control" type="text" name="last_name"
                                        style="text-transform:capitalize" value="{{ old('last_name') }}"
                                        placeholder="Enter lastname" required>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label class="form-control-label">Mobile Number: <span
                                            class="text-danger">*</span></label>
                                    <input class="form-control" type="phone" name="phone" value="{{ old('phone') }}"
                                        minlength="10" maxlength="13" placeholder="Enter Mobile Number" required>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div id="slWrapperBirth" class="parsley-select">
                                    <div class="form-group">
                                        <label class="form-control-label">Date of Birth: <span
                                            class="text-danger">*</span></label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">
                                                    <i class="uil uil-schedule tx-16 lh-0 op-6"></i>
                                                </div>
                                            </div>
                                            <input type="text" data-parsley-class-handler="#slWrapperBirth"
                                                data-parsley-errors-container="#slErrorContainerBirth"
                                                class="form-control fc-dobpicker" value="{{ old('dob') }}" name="dob"
                                                placeholder="Date of Birth" autocomplete="off">
                                        </div><!-- wd-200 -->
                                        <div id="slErrorContainerBirth"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="form-control-label">Address:</label>
                                    <textarea class="form-control" name="address"
                                        placeholder="Enter Address">{{ old('address') }}</textarea>
                                </div>
                            </div>

                            <x-locations />


                        </div><!-- row -->

                        <h2>Login Information</h2>
                        <hr>
                        <div class="row mg-b-25">

                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-control-label">Email/Username: <span
                                            class="text-danger">*</span></label>
                                    <input class="form-control" type="email" name="email" value="{{ old('email') }}"
                                        placeholder="Enter Email/Username" required>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                &nbsp;
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label class="form-control-label">Password: <span class="text-danger">*</span></label>
                                    <input class="form-control password" type="password" name="password"  placeholder="Create Password" required>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label class="form-control-label">Repeat Password: <span
                                            class="text-danger">*</span></label>
                                    <input class="form-control password" type="password" name="password_confirmation"
                                        placeholder="Repeat Password" required>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                &nbsp;
                            </div>

                            <div class="col-lg-6">
                                <div id="slWrapperStatus" class="parsley-select">
                                    <div class="form-group">
                                        <label class="form-control-label">Set Profile Status: <span
                                                class="text-danger">*</span></label>
                                        <select class="form-control select2" name="status"
                                            data-parsley-class-handler="#slWrapperStatus"
                                            data-parsley-errors-container="#slErrorContainerStatus" required>
                                            <option value="">Select One...</option>
                                            <option value="1" @if(old('status')=='1' ) selected @endif>Active</option>
                                            <option value="0" @if(old('status')=='0' ) selected @endif>Suspend</option>
                                        </select>
                                        <div id="slErrorContainerStatus"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <hr>
                                <div class="form-group">
                                    <label class="form-control-label">Permissions: </label>
                                </div>
                                
                                @foreach ($permissions as $key => $permission)
                                    <div class="form-group">
                                        <div class="custom-control custom-switch custom-switch-lg mb-10" dir="ltr">
                                            <input type="checkbox" class="custom-control-input" name="permissions[]" 
                                                value="{{ $permission->name }}" id="permissionSwitch{{ $key }}">
                                            <label class="custom-control-label" for="permissionSwitch{{ $key }}">{{ ucwords($permission->name) }}</label>
                                        </div>
                                    </div>
                                @endforeach

                            </div>
                        </div>
                        <hr>
                        <div class="form-layout-footer">
                            <button id="submit" class="btn btn-primary bd-0">Create Employee</button>
                        </div><!-- form-layout-footer -->
                    </div><!-- form-layout -->
                </form>
                <!-- Card Body -->
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
    <script>
        $('.fc-dobpicker').datetimepicker({
            timepicker: false,
            format:'d-m-Y',
            formatDate:'Y-m-d',
            formatTime:'H:i',
            maxDate:'{{ date("Y/m/d") }}',
        });

        $('.select2').select2();
        $('.password').password();
    </script>
@endpush

