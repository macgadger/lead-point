@extends('layouts.app')
@section('title', 'Employees List')
@section('turbolink','no-cache')
@section('content')
<div class="row">
    <div class="col"></div>
    @if(Auth::user()->profile->plan->users_limit == $employees->count())
        <div class="col">
            <div class="alert alert-solid alert-danger" role="alert">
                <strong>Users Limit {{ $employees->count()-1 }} of {{ Auth::user()->profile->plan->users_limit }} Reached. Please upgrade your account.</strong>
            </div><!-- alert -->
        </div>
    @else
        <div class="col-md-2">
            <div class="alert alert-solid alert-success" role="alert">
                <strong>Your Plan Users Limit {{ $employees->count()-1 }} of {{ Auth::user()->profile->plan->users_limit }}</strong>
            </div><!-- alert -->
        </div>
        <div class="col-md-2">
            <a href="{{ route('employee.create') }}" class="btn btn-success btn-rounded waves-effect waves-light mb-3 float-right">
                <i class="uil uil-plus-circle me-1"></i> Add New Employee
            </a>
        </div>
    @endif
</div>

<div class="row">
    <div class="col-12">
        <div class="table-wrapper">
            <table id="datatable"
                class="table table-centered table-hover datatable dt-responsive nowrap table-card-list"
                style="border-collapse: collapse; border-spacing: 0 12px; width: 100%;">
                <thead>
                    <tr class="bg-transparent">
                        <th class="wd-15p">Name</th>
                        <th class="wd-10p">Phone</th>
                        <th class="wd-10p">Email</th>
                        <th class="wd-15p">Created On</th>
                        <th class="wd-10p">Status</th>
                        <th class="wd-20p">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @if(isset($employees) && $employees->isNotEmpty())
                    @foreach($employees as $key => $employee)
                        @if($employee->user->hasRole('agent'))
                        <tr>
                            <td>
                                {{-- <img src="{{ $employee->asset_avatar }}" class="avatar-sm align-self-center
                                rounded-circle" /> --}}
                                <div class="d-flex align-items-center">
                                    <div>
                                        @if($employee->avatar === 'default')
                                            <img src="{{ Avatar::create($employee->full_name)->toBase64() }}" class="img-thumbnail avatar-sm align-self-center rounded-circle mr-1" />
                                        @else
                                            <img src="{{ $employee->asset_avatar }}" class="img-thumbnail avatar-sm align-self-center rounded-circle mr-1" />
                                        @endif
                                    </div>
                                    <div class="">
                                        <p class="font-weight-bold mb-0">{{ $employee->full_name }}</p>
                                        <p class="text-muted mb-0">Agent</p>
                                    </div>
                                </div>
                            </td>
                            <td>{{ $employee->phone }}</td>
                            <td style="text-transform: lowercase !important">{{ $employee->email }}</td>
                            <td>
                                <a href="#">
                                    {{ \Carbon\Carbon::parse($employee->created_at)->diffForHumans() }}
                                    on
                                    {{ date('h:i A', strtotime($employee->created_at))}}
                                </a>
                            </td>
                            <td>
                                @if($employee->user->status == 1)
                                    <span class="btn btn-success btn-sm">Active</span>
                                @else
                                    <span class="btn btn-danger btn-sm">Suspended</span>
                                @endif
                            </td>
                            <td>
                                <a href="{{ route('employee.edit', $employee->id ) }}" class="btn text-primary"
                                    data-toggle="tooltip" data-placement="bottom" title="Edit Details"><i
                                        class="uil uil-edit-alt"></i>
                                </a>
                                @role('admin')
                                <a href="#" class="btn text-danger" data-toggle="tooltip" data-placement="bottom"
                                    title="Delete User"><i class="uil uil-trash-alt"></i></a>
                                @endrole
                            </td>
                        @endif
                        @endforeach
                        @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function () {
            $('#datatable').DataTable({
                retrieve: true,
                'columnDefs': [ {
                    'targets'  : [1,2,3,5], /* column index */
                    'orderable': false, /* true or false */
                }],
            });
        });
    </script>
@endpush
