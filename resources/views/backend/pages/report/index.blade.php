@extends('layouts.app')
@section('title', 'Reporting')
@section('turbolink','no-cache')
@section('content')
<div class="row">
    <div class="col-12">

    <div class="card">
        <div class="card-body">
            <form name="reportsForm" id="reportsForm" method="GET" action="{{ route('reports.generate') }}">
                @csrf
                <div class="form-layout">
                    <div class="row mg-b-25">

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="form-control-label">Generate Report of:<span class="text-danger">*</span></label>
                                    <select class="form-control select2" name="type" required>
                                        <option value=""> ----Select Type---- </option>
                                        <option value="1" @if(\Request::get('type') == 1) selected @endif>Active Lead(s)</option>
                                        <option value="pending" @if(\Request::get('type') == 'pending') selected @endif>Pending Lead(s)</option>
                                        <option value="2" @if(\Request::get('type') == 2) selected @endif>Converted(s)</option>
                                        <option value="3" @if(\Request::get('type') == 3) selected @endif>Delay Lead(s)</option>
                                        <option value="4" @if(\Request::get('type') == 4) selected @endif>Dead Lead(s)</option>
                                        <option value="4" @if(\Request::get('type') == 'visit') selected @endif>Walkin/Visit</option>
                                    </select>
                            </div><!-- col-4 -->
                        </div>

                            <div class="col-md-4">
                                <div id="slWrapperRange" class="parsley-select">
                                    <div class="form-group">
                                        <label class="form-control-label">Date Range:</label>
                                        <div>
                                            <div class="input-daterange input-group" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true">
                                                <div class="input-group-prepend">
                                                <div class="input-group-text">
                                                    <i class="uil uil-schedule tx-16 lh-0 op-6"></i>
                                                </div>
                                            </div>
                                                <input type="text" class="form-control fc-datepicker" name="range_from" autocomplete="off" value="{{ \Request::get('range_from') }}" required/>
                                                <input type="text" class="form-control fc-datepicker" name="range_to" autocomplete="off"   value="{{ \Request::get('range_to') }}"/>
                                            </div>
                                        </div>
                                        <div id="slErrorContainerRange"></div>
                                    </div>
                                </div>
                            </div>
                            @if(Auth::user()->hasRole('admin'))
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="form-control-label">By Agent</label>
                                        <select class="form-control select2" name="agents">
                                            <option value="all" @if(\Request::get('agents') == 'all') selected @endif> --All-- </option>
                                            @foreach($agents as $agent)
                                                @if($agent->user->hasRole('agent'))
                                                    <option value="{{ $agent->id }}"  @if(\Request::get('agents') == $agent->id) selected @endif>{{ $agent->full_name }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            @endif

                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="control-label">&nbsp;</label>
                                    <button type="submit" class="btn btn-block btn-danger"><i class="uil uil-search"></i> Generate Report</button>
                                </div>
                            </div>

                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="col-12">
    @isset($reports)
    <hr>
        <table id="datatable" class="table table-centered table-hover datatable dt-responsive nowrap table-card-list" style="border-collapse: collapse; border-spacing: 0 12px; width: 100%;">
            <thead>
                <tr class="bg-transparent">
                    <th class="wd-5p">#</th>
                    <th class="wd-10p">Stage</th>
                    <th class="wd-15p">Name</th>
                    <th class="wd-20p">Inquiry for</th>
                    <th class="wd-10p">Next Followup Date</th>
                    <th class="wd-10p">Last Followup</th>
                    <th class="wd-10p">Agent</th>
                    <th class="wd-10p">Opportunity Cost</th>
                    <th class="wd-20p">Actions</th>
                </tr>
            </thead>
            <tbody>
                    @foreach($reports as $key => $report)
                    <tr>
                        <td>{{ $key+1 }}</td>
                        <td>
                            <button
                                @if($report->status == 'Follow Up')
                                    class="btn btn-warning modal-link rounded-10 btn-sm"
                                @elseif($report->status == 'Dead')
                                    class="btn btn-danger modal-link rounded-10  btn-sm"
                                @else
                                    class="btn btn-success modal-link rounded-10 btn-sm"
                                @endif
                                    data-title="Update report Status" data-href="{{ route('leads.status', $report->id) }}" title="Update report Status">
                                    {{ $report->status }}
                            </button>
                        </td>
                        <td>{{ $report->profile->full_name }}</td>
                        <td>{{ $report->inquiry_for }}</td>
                        <td>
                            <a href="#">
                            {{ date('M d, Y h:i A', strtotime($report->next_followup)) }}
                            </a>
                        </td>
                        <td>
                            {{ \Carbon\Carbon::parse($report->last_followup)->diffForHumans() }}
                            on
                            {{ date('M d, Y', strtotime($report->last_followup))}}
                        </td>
                        <td>
                            <a href="#" title="{{ ucwords($report->agent->full_name) }}" data-toggle="tooltip" data-placement="bottom">
                            @if($report->agent->avatar === 'default')
                                <img src="{{ Avatar::create($report->agent->full_name)->toBase64() }}" class="img-thumbnail avatar-sm align-self-center rounded-circle mr-1" />
                            @else
                                <img src="{{ $report->agent->asset_avatar }}" class="img-thumbnail avatar-sm align-self-center rounded-circle mr-1" />
                            @endif
                            </a>
                        </td>
                        <td>Rs {{ $report->price_quoted }}.00</td>
                        <td>
                            <div class="btn-group" role="group" aria-label="Controls">
                               
                                <button class="btn btn-warning btn-sm modal-link" data-toggle="tooltip" data-placement="bottom"
                                    data-title="Activity Timeline" data-href="{{ route('leads.timeline', $report->id) }}"
                                    title="Activity Timeline"><i class="uil uil-history"></i>
                                </button>
                               
                                <button class="btn btn-primary btn-sm modal-link" data-toggle="tooltip" data-placement="bottom"
                                    data-title="report Details" data-href="{{ route('leads.show', $report->id) }}"
                                    title="Lead Details"><i class="uil uil-info-circle"></i>
                                </button>

                            </div>
                        </td>
                        </tr>
                    @endforeach
            </tbody>
        </table>
    @endisset
</div>
</div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function () {
            $('#datatable').DataTable({
                'columnDefs': [ {
                    'targets'  : [0,1,2,6,8],
                    'orderable': false,
                }],
            });
            $('.select2').select2();

            $('.fc-datepicker').datetimepicker({
                timepicker: false,
                format:'d-m-Y',
                formatDate:'Y-m-d',
                inline: false,
            });
        });
    </script>
@endpush