@extends('layout.app')
@section('title', 'Leads')
@section('content')
<div class="card-body">

        <div class="table-wrapper" style="margin-top: 20px">
        <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
            <thead>
                <tr>
                    <th class="wd-5p">#</th>
                    <th class="wd-10p">Stage</th>
                    <th class="wd-15p">Name</th>
                    <th class="wd-20p">Inquiry for</th>
                    <th class="wd-10p">Next Followup Date</th>
                    <th class="wd-10p">Status</th>
                    <th class="wd-10p">Agent</th>
                    <th class="wd-10p">Opportunity Cost</th>
                    <th class="wd-20p">Actions</th>
                </tr>
            </thead>
            <tbody>
                @if($leads->isNotEmpty())
                @foreach($leads as $key => $lead)
                <tr>
                    <td>{{ $key+1 }}</td>
                    <td>
                        <button
                            @if($lead->status == 'Follow Up')
                                class="btn btn-warning modal-link rounded-10 btn-sm"
                            @elseif($lead->status == 'Dead')
                                class="btn btn-danger modal-link rounded-10  btn-sm"
                            @else
                                class="btn btn-success modal-link rounded-10 btn-sm"
                            @endif
                                data-title="Update Lead Status" data-href="{{ route('leads.status', $lead->id) }}" title="Update Lead Status">
                                {{ $lead->status }}
                        </button>
                    </td>
                    <td>{{ $lead->profile->full_name }}</td>
                    <td>{{ $lead->inquiry_for }}</td>
                    <td>
                        <a href="#">
                        {{ date('M d, Y h:i A', strtotime($lead->next_followup)) }}
                        </a>
                    </td>
                    <td>
                        @if($lead->status == 'Converted')
                            <button class="btn btn-success rounded-10 btn-sm"><i class="uil uil-check-circle"></i></button>
                        @else
                            @if($lead->next_followup < \Carbon\Carbon::today())
                                <button class="btn btn-danger rounded-10  btn-sm">Pending</button>
                            @elseif(date('Y-m-d',strtotime($lead->next_followup)) == \Carbon\Carbon::today()->format('Y-m-d'))
                                <button class="btn btn-success rounded-10 btn-sm">Today</button>
                            @elseif(date('Y-m-d',strtotime($lead->next_followup)) > \Carbon\Carbon::today()->format('Y-m-d'))
                                <button class="btn btn-success rounded-10 btn-sm">Upcoming</button>
                            @else
                                 <button class="btn btn-danger rounded-10 btn-sm">{{ $lead->status }}</button>
                            @endif
                        @endif
                    </td>
                    <td>
                        <div class="avatar-group avatar-group-sm avatar-group-overlapped mr-40 mt-25">
                                <a href="#" title="{{ ucwords($lead->agent->full_name) }}" data-toggle="tooltip" data-placement="bottom">
                                    <img src="{{ $lead->agent->asset_avatar }}" alt="user" class="img-thumbnail align-self-start rounded-circle avatar-sm">
                                </a>
                        </div>
                    </td>
                    <td>Rs {{ $lead->price_quoted }}.00</td>
                    <td>
                        <div class="btn-group" role="group" aria-label="Controls">
                            <button class="btn btn-warning btn-sm modal-link" data-toggle="tooltip" data-placement="bottom"
                                data-title="Lead Activity Timeline" data-href="{{ route('leads.timeline', $lead->id) }}"
                                title="Lead Activity Timeline"><i class="uil uil-history"></i>
                            </button>
                            <button class="btn btn-primary btn-sm modal-link" data-toggle="tooltip" data-placement="bottom"
                                data-title="Lead Details" data-href="{{ route('leads.show', $lead->id) }}"
                                title="Lead Details"><i class="uil uil-info-circle"></i>
                            </button>
                        </div>
                    </td>
                    </tr>
                    @endforeach
                    @endif
            </tbody>
        </table>
    </div>
</div>
@endsection
@push('styles')
    <link href="{{ asset('assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/libs/avatars/avatars.min.css') }}" rel="stylesheet">
@endpush

@push('scripts')
    <script src="{{ asset('assets/libs/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>

    <script src="{{ asset('assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') }}"></script>
    <script>
    $(function () {
        $('#datatable').DataTable({
            responsive: true,
            sDom: 'Rfrtlip',
            language: {
                searchPlaceholder: 'Search...',
                sSearch: '',
                lengthMenu: '_MENU_ items/page',
            },
            /*order: [
                [ 4, "desc" ]
            ],*/
            columnDefs: [{
                'targets': [6, 8],
                'orderable': false,
            }
            ],
            fixedColumns: true
        });
    });
    </script>
@endpush