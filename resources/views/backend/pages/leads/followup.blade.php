<ul class="nav nav-tabs nav-tabs-custom nav-justified" id="myTab" role="tablist">
    <li class="nav-item" role="presentation">
        <a class="nav-link tx-bold active" id="details-tab" data-toggle="tab" href="#details" role="tab"
            aria-controls="details" aria-selected="true">Lead Details</a>
    </li>
    <li class="nav-item" role="presentation">
        <a class="nav-link tx-bold" id="timeline-tab" data-toggle="tab" href="#timeline" role="tab"
            aria-controls="timeline" aria-selected="false">Activity Timeline</a>
    </li>
    <li class="nav-item" role="presentation">
        <a class="nav-link tx-bold" id="followup-tab" data-toggle="tab" href="#followup" role="tab"
            aria-controls="followup" aria-selected="false">Add Followup</a>
    </li>
</ul>

<div class="tab-content" id="myTabContent">

    <div class="tab-pane fade show active" id="details" role="tabpanel" aria-labelledby="details-tab">
        <div class="card p-3">

                        <div class="table-responsive">
                            <div>
                                <p class="mb-1">Name :</p>
                                <h5 class="font-size-16">{{ ucfirst($lead->profile->name) }}</h5>
                            </div>
                            <div class="mt-2">
                                <p class="mb-1">Phone Number :</p>
                                <h5 class="font-size-16">{{ $lead->profile->phone }}</h5>
                            </div>
                            @if(!empty($lead->email))
                            <div class="mt-2">
                                <p class="mb-1">Email :</p>
                                <h5 class="font-size-16">{{ $lead->profile->email }}</h5>
                            </div>
                            @endif
                            <div class="mt-2">
                                <p class="mb-1">Inquiry for and Duration :</p>
                                <h5 class="font-size-16">{{ ucfirst($lead->inquiry_for) }} {{ $lead->duration }}</h5>
                            </div>
                            <div class="mt-2">
                                <p class="mb-1">Price Quoted :</p>
                                <h5 class="font-size-16">Rs. {{ $lead->price_quoted }}.00</h5>
                            </div>

                            <div class="mt-2">
                                <p class="mb-1">Remarks/Notes :</p>
                                <h5 class="font-size-16">{{ $lead->notes }}</h5>
                            </div>

                        </div>


        </div>
    </div>

    <div class="tab-pane fade" id="timeline" role="tabpanel" aria-labelledby="timeline-tab" style="max-height: 400px; overflow-y: scroll;">
        <div class="card">
            <div class="p-3">
            @if($lead->timeline->isNotEmpty())
                <ul class="verti-timeline list-unstyled">
                    @foreach($lead->timeline as $line)
                        <li class="event-list">
                            <div class="event-date text-primary">{{ $line->created_at->format('M d, y') }}</div>
                            <h5>{{ strtoupper($line->status) }}</h5>
                            <p class="text-muted">
                                Lead followed up by <span class="txt-bold">{{ ucfirst($line->followup_type) }}</span><br>
                                <span class="text-primary">Remarks: {{ ucfirst($line->notes) }}</span>
                            </p>
                            @if($line->status === 'Converted')
                                <p class="timeline-text">Closing Price: Rs {{ $lead->price_quoted }}.00</p>
                            @endif
                        </li>
                    @endforeach
                    <li class="event-list">
                        <div class="event-date text-primary">{{ $lead->created_at->format('M d, y') }}</div>
                        <h5>{{ strtoupper($lead->status) }}</h5>
                        <p class="text-muted">
                            Lead Created at {{ date('M d, Y', strtotime($lead->created_at)) }}<br>
                            <span class="text-primary">By: {{ $lead->createdBy->full_name }}</span>
                        </p>
                    </li>
                </ul>
                @else
                    <h3 class="tx-center">No Activity Timeline Present</h3>
                @endif
            </div>
        </div>
    </div>

    <div class="tab-pane fade" id="followup" role="tabpanel" aria-labelledby="followup-tab">
        <div class="card">
            <form name="followupForm" method="post" action="{{ route('leads.followup', $lead->id) }}"
                data-parsley-validate>
                @csrf
                <div class="form-layout p-3">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="form-control-label">Followup Tools:</label>
                                <div class="btn-group mg-l-10" role="group" aria-label="Basic example">
                                    <a href="http://api.whatsapp.com/send?phone=91{{ ltrim($lead->profile->phone, '0') }}" target="_blank" class="btn btn-success btn-sm"><i class="uil uil-whatsapp"></i></a>
                                    <a href="tel:+91{{ ltrim($lead->profile->phone, '0') }}" target="_blank" class="btn btn-warning btn-sm"><i class="uil uil-phone"></i></a>
                                    <a href="mailto:{{ $lead->profile->email }}" target="_blank" class="btn btn-info btn-sm"><i class="uil uil-envelope"></i></a>
                                </div>
                                <small class="form-text text-danger">
                                    Some Tools will only work on Phone
                                </small>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="form-control-label">Followup Date: <span
                                        class="tx-danger">*</span></label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <i class="uil uil-schedule tx-16 lh-0 op-6"></i>
                                        </div>
                                    </div>
                                    <input type="text" class="form-control fc-datepicker" name="next_followup"
                                        placeholder="Next Followup Date" autocomplete="off" required>
                                </div><!-- wd-200 -->
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="form-control-label">Followup Type: <span
                                        class="tx-danger">*</span></label>
                                <select class="form-control" name="followup_type" required>
                                    <option value="walkin">Walkin\Visited</option>
                                    <option value="whatsapp">Whatsapp</option>
                                    <option value="call">Call</option>
                                    <option value="email">Email</option>
                                    <option value="sms">SMS</option>
                                    <option value="meeting">Meeting</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="form-control-label">Notes/Remarks:</label>
                                <textarea class="form-control" name="notes" rows="5"
                                    placeholder="Remarks/Notes"></textarea>
                            </div>
                        </div>

                        <div class="form-layout-footer mg-l-15">
                            <button class="btn btn-primary bd-0">Add Update</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div><!-- form-layout-footer -->

                    </div>
                </div>
                <form>
        </div>
    </div>
    
</div>

<script>
    $(function(){
        // Datepicker
        $('.fc-datepicker').datetimepicker({
            timepicker: true,
            format:'d-m-Y H:i',
            formatDate:'Y-m-d',
            formatTime:'H:i',
            minDate: 0, // yesterday is minimum date
        });
    });
</script>