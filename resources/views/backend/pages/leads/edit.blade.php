@extends('layouts.app')
@section('title', 'Edit Lead Details')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <button onclick="window.history.go(-1); return false;"  class="btn btn-sm btn-primary"><< Go Back</button> <hr>
                        <x-validation />
                        <!-- Card Body -->
                        <form name="leadForm" method="post" action="{{ route('leads.update', $lead->id ) }}"
                            data-parsley-validate>
                            @csrf
                            @method('PATCH')
                            <div class="form-layout">
                                <div class="row mg-b-25">

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="form-control-label">Full Name: <span
                                                    class="text-danger">*</span></label>
                                            <input class="form-control" type="text" name="name"
                                                style="text-transform:capitalize" value="{{ $lead->profile->name }}"
                                                minlength="3" placeholder="Enter Full name"
                                                @if(!Auth::user()->hasRole('admin')) readonly disable @endif
                                            required>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="form-control-label">Mobile Number: <span
                                                    class="text-danger">*</span></label>
                                            <input class="form-control" type="phone" name="phone"
                                                value="{{ $lead->profile->phone }}" minlength="10" maxlength="13"
                                                placeholder="Enter Mobile Number" @if(!Auth::user()->hasRole('admin'))
                                            readonly disable @endif
                                            required>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="form-control-label">Alt Phone Number:</label>
                                            <input class="form-control" type="tel" name="alt_phone"
                                                value="{{ $lead->profile->alt_number }}" minlength="10" maxlength="13"
                                                placeholder="Enter Alternate Phone Number">
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div id="slWrapperBirth" class="parsley-select">
                                            <div class="form-group">
                                                <label class="form-control-label">Date of Birth: <span
                                                        class="text-danger">*</span></label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <div class="input-group-text">
                                                            <i class="uil uil-schedule tx-16 lh-0 op-6"></i>
                                                        </div>
                                                    </div>
                                                    <input type="text" data-parsley-class-handler="#slWrapperBirth"
                                                        data-parsley-errors-container="#slErrorContainerBirth"
                                                        class="form-control fc-dobpicker"
                                                        value="{{ date('d-m-Y',strtotime($lead->profile->dob)) }}" name="dob"
                                                        placeholder="Date of Birth" autocomplete="off" required>
                                                </div><!-- wd-200 -->
                                                <div id="slErrorContainerBirth"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="form-control-label">Email:</label>
                                            <input class="form-control" type="email" name="email"
                                                value="{{ $lead->profile->email }}" placeholder="Enter Email Address">
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="form-control-label">Address: <span
                                                    class="text-danger">*</span></label>
                                            <textarea class="form-control" name="address" rows="2"
                                                placeholder="Enter Address"
                                                required>{{ $lead->profile->address }}</textarea>
                                        </div>
                                    </div>

                                    <x-locations :model="$lead->profile" />

                                    <div class="col-md-12">
                                        <hr>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="form-control-label">Inquiry For: <span
                                                    class="text-danger">*</span></label>
                                            <input class="form-control" type="text" name="inquiry_for"
                                                style="text-transform:capitalize" placeholder="Enter Inquiry For"
                                                value="{{ $lead->inquiry_for }}" required>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="form-control-label">Duration:</label>
                                            <input class="form-control" type="text" name="duration"
                                                style="text-transform:capitalize" placeholder="Enter Duration"
                                                value="{{ $lead->duration }}">
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="form-control-label">Price Quoted: <span
                                                    class="text-danger">*</span></label>
                                            <input class="form-control" type="text" name="price_quoted"
                                                style="text-transform:capitalize" placeholder="Enter Price Quoted"
                                                value="{{ $lead->price_quoted }}" @if(!Auth::user()->hasRole('admin'))
                                            readonly disable @endif
                                            required>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div id="slWrapperFType" class="parsley-select">
                                            <div class="form-group">
                                                <label class="form-control-label">Followup Type:<span
                                                        class="text-danger">*</span></label>
                                                <select class="form-control select2" name="followup_type"
                                                    data-parsley-class-handler="#slWrapperFType"
                                                    data-parsley-errors-container="#slErrorContainerFType" required>
                                                    <option value="walkin" @if($lead->followup_type == 'walkin')
                                                        selected @endif>Walkin\Visited</option>
                                                    <option value="call" @if($lead->followup_type == 'call') selected
                                                        @endif>Call</option>
                                                    <option value="email" @if($lead->followup_type == 'email') selected
                                                        @endif>Email</option>
                                                    <option value="meeting" @if($lead->followup_type == 'meeting')
                                                        selected @endif>Meeting</option>
                                                    <option value="sms" @if($lead->followup_type == 'sms') selected
                                                        @endif>SMS</option>
                                                </select>
                                                <div id="slErrorContainerFType"></div>
                                            </div>
                                        </div><!-- col-4 -->
                                    </div>

                                    <div class="col-md-4">
                                        <div id="slWrapperDob" class="parsley-select">
                                            <div class="form-group">
                                                <label class="form-control-label">Followup Date:<span
                                                        class="text-danger">*</span></label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <div class="input-group-text">
                                                            <i class="uil uil-schedule tx-16 lh-0 op-6"></i>
                                                        </div>
                                                    </div>
                                                    <input type="text" data-parsley-class-handler="#slWrapperDob"
                                                        data-parsley-errors-container="#slErrorContainerDob"
                                                        class="form-control fc-datepicker"
                                                        value="{{ date('d-m-Y h:i',strtotime($lead->next_followup)) }}"
                                                        name="next_followup" placeholder="Next Followup Date"
                                                        autocomplete="off" required>
                                                </div><!-- wd-200 -->
                                                <div id="slErrorContainerDob"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">

                                        <div id="slWrapperStatus" class="parsley-select">
                                            <div class="form-group">
                                                <label class="form-control-label">Followup Status:<span
                                                        class="text-danger">*</span></label>
                                                <select class="form-control select2" name="status"
                                                    data-parsley-class-handler="#slWrapperStatus"
                                                    data-parsley-errors-container="#slErrorContainerStatus" required>
                                                    <option value="1" @if($lead->status == 'Follow Up') selected
                                                        @endif>Follow Up</option>
                                                    <option value="2" @if($lead->status == 'Converted') selected
                                                        @endif>Converted</option>
                                                    <option value="3" @if($lead->status == 'Delay') selected
                                                        @endif>Delay</option>
                                                    <option value="4" @if($lead->status == 'Dead') selected @endif>Dead
                                                    </option>
                                                </select>
                                                <div id="slErrorContainerStatus"></div>
                                            </div>
                                        </div><!-- col-4 -->
                                    </div>

                                    <div class="col-md-4">
                                        <div id="slWrapperSource" class="parsley-select">
                                            <div class="form-group">
                                                <label class="form-control-label">Source:<span
                                                        class="text-danger">*</span></label>
                                                <select class="form-control select2" name="source"
                                                    data-parsley-class-handler="#slWrapperSource"
                                                    data-parsley-errors-container="#slErrorContainerSource" required>
                                                    @foreach($sources as $source)
                                                    <option value="{{ $source->id }}" @if($lead->source == $source->id)
                                                        selected @endif>{{ ucwords($source->name) }}</option>
                                                    @endforeach
                                                </select>
                                                <div id="slErrorContainerSource"></div>
                                            </div>
                                        </div><!-- col-4 -->
                                    </div>

                                    @can('assign leads')
                                    <div class="col-md-4">
                                        <div id="slWrapperAgent" class="parsley-select">
                                            <div class="form-group">
                                                <label class="form-control-label">Assign to Agent:<span
                                                        class="text-danger">*</span></label>
                                                <select class="form-control select2" name="agent_id"
                                                    data-parsley-class-handler="#slWrapperAgent"
                                                    data-parsley-errors-container="#slErrorContainerAgent" required>
                                                    @role('agent')
                                                        <option value="{{ Auth::user()->profile->id }}">{{ Auth::user()->profile->full_name }} (You)</option>
                                                    @endrole
                                                    @foreach($agents as $agent)
                                                    @if($agent->user->hasRole('agent'))
                                                        <option value="{{ $agent->id }}" @if($agent->id == $lead->agent_id) selected @endif>{{ $agent->full_name }}</option>
                                                    @endif
                                                    @endforeach
                                                </select>
                                                <div id="slErrorContainerAgent"></div>
                                            </div>
                                        </div><!-- col-4 -->
                                    </div>
                                    @else
                                        <input type="hidden" name="agent_id" value="{{ $lead->agent_id }}" readonly />
                                    @endcan

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="form-control-label">Tags:</label>
                                            <select class="form-control tags" name="tags[]" multiple="multiple">
                                                @foreach(explode(',',$lead->tags) as $key => $tag)
                                                <option value="{{ $tag }}" selected>{{ $tag }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-control-label">Lead Priority: </label>
                                            <div class="form-check form-check-inline mt-4 ">
                                                <input type="radio" id="customRadio1" name="priority"
                                                    class="form-check-input" value="1" @if($lead->priority == 1) checked
                                                @endif>
                                                <label class="form-check-label" for="customRadio1">Hot</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input type="radio" id="customRadio2" name="priority"
                                                    class="form-check-input" value="2" @if($lead->priority == 2) checked
                                                @endif>
                                                <label class="form-check-label" for="customRadio2">Warm</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input type="radio" id="customRadio3" name="priority"
                                                    class="form-check-input" value="3" @if($lead->priority == 3) checked
                                                @endif>
                                                <label class="form-check-label" for="customRadio3">Cold</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="form-control-label">Comment/Remarks:<span
                                                    class="text-danger">*</span></label>
                                            <textarea class="form-control" name="notes" rows="5"
                                                placeholder="Remarks/Notes" required>{{ $lead->notes }}</textarea>
                                        </div>
                                    </div>

                                </div><!-- row -->
                                <hr>
                                <div class="form-layout-footer">
                                    <button class="btn btn-primary bd-0">Update Lead</button>
                                </div><!-- form-layout-footer -->
                            </div><!-- form-layout -->
                        </form>
                        <!-- Card Body -->
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script>
        $(function(){
            // Datepicker
            $('.fc-datepicker').datetimepicker({
                timepicker: true,
                format:'d-m-Y H:i',
                formatDate:'Y-m-d',
                formatTime:'H:i',
                minDate: 0, // yesterday is minimum date
            });

            $('.fc-dobpicker').datetimepicker({
                timepicker: false,
                format:'d-m-Y',
                formatDate:'Y-m-d',
                formatTime:'H:i',
                maxDate:'{{ date("Y/m/d") }}',
            });

        });

        $('.select2').select2();
        $('.tags').select2({
            tags: true,
            tokenSeparators: [',',' '],
            placeholder: "Lead related tags",
        });
    </script>
@endpush