@extends('layouts.app')
@section('title', 'New Lead')
@section('turbolink','no-cache')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
        <div class="card-body">
            <button onclick="window.history.go(-1); return false;"  class="btn btn-sm btn-primary"><< Go Back</button> <hr>
            <x-validation />
            <!-- Card Body -->
            <form name="leadForm" method="post" action="{{ route('leads.store') }}" data-parsley-validate>
                @csrf
                <div class="form-layout">
                    <div class="row mg-b-25">

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="form-control-label">Full Name: <span
                                        class="text-danger">*</span></label>
                                <input class="form-control" type="text" name="name"
                                    style="text-transform:capitalize"
                                    value="{{ isset($lead) ? $lead->name : old('name') }}"
                                    minlength="3"
                                    placeholder="Enter Full name" required>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="form-control-label">Mobile Number: <span
                                        class="text-danger">*</span></label>
                                <input class="form-control" type="phone" name="phone" 
                                    value="{{ isset($lead) ? $lead->phone : old('phone') }}"
                                    minlength="10" maxlength="13" placeholder="Enter Mobile Number" required>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="form-control-label">Alt Phone Number:</label>
                                <input class="form-control" type="tel" name="alt_phone"
                                    value="{{ old('alt_phone') }}" minlength="10" maxlength="13"
                                    placeholder="Enter Alternate Phone Number">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div id="slWrapperBirth" class="parsley-select">
                                <div class="form-group">
                                    <label class="form-control-label">Date of Birth:</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                                <i class="uil uil-schedule tx-16 lh-0 op-6"></i>
                                            </div>
                                        </div>
                                        <input type="text" data-parsley-class-handler="#slWrapperBirth"
                                            data-parsley-errors-container="#slErrorContainerBirth"
                                            class="form-control fc-dobpicker" value="{{ old('dob') }}" name="dob"
                                            placeholder="Date of Birth" autocomplete="off">
                                    </div><!-- wd-200 -->
                                    <div id="slErrorContainerBirth"></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="form-control-label">Email:</label>
                                <input class="form-control" type="email" name="email" 
                                    value="{{ isset($lead) ? $lead->email : old('email') }}"  
                                    placeholder="Enter Email Address">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="form-control-label">Address: <span class="text-danger">*</span></label>
                                <textarea class="form-control" name="address" rows="2" placeholder="Enter Address"
                                    required>{{ old('address') }}</textarea>
                            </div>
                        </div>

                        <x-locations />
                        
                        <div class="col-md-12">
                            <hr>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="form-control-label">Inquiry For: <span
                                        class="text-danger">*</span></label>
                                <input class="form-control" type="text" name="inquiry_for"
                                    style="text-transform:capitalize" placeholder="Enter Inquiry For" 
                                    value="{{ isset($lead) ? $lead->inquiry_for : old('inquiry_for') }}" required>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="form-control-label">Duration:</label>
                                <input class="form-control" type="text" name="duration"
                                    style="text-transform:capitalize" placeholder="Enter Duration" value="{{ old('duration') }}">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="form-control-label">Price Quoted: <span
                                        class="text-danger">*</span></label>
                                <input class="form-control" type="text" name="price_quoted"
                                    style="text-transform:capitalize" placeholder="Enter Price Quoted"
                                    value="{{ isset($lead) ? $lead->price_quoted : old('price_quoted') }}" required>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div id="slWrapperFType" class="parsley-select">
                                <div class="form-group">
                                    <label class="form-control-label">Followup Type:<span
                                            class="text-danger">*</span></label>
                                    <select class="form-control select2" name="followup_type"
                                        data-parsley-class-handler="#slWrapperFType"
                                        data-parsley-errors-container="#slErrorContainerFType" required>
                                        <option value=""> ----Select Type---- </option>
                                        <option value="walkin">Walkin\Visited</option>
                                        <option value="call">Call</option>
                                        <option value="email">Email</option>
                                        <option value="meeting">Meeting</option>
                                        <option value="sms">SMS</option>
                                    </select>
                                    <div id="slErrorContainerFType"></div>
                                </div>
                            </div><!-- col-4 -->
                        </div>

                        <div class="col-md-4">
                            <div id="slWrapperDob" class="parsley-select">
                                <div class="form-group">
                                    <label class="form-control-label">Followup Date:<span
                                            class="text-danger">*</span></label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                                <i class="uil uil-schedule tx-16 lh-0 op-6"></i>
                                            </div>
                                        </div>
                                        <input type="text" data-parsley-class-handler="#slWrapperDob"
                                            data-parsley-errors-container="#slErrorContainerDob"
                                            class="form-control fc-datepicker fc-datepicker-color fc-datepicker-indigo"
                                            value="{{ old('next_followup') }}" name="next_followup"
                                            placeholder="Next Followup Date" autocomplete="off" required>
                                    </div><!-- wd-200 -->
                                    <div id="slErrorContainerDob"></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div id="slWrapperStatus" class="parsley-select">
                                <div class="form-group">
                                    <label class="form-control-label">Followup Status:<span
                                            class="text-danger">*</span></label>
                                    <select class="form-control select2" name="status"
                                        data-parsley-class-handler="#slWrapperStatus"
                                        data-parsley-errors-container="#slErrorContainerStatus" required>
                                        <option value=""> ----Select Status---- </option>
                                        <option value="1">Follow Up</option>
                                        <option value="2">Converted</option>
                                        <option value="3">Delay</option>
                                        <option value="4">Dead</option>
                                    </select>
                                    <div id="slErrorContainerStatus"></div>
                                </div>
                            </div><!-- col-4 -->
                        </div>

                        <div class="col-md-4">
                            <div id="slWrapperSource" class="parsley-select">
                                <div class="form-group">
                                    <label class="form-control-label">Source:<span class="text-danger">*</span></label>
                                    <select class="form-control select2" name="source"
                                        data-parsley-class-handler="#slWrapperSource"
                                        data-parsley-errors-container="#slErrorContainerSource" required>
                                        <option value=""> ----Select Source---- </option>
                                        @foreach($sources as $source)
                                            <option value="{{ $source->id }}">{{ ucwords($source->name) }}</option>
                                        @endforeach
                                    </select>
                                    <div id="slErrorContainerSource"></div>
                                </div>
                            </div><!-- col-4 -->
                        </div>

                        @can('assign leads')
                            <div class="col-md-4">
                                <div id="slWrapperAgent" class="parsley-select">
                                    <div class="form-group">
                                        <label class="form-control-label">Assign to Agent:<span
                                                class="text-danger">*</span></label>
                                        <select class="form-control select2" name="agent_id"
                                            data-parsley-class-handler="#slWrapperAgent"
                                            data-parsley-errors-container="#slErrorContainerAgent"
                                            data-placeholder="Select Agent..." required>
                                            <option value=""> Select Agent..</option>
                                            @role('agent')
                                                <option value="{{ Auth::user()->profile->id }}" selected>{{ Auth::user()->profile->full_name }} (You)</option>
                                            @endrole
                                            @foreach($agents as $agent)
                                                @if($agent->user->hasRole('agent'))
                                                    <option value="{{ $agent->id }}">{{ $agent->full_name }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        <div id="slErrorContainerAgent"></div>
                                    </div>
                                </div>
                            </div>
                        @endcan
                       
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="form-control-label">Tags:</label>
                                <select class="form-control tags" name="tags[]" multiple="multiple"></select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-control-label">Lead Priority: </label>
                                    <div class="form-check form-check-inline mt-4 ">
                                        <input type="radio" id="customRadio1" name="priority" class="form-check-input" value="1">
                                        <label class="form-check-label" for="customRadio1">Hot</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input type="radio" id="customRadio2" name="priority" class="form-check-input" value="2" checked>
                                        <label class="form-check-label" for="customRadio2">Warm</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input type="radio" id="customRadio3" name="priority" class="form-check-input" value="3">
                                        <label class="form-check-label" for="customRadio3">Cold</label>
                                    </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="form-control-label">Comment/Remarks:<span
                                        class="text-danger">*</span></label>
                                <textarea class="form-control" name="notes" rows="5" placeholder="Comment/Remarks"
                                    required>{{ old('notes') }}</textarea>
                            </div>
                        </div>

                    </div><!-- row -->

                    <div class="form-layout-footer">
                        <button class="btn btn-primary bd-0">Add &amp; Save Lead</button>
                    </div><!-- form-layout-footer -->
                </div><!-- form-layout -->
            </form>
            <!-- Card Body -->
        </div>
    </div>
    </div>
</div>
@endsection
@push('scripts')
    <script>
        $(function(){
            // Datepicker
            $('.fc-datepicker').datetimepicker({
                timepicker: true,
                format:'d-m-Y H:i',
                formatDate:'Y-m-d',
                formatTime:'H:i',
                minDate: 0, // yesterday is minimum date
                inline: false,
            });

            $('.fc-dobpicker').datetimepicker({
                timepicker: false,
                format:'d-m-Y',
                formatDate:'Y-m-d',
                formatTime:'H:i',
                maxDate:'{{ date("Y/m/d") }}',
            });
        });

        $('.select2').select2();
        $('.tags').select2({
            tags: true,
            tokenSeparators: [',',' '],
            placeholder: "Lead related tags",
        });
    </script>
@endpush

