@if($field === 'name')
  <div class="d-flex align-items-center">
      <div><img src="{{ Avatar::create($row->profile->full_name)->toBase64() }}" class="img-thumbnail rounded-circle avatar-sm mr-1" /> </div>
      <div class="">
        <p class="font-weight-bold mb-0">{{ $row->profile->full_name }}</p>
        <p class="text-muted mb-0">{{ $row->profile->phone }}</p>
      </div>
  </div>
@endif

@if($field === 'agent')
  <div class="avatar-group avatar-group-sm avatar-group-overlapped mr-40 mt-25">
    <a href="javascript:void(0)" title="{{ ucwords($row->agent->full_name) }}" data-toggle="tooltip" data-placement="bottom">
        <img src="{{ Avatar::create($row->agent->full_name)->toBase64() }}" alt="user" class="img-thumbnail align-self-start rounded-circle avatar-sm">
    </a>
  </div>
@endif

@if($field === 'status')
  <button
      @if($row->status == 'Follow Up')
          class="btn btn-warning modal-link rounded-10 btn-sm"
      @elseif($row->status == 'Dead')
          class="btn btn-danger modal-link rounded-10  btn-sm"
      @else
          class="btn btn-success modal-link rounded-10 btn-sm"
      @endif
          data-title="Change Lead Stage/Status" data-href="{{ route('leads.status', $row->id) }}"
          title="Change Lead Stage/Status" data-toggle="tooltip">
          {{ $row->status }} <i class="uil uil-angle-right"></i>
  </button>
@endif

@if($field === 'actions')
  <div class="btn-group" role="group" aria-label="Controls">
    @if($row->status !== 'Converted')
        <button class="btn text-success modal-link" data-toggle="tooltip" data-placement="bottom"
            data-title="Add Followup" data-href="{{ route('leads.followup', $row->id) }}"
            title="Add Followup"><i class="uil uil-plus font-size-18"></i>
        </button>
    @else
        <button class="btn text-warning modal-link" data-toggle="tooltip" data-placement="bottom"
            data-title="Lead Activity Timeline" data-href="{{ route('leads.show', $row->id) }}"
            title="Lead Activity Timeline"><i class="uil uil-history font-size-18"></i>
        </button>
    @endif
    
    @can('edit leads')
    <a href="{{ route('leads.edit', $row->id ) }}" class="btn text-warning" data-toggle="tooltip" data-placement="bottom"
        title="Edit Details" data-turbolinks="true"><i class="uil uil-edit-alt font-size-18"></i></a>
    @endcan

    <button class="btn text-primary  modal-link" data-toggle="tooltip" data-placement="bottom"
        data-title="Lead Details" data-href="{{ route('leads.show', $row->id) }}"
        title="Lead Details"><i class="uil uil-info-circle font-size-18"></i>
    </button>

    @if(Auth::user()->hasRole('admin'))
    <a href="{{ route('leads.destroy', $row->id) }}" class="btn text-danger" data-toggle="tooltip" data-placement="bottom"
        title="Delete Lead Data"><i class="uil uil-trash-alt font-size-18"></i></a>
    @endif
  </div>
@endif