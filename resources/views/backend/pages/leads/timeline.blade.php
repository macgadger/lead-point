<div class="p-3">
@if($timeline->isNotEmpty())
    <ul class="verti-timeline list-unstyled">
        @foreach($timeline as $line)
            <li class="event-list">
                <div class="event-date text-primary">{{ $line->created_at->format('M d, y') }}</div>
                <h5>{{ strtoupper($line->status) }}</h5>
                <p class="text-muted">
                    Lead followed up by <span class="txt-bold">{{ ucfirst($line->followup_type) }}</span><br>
                    <span class="text-primary">Remarks: {{ ucfirst($line->notes) }}</span>
                </p>
                @if($line->status === 'Converted')
                    <p class="timeline-text">Closing Price: Rs {{ $line->lead->price_quoted }}.00</p>
                @endif
            </li>
        @endforeach
        <li class="event-list">
            <div class="event-date text-primary">{{ $line->lead->created_at->format('M d, y') }}</div>
            <h5>{{ strtoupper($line->lead->status) }}</h5>
            <p class="text-muted">
                Lead Created at {{ date('M d, Y', strtotime($line->lead->created_at)) }}<br>
                <span class="text-primary">By: {{ $line->lead->createdBy->full_name }}</span>
            </p>
        </li>
    </ul>
    @else
        <h3 class="tx-center">No Activity Timeline Present</h3>
    @endif
</div>