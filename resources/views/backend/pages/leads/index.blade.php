@extends('layouts.app')
@role('admin')
    @section('title', 'Leads')
@else
    @section('title', 'Your Leads')
@endrole
@section('turbolink','no-cache')
@section('content')
    <div class="row mb-3" x-data="{ tab: 'today' }" data-turbolinks="false">
        <div class="col-md-10">
            <!-- Nav tabs -->
            <ul class="nav nav-pills nav-justified-sm bg-light mt-1" role="tablist">
                <li class="nav-item waves-effect waves-light">
                    <a class="nav-link filter-link" href="javascript:void(0)" role="tab" 
                        :class="{ 'active': tab === 'today' }"
                        data-filter='today'
                        @click="tab = 'today'">
                        <span class="d-block d-sm-none"><i class="uil uil-calender"></i></span>
                        <span class="d-none d-sm-block"><i class="uil uil-calender text-success"></i> Today Followup</span>
                    </a>
                </li>
                <li class="nav-item waves-effect waves-light">
                    <a class="nav-link filter-link" href="javascript:void(0)" role="tab" 
                        :class="{ 'active': tab === 'active' }"
                        data-filter='active'
                        @click="tab = 'active'">
                        <span class="d-block d-sm-none"><i class="uil uil-bullseye"></i></span>
                        <span class="d-none d-sm-block"><i class="uil uil-bullseye text-warning"></i> Active Leads</span>
                    </a>
                </li>
                <li class="nav-item waves-effect waves-light">
                    <a class="nav-link filter-link" href="javascript:void(0)" role="tab" 
                        :class="{ 'active': tab === 'hot' }"
                        data-filter='hot'
                        @click="tab = 'hot'"> 
                        <span class="d-block d-sm-none"><i class="uil uil-fire"></i></span>
                        <span class="d-none d-sm-block"><i class="uil uil-fire text-danger"></i> Hot</span>
                    </a>
                </li>
                <li class="nav-item waves-effect waves-light">
                    <a class="nav-link filter-link" href="javascript:void(0)" role="tab" 
                        :class="{ 'active': tab === 'pending' }"
                        data-filter='pending'
                        @click="tab = 'pending'">
                        <span class="d-block d-sm-none"><i class="uil uil-hourglass"></i></span>
                        <span class="d-none d-sm-block"><i class="uil uil-hourglass text-dark"></i> Pending</span>
                    </a>
                </li>
                <li class="nav-item waves-effect waves-light">
                    <a class="nav-link filter-link" href="javascript:void(0)" role="tab"
                        :class="{ 'active': tab === 'converted' }"
                        data-filter='converted'
                        @click="tab = 'converted'">
                        <span class="d-block d-sm-none"><i class="uil uil-check-square"></i></span>
                        <span class="d-none d-sm-block"><i class="uil uil-check-square text-success"></i> Converted</span>
                    </a>
                </li>
                <li class="nav-item waves-effect waves-light">
                    <a class="nav-link filter-link" href="javascript:void(0)" role="tab"
                        :class="{ 'active': tab === 'dead' }"
                        data-filter='dead'
                        @click="tab = 'dead'">
                        <span class="d-block d-sm-none"><i class="uil uil-filter-slash"></i></span>
                        <span class="d-none d-sm-block"><i class="uil uil-filter-slash text-danger"></i> Dead</span>
                    </a>
                </li>
                <li class="nav-item waves-effect waves-light">
                    <a class="nav-link filter-link" href="javascript:void(0)" role="tab"
                        :class="{ 'active': tab === 'all' }"
                        @click="tab = 'all'"
                        data-filter="all">
                        <span class="d-block d-sm-none"><i class="uil uil-list-ul"></i></span>
                        <span class="d-none d-sm-block"><i class="uil uil-list-ul"></i> All Leads</span>
                    </a>
                </li>
            </ul>
        </div>

        <div class="col-md-2">
            @if(Auth::user()->profile->plan->leads_limit == $count)
                @if(auth()->user()->can('create leads'))
                    <a href="{{ route('leads.create') }}" class="btn btn-success btn-rounded waves-effect waves-light btn-block mt-1 float-right" data-turbolinks="true" >
                        <i class="uil uil-plus-circle me-1"></i> Create New Lead
                    </a>
                @else
                    &nbsp;
                @endif
            @endif
        </div>

    </div>

    <hr>

    <div class="row">
        <div class="col-lg-12">
            @if(Auth::user()->profile->plan->leads_limit == $count)
                <div class="alert alert-solid alert-danger" role="alert">
                    <strong>You have exceed the leads limit of {{ $count }} of {{ Auth::user()->profile->plan->leads_limit }} your Plan.</strong>
                    Please upgrade your account.
                </div><!-- alert -->
            @else
                <div class="alert alert-solid alert-success" role="alert">
                    <strong>Your Plan Leads Limit of {{ $count }} of {{ Auth::user()->profile->plan->leads_limit }}</strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div><!-- alert -->
            @endif

            <table class="table table-centered table-hover datatable dt-responsive nowrap table-card-list" style="border-collapse: collapse; border-spacing: 0 12px; width: 100%;">
                <thead>
                    <tr class="bg-transparent">
                        <th style="width: 230px;">Name</th>
                        <th>Inquiry for</th>
                        <th>Next Followup Date</th>
                        <th>Last Followup</th>
                        <th>Agent</th>
                        <th>Opportunity Cost</th>
                        <th>Stage</th>
                        <th style="width: 120px;">Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $(function () {
            var oTable = $('.datatable').DataTable({                
                responsive: true,
                processing: true,
                serverSide: true,
                ajax: "{{ route('leads.sort','today') }}",
                language: {
                    'loadingRecords': 'Loading....',
                    'processing': '<div class="spinner-border text-primary"><span class="sr-only">Loading...</span></div>'
                },   
                columns: [
                    {
                        data: 'name', 
                        name: 'profile.name', 
                        orderable: false, 
                        searchable: true
                    },
                    {data: 'inquiry_for', name: 'inquiry_for',orderable: false},
                    {data: 'next_followup', name: 'next_followup'},
                    {data: 'last_followup', name: 'last_followup',orderable: false},
                    {data: 'agent', name: 'agent.first_name',orderable: false},                    
                    {data: 'price_quoted', name: 'price_quoted',orderable: false},
                    {data: 'status', name: 'status'},
                    {
                        data: 'action', 
                        name: 'action', 
                        orderable: false, 
                        searchable: false
                    },
                ]
            });
            
            $(document).on('click', '.filter-link', function () {
                //
                var filter   = $(this).attr('data-filter');                
                oTable.ajax.url("http://localhost:8000/app/leads/sort-leads?"+filter).load();
            });
        });
    </script>
@endpush