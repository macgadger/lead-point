<div class="card p-3">
    <form name="statusForm" method="post" action="{{ route('leads.status', $lead->id) }}" data-parsley-validate>
        @csrf
        <div class="form-layout">
            <div class="row">

                <div class="col-md-12">
                    <div class="form-group">
                        <label class="form-control-label">Lead Status: <span class="text-danger">*</span></label>
                        <select class="form-control" name="status" id="follow_status" required>
                            <option value="1" @if($lead->status == 'Follow Up') selected @endif>Follow Up</option>
                            <option value="2" @if($lead->status == 'Converted') selected @endif>Converted</option>
                            <option value="3" @if($lead->status == 'Delay') selected @endif>Delay</option>
                            <option value="4" @if($lead->status == 'Dead') selected @endif>Dead</option>
                        </select>
                    </div>
                </div>

                <div class="col-md-12" id="closing_price" style="display: none">
                    <div class="form-group">
                        <label class="form-control-label">Closing Price: <span class="text-danger">*</span></label>
                        <input class="form-control" type="text" id="price_quoted" name="price_quoted" style="text-transform:capitalize"
                            placeholder="Enter Price Quoted" autocomplete="off">
                    </div>
                </div>

                <div class="col-md-12" id="remarks" style="display: none">
                    <div class="form-group">
                        <label class="form-control-label">Notes/Remarks: <span class="text-danger">*</span></label>
                        <textarea class="form-control" name="notes" rows="5" placeholder="Remarks/Notes"></textarea>
                    </div>
                </div>

                <div class="form-layout-footer mg-l-15">
                    <button class="btn btn-primary bd-0" style="display: none">Update</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div><!-- form-layout-footer -->

            </div>
        </div>
        <form>
</div>

<script>
$("#follow_status").change(function () {
  var selected_option = $('#follow_status').val();
  $('.btn-primary').show();
  if (selected_option === '2') {
    $('#closing_price').show();
    $('#remarks').show();
    $("#price_quoted").prop('required','true');
  }
  else if(selected_option === '1'){
    $('#remarks').hide();
    $("#price_quoted").prop('required',null);
    $("#closing_price").hide();
  }
  else {
    $("#remarks").prop('required',null);
    $('#remarks').show();
    $("#price_quoted").prop('required',null);
    $("#closing_price").hide();
  }
})
</script>

<style>
select {
    font-weight: bold !important;
    font-size: 18px;
}

select option {
    font-weight: bold !important;
    font-size: 18px;
}

select option[value="1"] {
  background: rgba(255, 230, 00, 1.0);
}

select option[value="2"] {
  background: rgba(0, 255, 0, 1.0);
}

select option[value="3"] {
  background: rgba(255, 149, 0, 1.0);
}

select option[value="4"] {
  background: rgba(255, 0, 0, 1.0);
  color: white;
}
</style>