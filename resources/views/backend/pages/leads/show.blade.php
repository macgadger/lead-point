<div class="card p-3">
<div class="table-responsive">
    <div>
        <p class="mb-1">Name :</p>
        <h5 class="font-size-16">{{ ucfirst($lead->profile->name) }}</h5>
    </div>
    <div class="mt-2">
        <p class="mb-1">Phone Number / Alt Phone Number :</p>
        <h5 class="font-size-16">{{ $lead->profile->phone }} {{ $lead->alt_phone ?? '' }}</h5>
    </div>
    @if(!empty($lead->email))
    <div class="mt-2">
        <p class="mb-1">Email :</p>
        <h5 class="font-size-16">{{ $lead->profile->email }}</h5>
    </div>
    @endif
    <div class="mt-2">
        <p class="mb-1">Address :</p>
        <h5 class="font-size-16">{{ $lead->profile->full_address }}</h5>
    </div>
    <div class="mt-2">
        <p class="mb-1">Inquiry for and Duration :</p>
        <h5 class="font-size-16">{{ ucfirst($lead->inquiry_for) }} {{ $lead->duration }}</h5>
    </div>
    <div class="mt-2">
        <p class="mb-1">Price Quoted :</p>
        <h5 class="font-size-16">Rs. {{ $lead->price_quoted }}.00</h5>
    </div>
    <div class="mt-2">
        <p class="mb-1">Source :</p>
        <h5 class="font-size-16">{{ $lead->sources->name }}</h5>
    </div>
    <div class="mt-2">
        <p class="mb-1">Remarks/Notes :</p>
        <h5 class="font-size-16">{{ $lead->notes }}</h5>
    </div>

</div>
</div>
