@extends('layouts.app')
@section('turbolink','no-cache')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div>
            <div class="float-right">
                <button type="button" class="btn btn-danger waves-effect waves-light mb-3 right-bar-toggle"><i class="uil uil-filter mr-1"></i> Add Quick Lead</button>
            </div>
            <div class="page-title-box">
                <h4 class="mg-b-5">Welcome back, {{ Auth::user()->profile->full_name}} !</h4>
                <p class="mg-b-0"><q class="font-size-16" style="border-bottom: 2px dotted" data-turbolinks="false">{{ \Illuminate\Foundation\Inspiring::quote() }}</q> - {{ date('M d, Y') }}</p>
            </div>
        </div>
    </div>
</div>
<div class="row" data-turbolinks="true">
    <div class="col-md-3">
        @livewire('analytics.leads')
    </div> <!-- end col-->

    <div class="col-md-3">
        @livewire('analytics.followup')
    </div> <!-- end col-->

    <div class="col-md-3">
        @livewire('analytics.converted')
    </div> <!-- end col-->

    <div class="col-md-3">
        @livewire('analytics.visits')
    </div> <!-- end col-->

    @role('admin')
        <div class="col-md-6">
        @livewire('analytics.payments')
        </div> <!-- end col-->

        <div class="col-md-6">
            @livewire('analytics.sources')
        </div>

        <div class="col-md-6">
            @livewire('analytics.summary')
        </div>

        <div class="col-md-6">
            @livewire('analytics.activity')
        </div>
    @endrole

    @role('agent')
        <div class="col-md-12">
            @livewire('tables.today-leads')
        </div>
    @endrole
</div>
@endsection