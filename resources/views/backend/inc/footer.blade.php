<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">
                2019-{{ date('Y') }} © Curvet.
            </div>
            <div class="col-sm-6">
                <div class="text-sm-right d-none d-sm-block">
                    Crafted with <i class="uil uil-heart text-danger"></i> by <a href="https://overleap.co.in/" target="_blank" class="text-reset">Overleap</a>
                </div>
            </div>
        </div>
    </div>
</footer>