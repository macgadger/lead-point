<!-- Right Sidebar -->
<div class="right-bar">
    <div data-simplebar class="h-200">
        <div class="rightbar-title px-3 py-4">
            <a href="javascript:void(0);" class="right-bar-toggle float-right">
                <i class="uil uil-times noti-icon"></i>
            </a>
            <h5 class="m-0">Quick Lead</h5>
        </div>

        <!-- Settings -->
        <hr class="mt-0" />

        <div class="p-4">

            <form name="leadForm" method="post" action="{{ route('leads.quick') }}" data-parsley-validate>
                @csrf
                <div class="form-layout">
                    <div class="row mg-b-25">

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="form-control-label">Person/Company Name: <span
                                        class="text-danger">*</span></label>
                                <input class="form-control" type="text" name="name"
                                    style="text-transform:capitalize" value="{{ old('name') }}" minlength="3"
                                    placeholder="Enter Person or Company Name" required>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="form-control-label">Contact Number: <span
                                        class="text-danger">*</span></label>
                                <input class="form-control" type="phone" name="phone" value="{{ old('phone') }}"
                                    minlength="10" maxlength="13" placeholder="Enter Mobile Number" required>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="form-control-label">Email:</label>
                                <input class="form-control" type="email" name="email" value="{{ old('email') }}" placeholder="Enter email address">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="form-control-label">Inquiry For: <span
                                        class="text-danger">*</span></label>
                                <input class="form-control" type="text" name="inquiry_for"
                                    style="text-transform:capitalize" placeholder="Enter Inquiry For" required>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="form-control-label">Price Quoted:</label>
                                <input class="form-control" type="text" name="price_quoted"
                                    style="text-transform:capitalize" placeholder="Enter Price Quoted">
                            </div>
                        </div>

                    </div>
                </div>
                <hr>
                <div class="form-layout-footer">
                    <button class="btn btn-primary bd-0">Add Lead</button>
                </div><!-- form-layout-footer -->

            </form>


        </div>

    </div> <!-- end slimscroll-menu-->
</div>
<!-- /Right-bar -->

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div> 
<!-- END Right Sidebar -->