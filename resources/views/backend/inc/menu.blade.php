<div class="container-fluid">
    <div class="topnav">

        <nav class="navbar navbar-light navbar-expand-lg topnav-menu">

            <div class="collapse navbar-collapse" id="topnav-menu-content">
                <ul class="navbar-nav">

                    @auth('web')
                        <li class="nav-item">
                            <a class="{{ request()->routeIs('dashboard') ? 'nav-link active' : 'nav-link' }}" href="{{ route('dashboard') }}">
                                <i class="uil-dashboard mr-2"></i> Dashboard
                            </a>
                        </li>

                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle arrow-none" href="#" id="topnav-pages" role="button">
                                <i class="uil uil-filter me-2"></i>Leads <div class="arrow-down"></div>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="topnav-pages">
                                <a href="{{ route('leads.index') }}" class="dropdown-item">Leads</a>
                                <a href="{{ route('quick-leads.index') }}" class="dropdown-item">Quick Leads</a>
                            </div>
                        </li>

                        {{-- <li class="nav-item">
                            <a class="{{ request()->routeIs('calendar.*') ? 'nav-link active' : 'nav-link' }}" href="{{ route('calendar.index') }}">
                                <i class="uil uil-calendar-alt"></i> Calendar
                            </a>
                        </li> --}}

                        <li class="nav-item">
                            <a class="{{ request()->routeIs('reports.*') ? 'nav-link active' : 'nav-link' }}" href="{{ route('reports.index') }}">
                                <i class="uil uil-chart-line"></i> Reporting
                            </a>
                        </li>

                        @role('admin')
                            <li class="nav-item">
                                <a class="{{ request()->routeIs('clients.*') ? 'nav-link active' : 'nav-link' }}" href="{{ route('clients.index') }}">
                                    <i class="uil uil-user-square"></i> My Clients
                                </a>
                            </li>
                            

                            <li class="nav-item">
                                <a class="{{ request()->routeIs('employee.*') ? 'nav-link active' : 'nav-link' }}" href="{{ route('employee.index') }}">
                                    <i class="uil uil-users-alt"></i> My Team
                                </a>
                            </li>

                            <li class="nav-item">
                                <a class="{{ request()->routeIs('profile.employee.online') ? 'nav-link active' : 'nav-link' }}" href="{{ route('profile.employee.online') }}">
                                    <i class="uil uil-lightbulb"></i> Users Online
                                </a>
                            </li>

                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle arrow-none" href="#" id="topnav-pages" role="button">
                                    <i class="uil uil-sliders-v-alt me-2"></i>Admin Settings <div class="arrow-down"></div>
                                </a>
                                <div class="dropdown-menu" aria-labelledby="topnav-pages">
                                    <a href="{{ route('lead-sources.index') }}" class="dropdown-item">Lead Sources</a>
                                </div>
                            </li>
                        @endrole

                        <li class="nav-item">
                            <a class="{{ request()->routeIs('messages.*') ? 'nav-link active' : 'nav-link' }}" href="{{ route('messages.index') }}">
                                <i class="uil uil-envelope-minus"></i> Messages
                            </a>
                        </li>
                    @endauth

                    @auth('master')
                        <li class="nav-item">
                            <a class="{{ request()->routeIs('master.dashboard') ? 'nav-link active' : 'nav-link' }}" href="{{ route('master.dashboard') }}">
                                <i class="uil-dashboard mr-2"></i> Dashboard
                            </a>
                        </li>

                        <li class="nav-item dropdown">
                            <a class="{{ request()->routeIs('client.*') ? 'active' : '' }} nav-link dropdown-toggle arrow-none" href="#" id="topnav-layout"
                                role="button" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">
                                <i class="uil uil-user-square"></i> My Clients<div class="arrow-down">
                                </div>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="topnav-layout">
                                <a href="{{ route('client.create') }}" class="dropdown-item">Create New Client</a>
                                <a href="{{ route('client.index') }}" class="dropdown-item">Clients List</a>
                            </div>
                        </li>
    
                        <li class="nav-item dropdown">
                            <a class="{{ request()->routeIs('plans.*') ? 'active' : '' }} nav-link dropdown-toggle arrow-none" href="#" id="topnav-layout"
                                role="button" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">
                                <i class="uil uil-pricetag-alt"></i> Plans<div class="arrow-down">
                                </div>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="topnav-layout">
                                <a href="{{ route('plans.create') }}" class="dropdown-item">Create Plan</a>
                                <a href="{{ route('plans.index') }}" class="dropdown-item">View Plans</a>
                            </div>
                        </li>
                    @endauth
                </ul>
            </div>
        </nav>
    </div>
</div>