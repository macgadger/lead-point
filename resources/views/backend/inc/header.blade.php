<div class="navbar-header">
    <div class="d-flex">
        <!-- LOGO -->
        <div class="navbar-brand-box">
            <a href="{{ route('dashboard') }}" class="logo logo-dark">
                <span class="logo-sm">
                    <img src="{{ asset('web/media/bdm.png') }}" alt="" height="26">
                </span>
                <span class="logo-lg">
                    <img src="{{ asset('web/media/bdm.png') }}" alt="" height="20">
                </span>
            </a>

            <a href="{{ route('dashboard') }}" class="logo logo-light">
                <span class="logo-sm">
                    <img src="{{ asset('web/media/bdm.png') }}" alt="" height="26">
                </span>
                <span class="logo-lg">
                    <img src="{{ asset('web/media/bdm.png') }}" alt="" height="20">
                </span>
            </a>
        </div>

        <button type="button" class="btn btn-sm px-3 font-size-16 d-lg-none header-item waves-effect waves-light" data-toggle="collapse" data-target="#topnav-menu-content">
            <i class="uil uil-bars"></i>
        </button>

        <!-- App Search-->
        <form class="app-search d-none d-lg-block">
            <div class="position-relative">
                <input type="text" class="form-control" placeholder="Search...">
                <span class="uil-search"></span>
            </div>
        </form>
    </div>

    <div class="d-flex">

        <div class="dropdown d-inline-block d-lg-none ml-2">
            <button type="button" class="btn header-item noti-icon waves-effect" id="page-header-search-dropdown"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="uil-search"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right p-0"
                aria-labelledby="page-header-search-dropdown">
    
                <form class="p-3">
                    <div class="form-group m-0">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search ..." aria-label="Recipient's username">
                            <div class="input-group-append">
                                <button class="btn btn-primary" type="submit"><i class="mdi mdi-magnify"></i></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="dropdown d-none d-lg-inline-block ml-1">
            <button type="button" class="btn header-item noti-icon waves-effect" data-toggle="fullscreen">
                <i class="uil-minus-path"></i>
            </button>
        </div>

        {{-- <div class="dropdown d-inline-block" >
            <button type="button" class="btn header-item noti-icon waves-effect dropdown-toggle" id="page-header-notifications-dropdown"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="uil-bell"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right p-0"
                aria-labelledby="page-header-notifications-dropdown">
                <div class="p-3">
                    <div class="row align-items-center">
                        <div class="col">
                            <h5 class="m-0 font-size-16"> Notifications </h5>
                        </div>
                        <div class="col-auto">
                            <a href="#!" class="small"> Mark all as read</a>
                        </div>
                    </div>
                </div>
                <div data-simplebar style="max-height: 230px;">
                    <a href="#" class="text-reset notification-item">
                        <div class="media">
                            <div class="avatar-xs mr-3">
                                <span class="avatar-title bg-primary rounded-circle font-size-16">
                                    <i class="uil-shopping-basket"></i>
                                </span>
                            </div>
                            <div class="media-body">
                                <h6 class="mt-0 mb-1">Your order is placed</h6>
                                <div class="font-size-12 text-muted">
                                    <p class="mb-1">If several languages coalesce the grammar</p>
                                    <p class="mb-0"><i class="mdi mdi-clock-outline"></i> 3 min ago</p>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="#" class="text-reset notification-item">
                        <div class="media">
                            <img src="{{ asset('assets/images/users/avatar-3.jpg') }}"
                                class="mr-3 rounded-circle avatar-xs" alt="user-pic">
                            <div class="media-body">
                                <h6 class="mt-0 mb-1">James Lemire</h6>
                                <div class="font-size-12 text-muted">
                                    <p class="mb-1">It will seem like simplified English.</p>
                                    <p class="mb-0"><i class="mdi mdi-clock-outline"></i> 1 hours ago</p>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="#" class="text-reset notification-item">
                        <div class="media">
                            <div class="avatar-xs mr-3">
                                <span class="avatar-title bg-success rounded-circle font-size-16">
                                    <i class="uil-truck"></i>
                                </span>
                            </div>
                            <div class="media-body">
                                <h6 class="mt-0 mb-1">Your item is shipped</h6>
                                <div class="font-size-12 text-muted">
                                    <p class="mb-1">If several languages coalesce the grammar</p>
                                    <p class="mb-0"><i class="mdi mdi-clock-outline"></i> 3 min ago</p>
                                </div>
                            </div>
                        </div>
                    </a>

                    <a href="#" class="text-reset notification-item">
                        <div class="media">
                            <img src="{{ asset('assets/images/users/avatar-4.jpg') }}"
                                class="mr-3 rounded-circle avatar-xs" alt="user-pic">
                            <div class="media-body">
                                <h6 class="mt-0 mb-1">Salena Layfield</h6>
                                <div class="font-size-12 text-muted">
                                    <p class="mb-1">As a skeptical Cambridge friend of mine occidental.</p>
                                    <p class="mb-0"><i class="mdi mdi-clock-outline"></i> 1 hours ago</p>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="p-2 border-top">
                    <a class="btn btn-sm btn-link font-size-14 btn-block text-center" href="javascript:void(0)">
                        <i class="uil-arrow-circle-right mr-1"></i> View More..
                    </a>
                </div>
            </div>
        </div> --}}
        @auth('web')
            <div class="dropdown d-inline-block">
                <button type="button" class="btn header-item waves-effect dropdown-toggle" id="page-header-user-dropdown"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    @if(Auth::user()->profile->avatar === 'default')
                        <img class="rounded-circle header-profile-user" src="{{ Avatar::create(Auth::user()->profile->full_name)->toBase64() }}" alt="Header Avatar">
                    @else
                        <img class="rounded-circle header-profile-user" src="{{ asset('assets/images/users/avatar-4.jpg') }}" alt="Header Avatar">
                    @endif
                    <span class="d-none d-xl-inline-block ml-1 font-weight-medium font-size-15">{{ Auth::user()->profile->short_name }}</span>
                    <i class="uil-angle-down d-none d-xl-inline-block font-size-15"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-right" data-turbolink="false">
                    <!-- item-->
                    <a class="dropdown-item" href="{{ route('profile.view',Auth::user()->profile->id) }}"><i class="uil uil-user-circle font-size-18 align-middle text-muted mr-1"></i> <span class="align-middle">My Account</span></a>
                    <a class="dropdown-item" href="#"><i class="uil uil-lock-alt font-size-18 align-middle mr-1 text-muted"></i> <span class="align-middle">Lock screen</span></a>
                    <a class="dropdown-item" href="{{ route('account.logout') }}"><i class="uil uil-sign-out-alt font-size-18 align-middle mr-1 text-muted"></i> <span class="align-middle">Sign out</span></a>
                </div>
            </div>
        @endauth

        @auth('master')
            <div class="dropdown d-inline-block">
                <button type="button" class="btn header-item waves-effect dropdown-toggle" id="page-header-user-dropdown"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img class="rounded-circle header-profile-user" src="{{ Avatar::create(Auth::user()->name)->toBase64() }}" alt="Header Avatar">                    
                    <span class="d-none d-xl-inline-block ml-1 font-weight-medium font-size-15">{{ ucwords(Auth::user()->name) }}</span>
                    <i class="uil-angle-down d-none d-xl-inline-block font-size-15"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-right" data-turbolink="false">
                    <!-- item-->                    
                    <a class="dropdown-item" href="{{ route('master.logout') }}"><i class="uil uil-sign-out-alt font-size-18 align-middle mr-1 text-muted"></i> <span class="align-middle">Sign out</span></a>
                </div>
            </div>
        @endauth

    </div>
</div>