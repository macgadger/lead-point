<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="turbolinks-root" content="/app">
        <meta name="turbolinks-cache-control" content="@yield('turbolink')">

        <title>@yield('title','Dashboard') - {{ env('APP_NAME') }}</title>
        
        <!-- Bootstrap SPA App -->
        <script src="{{ asset('js/spa-bootstrap.js') }}" defer data-turbolinks-eval="false"></script>
        
        <!-- Icons -->
        <link rel="stylesheet" href="https://unicons.iconscout.com/release/v3.0.6/css/line.css">
        <!-- Bootstrap and App Styles -->
        <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" data-turbolinks-eval="false">
        <link href="{{ asset('css/plugins.css') }}" rel="stylesheet" data-turbolinks-track="reload">
        <link href="{{ asset('assets/css/app.min.css') }}" rel="stylesheet" data-turbolinks-eval="false">
        <link href="{{ asset('css/styles.css') }}" rel="stylesheet" data-turbolinks-track="reload">
        
        @livewireStyles
    </head>
    <body data-layout="horizontal" data-topbar="dark">

        <!-- Modal -->
        @include('backend.inc.modal')
        
        <!-- Begin page -->
        <div id="layout-wrapper">
            <header id="page-topbar">
                @include('backend.inc.header')
                @include('backend.inc.menu')
            </header>

            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">
                    <div class="container-fluid">
                        @if(request()->routeIs('dashboard') || request()->routeIs('master.dashboard'))
                            @yield('content')
                        @else
                            <!-- start page title -->
                            <div class="row">
                                <div class="col-12">
                                    <div class="page-title-box d-flex align-items-center justify-content-between">
                                        <h4 class="mb-0">@yield('title')</h4>

                                        <div class="page-title-right">
                                            <ol class="breadcrumb m-0">
                                                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                                                <li class="breadcrumb-item active">@yield('title')</li>
                                            </ol>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            
                            @yield('content')
                        @endif
                    </div> <!-- container-fluid -->
                </div>
                <!-- End Page-content -->

                @include('backend.inc.footer')
            </div>
            <!-- end main content-->
            
        </div>
        <!-- END layout-wrapper -->

        @include('backend.inc.quick')

        <!-- Bootstrap, jQuery default initializations -->
        <script src="{{ mix('js/app.js') }}" data-turbolinks-track="true"></script>
        <script src="{{ asset('assets/libs/toastr/build/toastr.min.js') }}"></script>

        <script>
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": true,
                "progressBar": true,
                "positionClass": "toast-bottom-right",
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": 300,
                "hideDuration": 1000,
                "timeOut": 5000,
                "extendedTimeOut": 1000,
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };

            @if(session('success'))
                toastr.info('{{ session('success') }}');
            @elseif(session('error'))
                toastr.error('{{ session('error') }}');
            @endif

            $(document).on('click', '.modal-link', function () {
                //
                //$('#preloader').show();
                var dataURL   = $(this).attr('data-href');
                var dataTitle = $(this).attr('data-title');
                $('.modal-title').html(dataTitle);
                $('.modal-body').load(dataURL, function () 
                {
                    $('#modal-view').modal({
                        backdrop: 'static',
                        keyboard: true,
                        show: true
                    });
                });
            });
        </script>

        <!-- Custom Load Scripts -->
        @stack('scripts')
        @livewireScripts
        <script src="https://cdn.jsdelivr.net/gh/livewire/turbolinks@v0.1.x/dist/livewire-turbolinks.js" data-turbolinks-eval="false"></script>
    </body>
</html>
