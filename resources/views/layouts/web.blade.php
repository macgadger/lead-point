<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1">
      <title>@yield('title') - Business Development Manager</title>
    
    
      <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('web/fav/apple-touch-icon.png') }}">
      <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('web/fav/favicon-32x32.png') }}">
      <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('web/fav/favicon-16x16.png') }}">
      
      <meta name="msapplication-TileColor" content="#504A9D">
      <meta name="theme-color" content="#504A9D">

      <link rel="stylesheet" href="{{ asset('web/dependencies/bootstrap/css/bootstrap.min.css') }}" type="text/css">
      <link rel="stylesheet" href="{{ asset('web/dependencies/fontawesome/css/all.min.css') }}" type="text/css">
      <link rel="stylesheet" href="{{ asset('web/dependencies/swiper/css/swiper.min.css') }}" type="text/css">
      <link rel="stylesheet" href="{{ asset('web/dependencies/wow/css/animate.css') }}" type="text/css">
      <link rel="stylesheet" href="{{ asset('web/dependencies/magnific-popup/css/magnific-popup.css') }}" type="text/css">
      <link rel="stylesheet" href="{{ asset('web/dependencies/components-elegant-icons/css/elegant-icons.min.css') }}" type="text/css">
      <link rel="stylesheet" href="{{ asset('web/dependencies/simple-line-icons/css/simple-line-icons.css') }}" type="text/css">
      <link rel="stylesheet" href="{{ asset('web/assets/css/app.css') }}" type="text/css">
      <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800" rel="stylesheet">

   </head>
   <body id="home-version-1" class="home-version-1" data-style="default">
      <a href="#main_content" data-type="section-switch"
         class="return-to-top"><i class="fa fa-chevron-up"></i></a>
      <div class="page-loader">
        <div class="loader">
            <div class="blobs">
               <div class="blob-center"></div>
               <div class="blob"></div>
               <div class="blob"></div>
               <div class="blob"></div>
               <div class="blob"></div>
               <div class="blob"></div>
               <div class="blob"></div>
            </div>
            <svg xmlns="http://www.w3.org/2000/svg" version="1.1">
               <defs>
                  <filter id="goo">
                     <feGaussianBlur in="SourceGraphic" stdDeviation="10" result="blur" />
                     <feColorMatrix in="blur" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 18 -7" result="goo" />
                     <feBlend in="SourceGraphic" in2="goo" />
                  </filter>
               </defs>
            </svg>
        </div>
      </div>

        <div id="main_content">
            @include('frontend.inc.header')
            @yield('content')
            @include('frontend.inc.footer')
        </div>

      <script src="{{ asset('web/dependencies/jquery/jquery.min.js') }}"></script>
      <script src="{{ asset('web/dependencies/bootstrap/js/bootstrap.min.js') }}"></script>
      <script src="{{ asset('web/dependencies/swiper/js/swiper.min.js') }}"></script>
      <script src="{{ asset('web/dependencies/jquery.appear/jquery.appear.js') }}"></script>
      <script src="{{ asset('web/dependencies/wow/js/wow.min.js') }}"></script>
      <script src="{{ asset('web/dependencies/countUp.js/countUp.min.js') }}"></script>
      <script src="{{ asset('web/dependencies/isotope-layout/isotope.pkgd.min.js') }}"></script>
      <script src="{{ asset('web/dependencies/imagesloaded/imagesloaded.pkgd.min.js') }}"></script>
      <script src="{{ asset('web/dependencies/jquery.parallax-scroll/js/jquery.parallax-scroll.js') }}"></script>
      <script src="{{ asset('web/dependencies/magnific-popup/js/jquery.magnific-popup.min.js') }}"></script>
      <script src="{{ asset('web/dependencies/gmap3/js/gmap3.min.js') }}"></script>
     
      <script src="{{ asset('web/assets/js/header.js') }}"></script>
      <script src="{{ asset('web/assets/js/app.js') }}"></script>
   </body>
</html>