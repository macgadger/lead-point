require('alpinejs')
const Turbolinks = require("turbolinks")
Turbolinks.start();

document.addEventListener('turbolinks:load', function() {
    $('.dropdown-toggle').dropdown();
    $('[data-toggle="tooltip"]').tooltip();
});