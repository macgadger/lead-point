require('./bootstrap');
require('./init');

require('parsleyjs');
require('select2');
require('datetimepicker-jquery');
require('password-strength-meter');

/* Datatables */ 
require('datatables.net-bs4');
require('datatables.net-responsive-bs4');

// require('fullcalendar');

/* Datatables Buttons and Export */ 
/*require('datatables.net-buttons-bs4');
require( 'jszip');
require( 'pdfmake');
require( 'datatables.net-buttons/js/buttons.colVis.js');
require( 'datatables.net-buttons/js/buttons.html5.js');
require( 'datatables.net-buttons/js/buttons.print.js');*/

// document.addEventListener("turbolinks:load", function() {
//     $('.parsley-validate').parsley();
//     $('.select2').select2();
//     $('.timepicker').datetimepicker();
// });

