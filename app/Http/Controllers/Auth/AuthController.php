<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\ForgotPassword;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{

    //
    public function index()
    {
        if(Auth::check()){
            return redirect()->intended('app/dashboard');
        }else{
            return redirect()->intended('account/signin/');
        }
    }

    /**
     * Open User signin form
    */
    public function signin(){
        //
        return view('auth.loginnew');
    }

    /**
    * Authenticate user and Logged in
    */
    public function authenticate(Request $request){
        //
        $validator = Validator::make($request->all(),[
            'email'    => 'required|email',
            'password' => 'required|min:6',
        ]);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }
        
        if (Auth::attempt($request->only('email', 'password'))) {
            // Authentication passed...
            $redirectTo = 'app/dashboard';
            return redirect()->intended($redirectTo);
        }
        else {
        // Authentication failed...
        return redirect()->back()->with('error', 'Invalid Username or Password');
        }

    }

    /**
    * Signout current signed in user
    */
    public function signout(){
        //
        if(Auth::check()) {
            Cache::forget('user-is-online-'.Auth::user()->id);
        }
        Auth::logout();
        Session::flush();
        //Cache::flush();
        return redirect()->intended('account/signin');
    }

    public function reset(Request $request, $id = NULL){
        //
        if($request->isMethod('POST')){
            //
            $user = User::where('email',$request->email)->first();
            if($user){
                //return (new ForgotPassword(Crypt::encryptString($user->id)))->render();
                Mail::to($request->email)->send(new ForgotPassword(Crypt::encryptString($user->id)));                
                if(Mail::failures()) {
                    return response()->json([
                        'status'  => false,                        
                        'message' => 'Not sending mail.. retry again...'
                    ]);
                }
                return redirect()->back()->with('success', 'Password reset link has been sent to your email. Please check your email');
            }else{
                return redirect()->back()->with('error', 'No User found with this email address');
            }
        }else{
            if($id != NULL) {                
                return view('auth.reset',compact('id'));
            }else{
                return view('auth.forgot');
            }
        }
    }

    public function resetPass(Request $request){
        //
        $this->validate($request, [
            'password'   => 'required|min:6|confirmed',
        ]);
        $key = Crypt::decryptString($request->key);
        User::find($key)->update([
            'password' => Hash::make($request->password)
        ]);
        return redirect()->back()->with('success', 'Password reset successfully! Please login to continue');
    }

    /**
     * Open User signin form master
    */
    public function masterSignin(){
        //
        return view('auth.login');
    }

    /**
    * Authenticate user and Logged in master
    */
    public function masterAuthenticate(Request $request){
        //
        $validator = Validator::make($request->all(),[
            'email'    => 'required|email',
            'password' => 'required|min:6',
        ]);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }
        
        if (Auth::guard('master')->attempt($request->only('email', 'password'))) {
            // Authentication passed...
            $redirectTo = 'master/dashboard';
            return redirect()->intended($redirectTo);
        }
        else {
        // Authentication failed...
        return redirect()->back()->with('error', 'Invalid Username or Password');
        }

    }

    /**
    * Signout current signed in master
    */
    public function masterSignout(){
        //        
        Auth::guard('master')->logout();
        Session::flush();
        Cache::flush();
        return redirect()->intended('account/master');
    }

}
