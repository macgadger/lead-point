<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Http\Requests\ClientRequest;
use App\Models\Admin;
use App\Models\Plans;
use App\Models\User;
use App\Models\UserProfile;
use Carbon\Carbon;
use DB;
use Hash;
use Illuminate\Http\Request;
use Str;

class ClientsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $clients = User::with('profile')
                        ->whereHas('roles',function($query) {
                            return $query->where('name','admin');
                        })
                        ->get();
        
        return view('master.clients.index',[
            'clients' => $clients,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $plans = Plans::where('status',1)->get();
        return view('master.clients.new',compact('plans'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClientRequest $request)
    {
        //
        if($request->validated()){
            // Begin Transaction
            DB::beginTransaction();
            try {
                //
                $uuid   = Str::uuid();
                $ucid   = strtoupper(Str::substr($request->company_name , 0, 4).'-'.Str::substr($uuid, 9, 4).'-'.Str::substr($uuid, 19, 4));

                //Create New Client Account(Admin)
                $client = new User;
                $client->ucid  = $ucid;
                $client->email = $request->username;
                $client->password = Hash::make($request->password);
                $client->status   = $request->status;
                $client->assignRole('admin');
                $client->save();

                if($request->hasFile('user_img'))
                {
                    $image           = $request->file('user_img');
                    $user_pic        = time().'.'.$image->getClientOriginalExtension();
                    $destinationPath = public_path('/assets/avatars/');
                    $request['avatar']   = $user_pic;
                    $image->move($destinationPath, $user_pic);
                }

                if($request->hasFile('user_logo'))
                {
                    $image           = $request->file('user_logo');
                    $user_logo        = time().'.'.$image->getClientOriginalExtension();
                    $destinationPath = public_path('/assets/logos/');
                    $request['company_logo']   = $user_logo;
                    $image->move($destinationPath, $user_logo);
                }

                $request['uid']  = $client->id;
                $request['ucid'] = $client->ucid;
                $request['plan_id']  = $request->plan_id;
                $request['plan_duration'] = $request->plan_duration;
                $request['plan_expiry']   = Carbon::now()->addMonth($request->plan_duration)->format('Y-m-d');
                
                UserProfile::create($request->all());
                //
                DB::commit();
            }
            catch(\Exception $e){
                DB::rollback();
                dd($e->getMessage()); 
            }
            return redirect()
                    ->route('client.index')
                    ->with('success','New Client has been created Successfully!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $client = User::with('profile')->find($id);
        $plans  = Plans::where('status',1)->get();
        return view('master.clients.edit',compact('client','plans'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Begin Transaction
        DB::beginTransaction();
        try {
            //
            $client  = User::find($id);            
            $client->status       = $request->status;
            if($request->password != NULL){
                $client->password = Hash::make($request->password);
            }
            $client->update();

            $profile = UserProfile::where('uid',$client->id)->first();

            if($request->hasFile('user_img'))
            {
                $image           = $request->file('user_img');
                $user_pic        = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('/assets/avatars/');
                $request['avatar']   = $user_pic;
                $image->move($destinationPath, $user_pic);
                if($profile->avatar != 'default.jpg'){
                    unlink($destinationPath.$profile->avatar);
                }
            }

            if($request->hasFile('user_logo'))
            {
                $image           = $request->file('user_logo');
                $user_logo       = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('/assets/logos/');
                $request['company_logo']   = $user_logo;
                $image->move($destinationPath, $user_logo);
                if($profile->company_logo != 'logo-default.jpg'){
                    unlink($destinationPath.$profile->company_logo);
                }
            }

            $request['plan_id']  = $request->plan_id;
            $request['plan_duration'] = $request->plan_duration;
            $request['plan_expiry']   = Carbon::now()->addMonth($request->plan_duration)->format('Y-m-d');
            $profile->update($request->all());
            //
            DB::commit();
        }
        catch(\Exception $e){
            DB::rollback();
            dd($e->getMessage()); 
        }
        return redirect()
                ->route('client.index')
                ->with('success','Client information Updated Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
