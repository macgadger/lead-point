<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Models\TrialRequest;
use App\Models\User;
use App\Models\UserProfile;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class MasterController extends Controller
{
    //
    public function index(){
        //

        $today = Carbon::today();

        $analytics['clients']  = User::whereHas('roles',function($query) {
                                    return $query->where('name','admin');
                                 })
                                ->selectRaw("count(case when date(created_at) = ? then 0 end) as today",[$today->format('Y-m-d')])
                                ->selectRaw('count(*) as total')
                                ->first();
        
        $analytics['requests'] = DB::table('trial_request')
                                    ->selectRaw("count(case when date(created_at)   = ? then 0 end) as today",[$today->format('Y-m-d')]) 
                                    ->selectRaw("count(case when month(created_at)  = ? then 0 end) as month",[$today->format('n')])
                                    ->first();
        
        $admins                 =   User::with('profile')
                                    ->whereHas('roles',function($query) {
                                        return $query->where('name','admin');
                                    })
                                    ->pluck('id');

        $analytics['plans']     =  UserProfile::whereIn('uid',$admins)
                                    ->selectRaw("count(case when date(plan_expiry)   = ? then 0 end) as today",[$today->format('Y-m-d')]) 
                                    ->selectRaw("count(case when month(plan_expiry)  = ? then 0 end) as month",[$today->format('n')])
                                    ->first();

        return view('master.dashboard',[
            'analytics' => (object)$analytics
        ]);
    }

    public function filter($type){
        //
        $admins =   User::with('profile')
                            ->whereHas('roles',function($query) {
                                return $query->where('name','admin');
                            })
                            ->pluck('id');

        if($type == 'clients')
        {

            $clients  = UserProfile::whereIn('uid',$admins)->get();
        }

        if($type == 'trials')
        {
            $clients  = TrialRequest::all();
        }

        if($type == 'expiring')
        {
            $clients  = UserProfile::whereIn('uid',$admins)
                        ->whereMonth('plan_expiry',Carbon::today()->format('n'))
                        ->get();
        }

        
        return view('master.filter',compact('type','clients'));
    }
}
