<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Leads;
use App\Models\User;
use App\Models\UserProfile;
use Arr;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    //
    public function index(){
        $agents = UserProfile::companyEmployees()->get();
        return view('backend.pages.report.index',[
            'agents' => $agents,
        ]);
    }

    //
    public function generate(Request $request){
        //
        $agents = UserProfile::companyEmployees()->get();
        $leads  = (new Leads)->newQuery();
        
        // Search Followup Type.
        if($request->has('type')) {
            //
            if($request->type == 'pending'){
                //
                $today = Carbon::today();  
                $leads = $leads->companyLeads()->where('status',1)->whereDate('next_followup','<',$today);
            }
            if($request->type == 'visit'){
                //
                $today = Carbon::today();  
                $leads = $leads->companyLeads()->where('followup_type','walkin');
            }
            else{
                $leads = $leads->companyLeads()->where('status',$request->type);
            }
        }
        if($request->has('agents')) {
            if($request->agents == 'all'){
                $users  = UserProfile::companyEmployees()->pluck('id');
                $leads = $leads->whereIn('agent_id',$users);
            }else{
                $leads = $leads->where('agent_id', $request->agents);
            }
        }
        else{
            $leads = $leads->where('agent_id', Auth::user()->profile->id);
        }

        if($request->has('range_from')) {
            $from_date = date('Y-m-d',strtotime($request->range_from));
            $to_date   = date('Y-m-d',strtotime($request->range_to));
            $leads     = $leads->whereBetween(DB::raw('DATE(generated_at)'), array($from_date, $to_date));
        }

        return view('backend.pages.report.index',[
            'agents'  => $agents,
            'reports' => $leads->get(),
        ]);
    }

}
