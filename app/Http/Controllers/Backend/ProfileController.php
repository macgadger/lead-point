<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Employee;
use App\Models\User;
use App\Models\UserProfile;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    //
    public function show($id)
    {
        //
        $profile = UserProfile::find($id);
        return view('backend.pages.profile.index',[
            'profile' => $profile
        ]);
    }

    public function update(Request $request, $id)
    {
        //
        $profile = UserProfile::find($id);
        if($request->hasFile('user_img'))
        {
            $image           = $request->file('user_img');
            $user_pic        = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/assets/avatars/');
            $request['avatar']   = $user_pic;
            $image->move($destinationPath, $user_pic);
            unlink($destinationPath.$profile->avatar);
        }
        $profile->update($request->all());
        return redirect()->back()->with('success','Your Account information has been updated');
    }


    public function password(Request $request, $id){
        //
        $validator = Validator::make($request->all(),[
            'password'   => 'required|confirmed',
        ]);

        if($validator->fails()){
           return redirect()->back()->with('error',"Password must be matched or check your password");
        }
        else{
            //
            if(Auth::guard('admin')->check()){
                $user = Admin::find($id)->update([
                    'password' => Hash::make($request->password)
                ]);
                Auth::guard('admin')->loginUsingId($id);
            }else{
                $user = Employee::find($id)->update([
                    'password' => Hash::make($request->password)
                ]);
                Auth::guard('employee')->loginUsingId($id);
            }
            return redirect()->back()->with('success','New Password changed successfully');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateEmployeePassword(Request $request, $id)
    {
        //
        $validator = Validator::make($request->all(),[
            'password'   => 'required|confirmed',
        ]);
        if($validator->fails()){
           return redirect()->back()->with('error',"Password must be same or check your password");
        }
        else{
            //
            $user = User::with('permissions')->find($id);
            $user->password   = Hash::make($request->password);
            $user->status     = $request->status;
            $user->syncPermissions(Arr::flatten($request->permissions));
            $user->update();
        }
        return redirect()->route('employee.index')->with('success','Employee profile has been updated');
    }


    public function onlineEmployee(){
        // 
        $employees = UserProfile::companyEmployees()->get();
        return view('backend.pages.online.index',[
            'employees' => $employees
        ]);
    }

    public function accessEmployee($id, $type){
        //
        $user = User::find($id);
        if($type == 'block'){
            $user->update(['status' => 2]);
        }elseif($type == 'unblock'){
            $user->update(['status' => 1]);
        }
        return redirect()->back()->with('status','Operation done Successfully');
    }

}
