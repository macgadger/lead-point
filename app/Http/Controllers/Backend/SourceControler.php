<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Sources;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class SourceControler extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $sources = Sources::all();
        return view('backend.pages.sources.index',compact('sources'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('backend.pages.sources.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'name' => [
                'required',
                function ($attribute, $value, $fail) {
                    if (Sources::where('name',$value)->count() > 0) {
                        $fail($attribute.' is already used.');
                    }
                },
            ]
        ]);

        $source = new Sources;
        $source->name = $request->name;
        $source->slug = Str::of($request->name)->slug('_');
        $source->status = $request->status;
        $source->save();

        return redirect()->route('lead-sources.index')->with('success','Lead Source has been created Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $source = Sources::find($id);
        return view('backend.pages.sources.edit',compact('source'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $source = Sources::find($id);
        $source->name = $request->name;
        $source->slug = Str::of($request->name)->slug('_');
        $source->status = $request->status;
        $source->update();

        return redirect()->route('lead-sources.index')->with('success','Lead Source has been updated Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
