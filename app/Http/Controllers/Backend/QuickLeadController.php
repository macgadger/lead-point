<?php

namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;

use App\Models\QuickLead;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class QuickLeadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->hasRole('admin')){
            $quickLeads = QuickLead::where('ucid',Auth::user()->ucid)
                                        ->orderBy('created_at','DESC')
                                        ->get();
        }else{
            $quickLeads = QuickLead::where('ucid',Auth::user()->ucid)
                                        ->where('created_by',Auth::user()->profile->id)
                                        ->orderBy('created_at','DESC')
                                        ->get();
        }

        return view('backend.pages.quickLeads.index', compact('quickLeads'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.pages.quickLeads.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        QuickLead::create($request->all());

        return back()->with('message', 'item stored successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\QuickLead $quickLead
     * @return \Illuminate\Http\Response
     */
    public function show(QuickLead $quickLead)
    {
        return view('backend.pages.quickLeads.show', compact('quickLead'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\QuickLead $quickLead
     * @return \Illuminate\Http\Response
     */
    public function edit(QuickLead $quickLead)
    {
        return view('backend.pages.quickLeads.edit', compact('quickLead'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\QuickLead $quickLead
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, QuickLead $quickLead)
    {
        $quickLead->update($request->all());

        return back()->with('message', 'item updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\QuickLead $quickLead
     * @return \Illuminate\Http\Response
     */
    public function destroy(QuickLead $quickLead)
    {
        $quickLead->delete();

        return back()->with('message', 'item deleted successfully');
    }
}
