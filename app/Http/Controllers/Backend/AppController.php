<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;


class AppController extends Controller
{
    //
    public function index(){
        //
        return redirect()->route('dashboard');
    }

    public function dashboard(){
        //
        return view('backend.pages.dashboard');
    }

    public function admin(){
        //
        return view('backend.pages.admin');
    }
}
