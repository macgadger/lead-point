<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\LeadRequest;
use App\Models\Leads;
use App\Models\Sources;
use App\Models\UserProfile;
use App\Models\LeadProfile;
use App\Models\QuickLead;
use App\Models\Timeline;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;


class LeadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
    */
    public function __construct()
    {
        //$this->middleware(['permission:assign leads|create leads|delete leads|edit leads|view leads']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $count = Leads::companyLeads()->count();
        return view('backend.pages.leads.index',compact('count'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $agents  =  UserProfile::companyEmployees()->get();
        $sources =  Sources::where('status',1)->get();
        
        return view('backend.pages.leads.new',compact('agents','sources'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LeadRequest $request)
    {
        //
        if($request->validated()){
            // Begin Transaction
            DB::beginTransaction();
            try {
                //
                $lead = new Leads;
                $lead->agent_id      = Auth::user()->profile->id;
                $lead->inquiry_for   = $request->inquiry_for;
                $lead->price_quoted  = $request->price_quoted;
                $lead->duration      = $request->duration;
                $lead->source        = $request->source;
                $lead->next_followup = date('Y-m-d H:i:s', strtotime($request->next_followup));
                $lead->last_followup = date('Y-m-d');
                $lead->tags          = (isset($request->tags)) ? implode(',',$request->tags) : NULL;
                $lead->followup_type = $request->followup_type;
                $lead->notes         = $request->notes;
                $lead->priority      = $request->priority;
                $lead->status        = $request->status;
                $lead->save();
                //
                $profile = new LeadProfile;
                $profile->lead_id   = $lead->id;
                $profile->name      = $request->name;
                $profile->phone     = $request->phone;
                $profile->alt_phone = $request->alt_phone;
                $profile->email     = $request->email;
                $profile->dob       = $request->dob;
                $profile->address   = $request->address;
                $profile->city      = $request->city;
                $profile->state     = $request->state;
                $profile->country   = $request->country;
                $profile->save();
                
                //
                $timeline = new Timeline;
                $timeline->lead_id       = $lead->id;
                $timeline->next_followup = date('Y-m-d H:i:s',strtotime($request->next_followup));
                $timeline->followup_type = $request['followup_type'];
                $timeline->notes         = $request['notes'];
                $timeline->status        = $lead->status;
                $timeline->comment       = "New Lead Created by ".Auth::user()->profile->short_name;
                $timeline->save();
                //
                DB::commit();
            }
            catch(\Exception $e){
                DB::rollback();
                return $e; 
            }
            
            return redirect()->route('leads.index')->with('success','New Lead has been created Successfully!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $lead = Leads::with('timeline')->find($id);
        return view('backend.pages.leads.show',[
            'lead' => $lead
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $lead     = Leads::find($id);
        $agents   = UserProfile::companyEmployees()->get();
        $sources  = Sources::where('status',1)->get();

        return view('backend.pages.leads.edit',[
            'lead'    => $lead,
            'agents'  => $agents,
            'sources' => $sources,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(LeadRequest $request, $id)
    {
        if($request->validated()){
            // Begin Transaction
            DB::beginTransaction();
            try {
                //
                $lead = Leads::find($id);
                // 
                $oldFollowTime = strtotime($lead->next_followup);
                $reqFollowTime = strtotime($request['next_followup']);

                $lead->agent_id      = Auth::user()->profile->id;
                $lead->inquiry_for   = $request->inquiry_for;
                $lead->price_quoted  = $request->price_quoted;
                $lead->duration      = $request->duration;
                $lead->source        = $request->source;
                $lead->next_followup = date('Y-m-d H:i:s', strtotime($request->next_followup));
                $lead->last_followup = ($oldFollowTime !== $reqFollowTime) ? date('Y-m-d') : $lead->last_followup;
                $lead->tags          = (isset($request->tags)) ? implode(',',$request->tags) : $lead->tags;
                $lead->followup_type = $request->followup_type;
                $lead->notes         = $request->notes;
                $lead->priority      = $request->priority;
                $lead->status        = $request->status;
                $lead->converted_at  = ($request->status == 'Converted') ? date('Y-m-d') : $lead->converted_at;
                $lead->update();
                //
                $profile = LeadProfile::find($id);
                $profile->name      = $request->name;
                $profile->phone     = $request->phone;
                $profile->alt_phone = $request->alt_phone;
                $profile->email     = $request->email;
                $profile->dob       = $request->dob;
                $profile->address   = $request->address;
                $profile->city      = $request->city;
                $profile->state     = $request->state;
                $profile->country   = $request->country;
                $profile->update();

                // Update Timeline if any changes in Followup
                if(($oldFollowTime !== $reqFollowTime) || strcmp($lead->followup_type ,$request->followup_type) == 1 || strcmp($lead->notes,$request->notes) == 1){
                    //
                    $timeline = new Timeline;
                    $timeline->lead_id       = $lead->id;
                    $timeline->next_followup = date('Y-m-d H:i:s',strtotime($request->next_followup));
                    $timeline->followup_type = $request['followup_type'];
                    $timeline->notes         = $request['notes'];
                    $timeline->status        = $lead->status;
                    $timeline->comment       = "Lead Followup Updated by ".Auth::user()->profile->short_name;
                    $timeline->save();
                }
                //
                DB::commit();
            }
            catch(\Exception $e){
                DB::rollback();
                dd($e->getMessage()); 
            }

            return redirect()->route('leads.index')->with('success','Lead Updated Successfully!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Add followup to the lead timeline.
     *
     * @param  int  $id
    */
    public function addFollowup(Request $request, $id){
        //
        if($request->isMethod('POST')){
            //
            Leads::find($id)->update([
                "last_followup" => date('Y-m-d'),
                "next_followup" => date('Y-m-d H:i:s',strtotime($request->next_followup)),
                "followup_type" => $request->followup_type,
                "notes"         => $request->notes,
            ]);

            Timeline::create([
                "lead_id"       => $id,
                "next_followup" => date('Y-m-d H:i:s',strtotime($request->next_followup)),
                "followup_type" => $request->followup_type,
                "notes"         => $request->notes,
                "status"        => 'Follow Up',
                "comment"       => "Followup Added by ".Auth::user()->profile->short_name,
            ]);

            return redirect()->route('leads.index')->with('success','Lead Followup Updated Successfully!');
        }
        else{
            $lead = Leads::find($id);
            return view('backend.pages.leads.followup',[
                'lead' => $lead
            ]);
        }

    }

    /**
     * Get followup timeline.
     *
     * @param  int  $id
     */
    public function leadTimeline($id){
        //
        $timeline  = Timeline::where('lead_id',$id)->orderBy('id','desc')->get();
        return view('backend.pages.leads.timeline',[
            'timeline' => $timeline
        ]);
    }

    /**
     * Get and update followup status.
     *
     * @param  int  $id
     */
    public function leadStatus(Request $request, $id){
        //
        if($request->isMethod('POST')){
            //
            $lead = Leads::find($id);
            $lead->update([
                "last_followup" => date('Y-m-d'),
                "status"        => $request->status,
                "notes"         => ($request->notes == null) ? $lead->notes : $request->notes,
                "price_quoted"  => ($request->price_quoted == null) ? $lead->price_quoted : $request->price_quoted,
                "converted_at"  => ($request->status == 2) ? date('Y-m-d') : null,
            ]);
            Timeline::create([
                "lead_id"       => $id,
                "next_followup" => $lead->next_followup,
                "followup_type" => $lead->followup_type,
                "notes"         => ($request->notes == null) ? $lead->notes : $request->notes,
                "status"        => $lead->status,
                "comment"       => "Lead Status Updated by ".Auth::user()->profile->short_name,
            ]);
            
            return redirect()->route('leads.index')->with('success','Lead Status Updated Successfully!');
        }
        else{
            $lead  = Leads::find($id);
            return view('backend.pages.leads.status',[
                'lead' => $lead
            ]);
        }
    }

    /**
     * Add Quick Lead.
     */
    public function leadQuick(Request $request){
        //
        $quick = QuickLead::create($request->all());
        // Timeline::create([
        //     "lead_id"       => $quick->id,
        //     "next_followup" => date('Y-m-d H:i:s'),
        //     "followup_type" => date('Y-m-d H:i:s'),
        //     "notes"         => "Quick Lead",
        //     "status"        => "Quick Lead",
        //     "comment"       => "Quick Lead Added by ".Auth::user()->profile->short_name,
        // ]);
        return redirect()->route('quick-leads.index')->with('success','Quick Lead added Successfully!');
    }

    public function moveQuick($id){
        //
        $lead     = QuickLead::find($id);
        $agents   = UserProfile::companyEmployees()->get();
        $sources  = Sources::where('status',1)->get();

        return view('backend.pages.leads.new',compact('lead','agents','sources'));
    }

    public function sortLeads(Request $request){
        //
        if ($request->ajax()) 
        {
            $query = (new Leads)->newQuery();
            $query = $query->with(['profile','agent'])
                            ->companyLeads();

            if(auth()->user()->hasRole('agent'))
            {
                $query->where('agent_id', Auth::user()->profile->id);
            }

            if($request->has('today')){
                $query = $query->today();
            }

            if($request->has('active')){
                $query = $query->active();
            }

            if($request->has('hot')){
                $query = $query->hot();
            }

            if($request->has('pending')){
                $query = $query->pending();
            }
            
            if($request->has('converted')){
                $query = $query->converted();
            }

            if($request->has('dead')){
                $query = $query->dead();
            }

            if($request->has('all')){
                $query = $query->all();
            }

            $query = $query->select('*')->limit(1);
        
            return DataTables::eloquent($query)
                        ->addIndexColumn()
                        ->editColumn('name', function ($row) {
                            return view('backend.pages.leads.field', ['row' => $row, 'field' => 'name']);
                        })
                        ->editColumn('next_followup', function ($row) {
                            return '<a href="javascript:void(0)">'.$row->next_followup->format('M d, Y h:i A').'</a>';
                        })
                        ->editColumn('last_followup', function ($row) {
                            return Carbon::parse($row->last_followup)->diffForHumans();
                        })
                        ->editColumn('price_quoted', function ($row) {
                            return '<i class="uil uil-rupee-sign"></i>'.$row->price_quoted.'.00';
                        })
                        ->editColumn('agent', function ($row) {                            
                            return view('backend.pages.leads.field', ['row' => $row, 'field' => 'agent']);
                        })
                        ->editColumn('status', function ($row) {
                            return view('backend.pages.leads.field', ['row' => $row, 'field' => 'status']);
                        })
                        ->editColumn('action', function($row){
                            return view('backend.pages.leads.field', ['row' => $row, 'field' => 'actions']);
                        })                        
                        ->rawColumns(['name','next_followup','agent','price_quoted','status','action'])
                        ->setTotalRecords($query->count())
                        ->make(true);
        }
    }

}
