<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Leads;
use Carbon\Carbon;
use Auth;

class FilterController extends Controller
{
    //
    public function filter($type, $role = 'admin'){
        //
        if($role == 'admin'){
            //
            if($type == 'leads'){
                //
                $today = Leads::companyLeads()->where('status',1)->whereDate('created_at', Carbon::today()->format('Y-m-d'))->get();
                $month = Leads::companyLeads()->whereIn('status',[1,2,3,4])->get();
            }
            if($type == 'followup'){
                $today = Leads::companyLeads()->where('status',1)->whereDate('next_followup', Carbon::today()->format('Y-m-d'))->get();
                $month = Leads::companyLeads()->where('status',1)->whereDate('next_followup','<',Carbon::today()->format('Y-m-d'))->get();
            }
    
            if($type == 'converted'){
                $today = Leads::companyLeads()->where('status',2)->whereDate('next_followup', Carbon::today()->format('Y-m-d'))->get();
                $month = Leads::companyLeads()->where('status',2)->whereMonth('next_followup',Carbon::today()->format('n'))->get();
            }
    
            if($type == 'walkin'){
                $today = Leads::companyLeads()->where('status',1)->where('followup_type','walkin')->whereDate('generated_at', Carbon::today()->format('Y-m-d'))->get();
                $month = Leads::companyLeads()->where('status',1)->where('followup_type','walkin')->whereMonth('generated_at',Carbon::today()->format('n'))->get();
            }
    
            if($type == 'payment'){
                $today = Leads::companyLeads()->where('status',2)->whereDate('converted_at', Carbon::today()->format('Y-m-d'))->get();
                $month = Leads::companyLeads()->where('status',2)->whereMonth('converted_at',Carbon::today()->format('n'))->get();
            }
        }else{
            if($type == 'leads'){
                //
                $today = Leads::companyLeads()->where('agent_id',Auth::user()->profile->id)->whereIn('status',[1,2,3])->get();
                $month = Leads::companyLeads()->where('agent_id',Auth::user()->profile->id)->where('status',4)->get();
            }
            if($type == 'followup'){
                $today = Leads::companyLeads()->where('agent_id',Auth::user()->profile->id)->where('status',1)->whereDate('next_followup', Carbon::today()->format('Y-m-d'))->get();
                $month = Leads::companyLeads()->where('agent_id',Auth::user()->profile->id)->where('status',1)->whereDate('next_followup','<',Carbon::today()->format('Y-m-d'))->get();
            }
    
            if($type == 'converted'){
                $today = Leads::companyLeads()->where('agent_id',Auth::user()->profile->id)->where('status',2)->whereDate('converted_at', Carbon::today()->format('Y-m-d'))->get();
                $month = Leads::companyLeads()->where('agent_id',Auth::user()->profile->id)->where('status',2)->whereMonth('converted_at',Carbon::today()->format('n'))->get();
            }
    
            if($type == 'walkin'){
                $today = Leads::companyLeads()->where('agent_id',Auth::user()->profile->id)->where('status',1)->where('followup_type','walkin')->whereDate('generated_at', Carbon::today()->format('Y-m-d'))->get();
                $month = Leads::companyLeads()->where('agent_id',Auth::user()->profile->id)->where('status',1)->where('followup_type','walkin')->whereMonth('generated_at',Carbon::today()->format('n'))->get();
            }
    
            if($type == 'payment'){
                $today = Leads::companyLeads()->where('agent_id',Auth::user()->profile->id)->where('status',2)->whereDate('converted_at', Carbon::today()->format('Y-m-d'))->get();
                $month = Leads::companyLeads()->where('agent_id',Auth::user()->profile->id)->where('status',2)->whereMonth('converted_at',Carbon::today()->format('n'))->get();
            }
            
        }

        return view('backend.pages.filter.index',[
            'today' => $today,
            'month' => $month,
            'type'  => $type,
        ]);
    }
}
