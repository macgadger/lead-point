<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Messages;
use App\Models\UserProfile;
use Auth;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $inbox = Messages::where('sent_to',Auth::user()->profile->id)->orderBy('created_at','DESC')->get();
        $sent  = Messages::where('sent_by',Auth::user()->profile->id)->orderBy('created_at','DESC')->get();
        return view('backend.pages.message.index',compact('inbox','sent'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id = null)
    {
        //
        $agents  =  UserProfile::companyEmployees()->get();
        return view('backend.pages.message.new',compact('agents','id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function send(Request $request)
    {
        //
        $this->validate($request,[
            'agent_id' => 'required',
            'subject'  => 'required',
            'message'  => 'required',
        ]);
        
        $request['sent_by']   = Auth::user()->profile->id;
        $request['sent_to']   = $request->agent_id;
        $message = Messages::create($request->all());
        return redirect()->route('dashboard')->with('success','Message Sent');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $message = Messages::find($id);
        $message->update([
            'is_read' => 1,
        ]);
        return view('pages.message.show',compact('message'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
