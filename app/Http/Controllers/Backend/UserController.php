<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Http\Requests\EmployeeRequest;
use App\Models\User;
use App\Models\UserProfile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use DB;
use Hash;
use Illuminate\Support\Arr;
use Spatie\Permission\Models\Permission;
use Torann\GeoIP\Facades\GeoIP;

class UserController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
    */
    public function __construct()
    {
        $this->middleware(['role:admin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = UserProfile::companyEmployees()->get();
        return view('backend.pages.employee.index',[
            'employees' => $employees
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $employees = UserProfile::companyEmployees()->get();
        if(Auth::user()->profile->plan->users_limit == $employees->count()){
            return redirect()->back()->with('error','Users Limit Exceed. Please upgrade account');
        }
        $permissions = Permission::all();
        return view('backend.pages.employee.new',compact('permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeRequest $request)
    {
        //
        if($request->validated()){
            // Begin Transaction
            DB::beginTransaction();
            try {
                //
                $employee = new User;
                $employee->email      = $request->email;
                $employee->password   = Hash::make($request->password);
                $employee->status     = $request->status;
                $employee->assignRole('agent');
                $employee->givePermissionTo(Arr::flatten($request->permissions));
                $employee->save();

                if($request->hasFile('user_img'))
                {
                    $image           = $request->file('user_img');
                    $user_pic        = time().'.'.$image->getClientOriginalExtension();
                    $destinationPath = public_path('/assets/avatars/');
                    $request['avatar']   = $user_pic;
                    $image->move($destinationPath, $user_pic);
                }

                $request['uid'] = $employee->id;
                $request['plan_id'] = Auth::user()->profile->plan_id;
                UserProfile::create($request->all());
                DB::commit();
            }
            catch(\Exception $e){
                DB::rollback();
                dd($e->getMessage()); 
            }
            return redirect()->route('employee.index')->with('success','New Employee has been created Successfully!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $employee = UserProfile::find($id);
        $permissions = Permission::all();
        return view('backend.pages.employee.edit',[
            'employee' => $employee,
            'permissions' => $permissions,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $profile = UserProfile::find($id);
        if($request->hasFile('user_img'))
        {
            $image           = $request->file('user_img');
            $user_pic        = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/assets/avatars/');
            $request['avatar']   = $user_pic;
            $image->move($destinationPath, $user_pic);
            unlink($destinationPath.$profile->avatar);
        }
        $profile->update($request->all());
        return redirect()->route('employees.index')->with('success','Employee profile has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
