<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LeadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'name'        => 'required',
            'phone'       => 'sometimes|required',//|unique:lead_profiles,phone',
            'address'     => 'required',
            //'dob'         => 'required',
            'city'        => 'required',
            'state'       => 'required',
            'country'     => 'required',
            'agent_id'   => 'sometimes|required',
            'inquiry_for' => 'required',
            'price_quoted'    => 'required',
            'source'          => 'required',
            'next_followup'   => 'required',
            'followup_type'   => 'required',
            'notes'           => 'required',
            'status'          => 'required',
        ];
    }

        /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //
            'agents_id.required' => 'Please select One or more agents to assign lead',
            'phone.unique'       => 'User with this Phone number already exists'
        ];
    }
}
