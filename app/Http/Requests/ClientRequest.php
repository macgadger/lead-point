<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'username'   => 'bail|required|unique:users,email',
            'password'   => 'bail|required|min:6|confirmed',
            'company_name' => 'required',            
            'first_name' => 'required',
            'last_name'  => 'required',
            'phone'      => 'required|min:10',
            'address'    => 'required',
            'city'       => 'required',
            'state'      => 'required',
            'country'    => 'required',
            'status'     => 'required',
            'plan_id'    => 'required',
            'plan_duration' => 'required',
        ];
    }
}
