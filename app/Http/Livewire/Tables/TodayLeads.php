<?php

namespace App\Http\Livewire\Tables;

use App\Models\LeadProfile;
use App\Models\Leads as ModelsLeads;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class TodayLeads extends Component
{
    public function render()
    {
        $leads =    ModelsLeads::where('ucid', Auth::user()->ucid)
                                ->where('agent_id',Auth::user()->profile->id)
                                ->whereDate('next_followup',Carbon::today()->format('Y-m-d'))
                                ->where('status',1)
                                ->get();
        
        $reminders  = LeadProfile::where('ucid', Auth::user()->ucid)
                                ->where('created_by',Auth::user()->profile->id)
                                ->whereDay('dob',Carbon::today()->format('d'))
                                ->whereMonth('dob',Carbon::today()->format('m'))
                                ->select('name','dob')
                                ->get();

        return view('livewire.tables.today-leads',compact('leads','reminders'));
    }
}
