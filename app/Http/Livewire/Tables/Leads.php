<?php

namespace App\Http\Livewire\Tables;

use App\Models\Leads as ModelsLeads;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Leads extends Component
{
    public $leads;
    protected $listeners = ['filterLeads'];

    public function mount()
    {
        $this->active();
    }

    public function active()
    {
        $this->tab = 'active';
        $query = (new ModelsLeads)->newQuery();

        if(Auth::user()->hasRole('agent'))
            $query->where('agent_id', Auth::user()->profile->id);

        $this->leads = $query->where('ucid', Auth::user()->ucid)
                            ->where('status',1)
                            ->orderBy('created_by','DESC')
                            //->take(10)
                            ->get();

        $this->dispatchBrowserEvent('filterLeads', []);
    }

    public function hot()
    {
        $this->tab = 'hot';
        $query = (new ModelsLeads)->newQuery();

        if(Auth::user()->hasRole('agent'))
            $query->where('agent_id', Auth::user()->profile->id);

        $this->leads = $query->where('ucid', Auth::user()->ucid)
                            ->where('priority',1)
                            ->orderBy('created_by','DESC')
                            ->get();

        $this->dispatchBrowserEvent('filterLeads', []);
    }

    public function pending()
    {
        $this->tab = 'pending';
        $query = (new ModelsLeads)->newQuery();

        if(Auth::user()->hasRole('agent'))
            $query->where('agent_id', Auth::user()->profile->id);

        $this->leads = $query->where('ucid', Auth::user()->ucid)
                            ->whereDate('next_followup','<', Carbon::today()->format('Y-m-d'))
                            ->where('status',1)
                            ->orderBy('next_followup','DESC')
                            ->get();

        $this->dispatchBrowserEvent('filterLeads', []);
    }

    public function converted()
    {
        $query = (new ModelsLeads)->newQuery();

        if(Auth::user()->hasRole('agent'))
            $query->where('agent_id', Auth::user()->profile->id);

        $this->leads = $query->where('ucid', Auth::user()->ucid)
                            ->where('status',2)->get();

        $this->dispatchBrowserEvent('filterLeads', []);
    }

    public function dead()
    {
        $query = (new ModelsLeads)->newQuery();

        if(Auth::user()->hasRole('agent'))
            $query->where('agent_id', Auth::user()->profile->id);

        $this->leads = $query->where('ucid', Auth::user()->ucid)
                            ->where('status',4)->get();
                            
        $this->dispatchBrowserEvent('filterLeads', []);
    }
    
    public function render()
    {
        return view('livewire.tables.leads');
    }
}