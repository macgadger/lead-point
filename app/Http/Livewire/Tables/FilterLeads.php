<?php

namespace App\Http\Livewire\Tables;

use Livewire\Component;
use Illuminate\Support\Facades\Auth;
use App\Models\Leads;
use Carbon\Carbon;

class FilterLeads extends Component
{
    public $leads;
    protected $listeners = ['filterLeads'];

    public function mount()
    {
        $this->all();
    }

    public function all()
    {
        $this->tab = 'all';
        $query = (new Leads)->newQuery();

        $this->leads = $query->where('ucid', Auth::user()->ucid)
                            ->where('status',1)
                            ->orderBy('created_by','DESC')
                            //->take(10)
                            ->get();

        $this->dispatchBrowserEvent('filterLeads', ['status' => true]);
    }
    
    public function render()
    {
        return view('livewire.tables.filter-leads');
    }
}
