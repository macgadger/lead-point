<?php

namespace App\Http\Livewire\Analytics;

use App\Models\Sources as ModelsSources;
use Livewire\Component;

class Sources extends Component
{
    public $sources;

    public function refresh()
    {
        $this->sources = ModelsSources::where('status',1)->get();
    }

    public function render()
    {
        $this->refresh();
        return view('livewire.analytics.sources');
    }
}
