<?php

namespace App\Http\Livewire\Analytics;

use App\Models\Timeline;
use Carbon\Carbon;
use Livewire\Component;

class Activity extends Component
{
    public $activities;

    public function reload()
    {
        $this->activities =  Timeline::with('lead')
        //                         //->where('ucid', Auth::user()->ucid)
                                ->whereDate('created_at', Carbon::today()->toDateString())
                                ->orderBy('id','DESC')
                                ->get();

    }

    public function render()
    {
        $this->reload();
        return view('livewire.analytics.activity');
    }
}
