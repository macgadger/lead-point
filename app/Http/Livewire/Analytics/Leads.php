<?php

namespace App\Http\Livewire\Analytics;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class Leads extends Component
{
    public $leads;

    public function refresh()
    {
        $today = Carbon::today();
        $query = DB::table('leads');

        if(Auth::user()->hasRole('agent'))
            $query->where('agent_id', Auth::user()->profile->id);

        $query->where('ucid', Auth::user()->ucid)
                ->selectRaw("count(case when status IN(1,2,3,4) then 1 end) as total")
                ->selectRaw("count(case when status = 1 and date(generated_at) = ? then 1 end) as today",[$today->format('Y-m-d')]);
                //->selectRaw("count(case when status = 4 then 1 end) as today");
                
        $this->leads = $query->get()->toArray();
    }

    public function render()
    {
        $this->refresh();
        return view('livewire.analytics.leads');
    }
}
