<?php

namespace App\Http\Livewire\Analytics;

use App\Models\LeadProfile;
use App\Models\Leads;
use App\Models\UserProfile;
use Livewire\Component;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;

class Summary extends Component
{
    public $summary;
    public $reminders; 

    public function reload()
    {
        $today         = Carbon::today();
        $agents        = UserProfile::where('ucid', Auth::user()->ucid)
                                    ->where('uid','!=', Auth::user()->id)
                                    ->pluck('id');

        $this->summary = Leads::where('ucid', Auth::user()->ucid)
                                ->whereIn('agent_id',$agents)
                                ->select('agent_id')
                                ->selectRaw('count(*) as total')
                                ->selectRaw("count(case when status IN(1,2,3,4) and date(generated_at)  = ? then 1 end) as today",[$today->format('Y-m-d')])
                                ->selectRaw("count(case when status IN(1,2,3,4) and month(generated_at) = ? then 1 end) as month",[$today->format('n')])
                                ->selectRaw("count(case when priority = 1 then 1 end) as hot")
                                ->groupBy('agent_id')
                                ->get();

        $this->reminders['leads']  = LeadProfile::where('ucid', Auth::user()->ucid)
                                        ->whereDay('dob',$today->format('d'))
                                        ->whereMonth('dob',$today->format('m'))
                                        ->select('name','dob')
                                        ->get();

        $this->reminders['agents'] = UserProfile::where('ucid', Auth::user()->ucid)
                                        ->whereDay('dob',$today->format('d'))
                                        ->whereMonth('dob',$today->format('m'))
                                        ->select('first_name','last_name','dob')
                                        ->get();

    }
    
    public function render()
    {
        $this->reload();
        return view('livewire.analytics.summary');
    }
}
