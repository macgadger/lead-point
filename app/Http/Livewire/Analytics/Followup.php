<?php

namespace App\Http\Livewire\Analytics;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class Followup extends Component
{
    public $followup;

    public function refresh()
    {
        $today = Carbon::today();

        $query = DB::table('leads');

        if(Auth::user()->hasRole('agent'))
            $query->where('agent_id', Auth::user()->profile->id);

        $query->where('ucid', Auth::user()->ucid)
            ->where('status', 1)
            ->selectRaw("count(case when date(next_followup) = ? then 1 end) as today",[$today->format('Y-m-d')])
            ->selectRaw("count(case when date(next_followup) < ? then 1 end) as pending" ,[$today->format('Y-m-d')]);

        $this->followup = $query->get()->toArray();
    }

    public function render()
    {
        $this->refresh();
        return view('livewire.analytics.followup');
    }
}
