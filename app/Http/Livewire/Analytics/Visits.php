<?php

namespace App\Http\Livewire\Analytics;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class Visits extends Component
{
    public $visits;

    public function refresh()
    {
        $today = Carbon::today();
        $query = DB::table('leads');

        if(Auth::user()->hasRole('agent'))
            $query->where('agent_id', Auth::user()->profile->id);

        $query->where('ucid', Auth::user()->ucid)
                ->where('status', 1)
                // ->selectRaw('count(*) as total')
                ->selectRaw("count(case when followup_type = ? and date(generated_at)  = ? then 0 end) as today",['walkin',$today->format('Y-m-d')])
                 ->selectRaw("count(case when followup_type = ? and month(generated_at) = ? then 0 end) as month",['walkin',$today->format('n')]);

        $this->visits = $query->get()->toArray();
    }

    public function render()
    {
        $this->refresh();
        return view('livewire.analytics.visits');
    }
}
