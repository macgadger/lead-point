<?php

namespace App\Http\Livewire\Analytics;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class Payments extends Component
{
    public $payments;

    public function refresh()
    {
        $today = Carbon::today();
        $this->payments = DB::table('leads')
                                //->where('ucid', Auth::user()->ucid)
                                ->where('status',2)
                                ->selectRaw("sum(case when date(converted_at)  = ? then price_quoted else 0 end) as today",[$today->format('Y-m-d')])
                                ->selectRaw("sum(case when month(converted_at) = ? then price_quoted else 0 end) as month",[$today->format('n')])
                                ->get()
                                ->toArray();
    }

    public function render()
    {
        $this->refresh();
        return view('livewire.analytics.payments');
    }
}
