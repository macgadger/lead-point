<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class UserAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(Auth::check() && Auth::user()->status != 1 ){
            Cache::forget('user-is-online-'.Auth::user()->id);
            Auth::logout();
            return redirect()->route('account.login')->with('error','User Acccess Blocked. Please contact administrator');
        }
        return $next($request);
    }
}
