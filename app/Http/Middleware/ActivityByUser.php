<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Auth;
use Cache;
use Carbon\Carbon;

class ActivityByUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (Auth::check()) {
            //
            Cache::put('user-is-online-'.Auth::user()->id, true, now()->addMinutes(1));
            // last seen and user ip
            User::where('id', Auth::user()->id)->update([
                'last_seen' => (new \DateTime())->format("Y-m-d H:i:s"),
                'last_login_ip' => $request->ip(),
            ]);
        }
        return $next($request);
    }
}
