<?php
namespace App\TableModels;

use SingleQuote\DataTables\Controllers\ColumnBuilder;
use SingleQuote\DataTables\Fields\Button;
use SingleQuote\DataTables\Fields\Date;
/**
 * TableModel for SingleQuote\DataTable
 * See the full focs here for the table models https://singlequote.github.io/Laravel-datatables/table-models
 *
 */
class LeadsTable extends ColumnBuilder
{
    // @var array
    public $tableClass = "table table-centered table-hover responsize table-card-list";
    public $tableHeaderClass = "";

    /**
     * The default pagelength
     *
     * @var int
     */
    public $pageLength = 10;
        
    /**
     * Remember the data page of the user
     *
     * @var bool
     */
    public $rememberPage = true;

    /**
     * When set to true.
     * The package will auto reload the content of the current table page
     * @var bool
     */
    public $autoReload = false;

    /**
     *  
     * The package will set auto reload time in seconds
     * @var int
     */
    public $reloadTime = 10;
    
    /**
     *  
     * The package will set dom for datatable how to show elements
     * @var string
    */
    // public $dom = "<'row'<'col-md-4'B><'col-md-6'f><'col-md-2'l>><'row'<'col-md-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>";

    /**
     *  
     * The package will set which button to show on datatable
     * @var array
    */
    // public $buttons = [
    //     'copyHtml5',
    //     'pdf',
    //     //'print',
    //     //'csvHtml5',
    //     //'colvis',
    // ];

    /**
     *  
     * The package will set which processing content show on datatable
     * @var array
    */
    public $processing = "<i class='uil uil-sync uil-spin'></i>";

    /**
     * Set the columns for your table
     * These are also your header, searchable and filter columns
     *
     * @var array
     */
    public $columns = [
        [
            'name' => 'stage',
            'data' => 'status',
            "searchable" => false,
            "orderable"  => true,
            "columnSearch" => false,
        ],
        'profile.name',
        'inquiry_for',
        'next_followup',
        'last_followup',
        'agent.first_name',
        'price_quoted',
        [
            'data' => 'id',
            'name' => 'action',
            "searchable" => false,  //set the column to be searchable
            "orderable" => false,    //set the column to be orderable
            "columnSearch" => false, //this generates a search input below the column header
        ],
    ];

    /**
    * Set the translation columns for the headers
    *
    * @return array
    */
    public function translate() : array
    {
        return [
            'stage' => __('Stage'),
            'profile.name' => __('Name'),
            'inquiry_for'  => __('Inquiry For'),
            'next_followup' => __('Next Followup'),
            'last_followup' => __('Last Followup'),
            'agent.first_name'    => __('Agent'),
            'price_quoted'  => __('Price Quoted'),
            'action' => __('Actions'),
        ];
    }

    /**
     * Alter the fields displayed by the resource.
     *
     * @return array
     */
    public function fields() : array
    {
        return [
            Button::make('stage')->class('btn btn-primary btn-xs')->label('Status'),
            Date::make('next_followup')->format('d M, Y'),
            Date::make('last_followup')->format('d M, Y'),
            Button::make('action')->class('btn btn-outline-primary btn-sm')->icon("uil uil-eye")->route('leads.show', 'id'),
            Button::make('action')->class('btn btn-outline-warning btn-sm')->icon("uil uil-edit")->route('leads.edit', 'id'),
            Button::make('action')->class('btn btn-outline-danger btn-sm')->icon("uil uil-trash")->method('delete')->route('leads.destroy', 'id'),
        ];
    }

    /**
    * Run an elequent query
    *
    * @param \Illuminate\Database\Query\Builder $query
    * @return \Illuminate\Database\Query\Builder
    */
    public function query($query)
    {
        //return $query->whereNotNull('id');
        return $query->with(['profile','agent']);
    }
    

}