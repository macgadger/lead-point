<?php
namespace App\TableModels;

use Carbon\Carbon;
use SingleQuote\DataTables\Controllers\ColumnBuilder;
use SingleQuote\DataTables\Fields\Button;
use SingleQuote\DataTables\Fields\Date;
use SingleQuote\DataTables\Filters\Dropdown;

/**
 * TableModel for SingleQuote\DataTable
 * See the full focs here for the table models https://singlequote.github.io/Laravel-datatables/table-models
 *
 */
class UsersTable extends ColumnBuilder
{
    
    // @var array
    public $tableClass = "table table-centered table-hover responsize table-card-list";
    public $tableHeaderClass = "";

    /**
     * The default pagelength
     *
     * @var int
     */
    public $pageLength = 10;
        
    /**
     * Remember the data page of the user
     *
     * @var bool
     */
    public $rememberPage = true;

    /**
     * When set to true.
     * The package will auto reload the content of the current table page
     * @var bool
     */
    public $autoReload = false;

    /**
     *  
     * The package will set auto reload time in seconds
     * @var int
     */
    public $reloadTime = 10;
    
    /**
     *  
     * The package will set dom for datatable how to show elements
     * @var string
    */
    // public $dom = "<'row'<'col-md-4'B><'col-md-6'f><'col-md-2'l>><'row'<'col-md-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>";

    /**
     *  
     * The package will set which button to show on datatable
     * @var array
    */
    // public $buttons = [
    //     'copyHtml5',
    //     'pdf',
    //     //'print',
    //     //'csvHtml5',
    //     //'colvis',
    // ];

    /**
     *  
     * The package will set which processing content show on datatable
     * @var array
    */
    public $processing = "<i class='uil uil-sync uil-spin'></i>";


    /**
     * Set the columns for your table
     * These are also your header, searchable and filter columns
     *
     * @var array
     */
    public $columns = [
        // [
        //     'name' => "name",
        //     "columnSearch" => true,
        //     "data" => "name"
        // ],
        'email',
        'created_at',
        [
            'data' => 'id',
            'name' => 'action',
            "searchable" => false,  //set the column to be searchable
            "orderable" => false,    //set the column to be orderable
            "columnSearch" => false, //this generates a search input below the column header
        ],
    ];

    
   /**
    * Set the translation columns for the headers
    *
    * @return array
    */
    public function translate() : array
    {
        return [
            //'name' => __('Name'),
            'email' => __('Email'),
            'created_at' => __('Created At'),
            'action' => __('Actions'),
        ];
    }
            
    
   /**
    * Run an elequent query
    *
    * @param \Illuminate\Database\Query\Builder $query
    * @return \Illuminate\Database\Query\Builder
    */
    public function query($query)
    {
        return $query->whereNotNull('id');
        // if ($this->getFilter('filter') && (int) $this->getFilter('filter') === 2) {
        //     return $query->where('status',0);
        // }
        
        // if ($this->getFilter('bydate') && (int) $this->getFilter('bydate') === 2) {
        //     $today = Carbon::today();
        //     return $query->whereDate('created_at',$today->format('Y-m-d'));
        // }

        // return $query;
    }
            

    /**
     * Alter the fields displayed by the resource.
     *
     * @return array
     */
    public function fields() : array
    {
        return [
            Date::make('created_at')->format('d M, Y'),
            Button::make('action')->class('btn btn-outline-info btn-xs modal-link')
                                    ->icon("uil uil-plus")
                                    ->data([
                                      'id'  => 'id',
                                      //'title' => 'Add Followup',
                                      'placement' => 'bottom',
                                    ])
                                    //->uri('users.edit','id')
                                    ->title('Add Followup')
        ];
    }

    /**
     * Create filters
     *
     * @return array
     */
    // public function filter() : array
    // {
    //     $filter = [
    //         [
    //             'name' => "Active", 
    //             "id" => 1
    //         ],[
    //             'name' => "Suspended", 
    //             "id" => 2
    //         ]
    //     ];

    //     $bydate = [
    //         [
    //             'name' => "All", 
    //             "id" => 1
    //         ],[
    //             'name' => "Today", 
    //             "id" => 2
    //         ]
    //     ];
        
    //     return [
	//         //the data attribute accepts an array as an object|collection
    //         //Label::make('filter'),
    //         Dropdown::make('filter')->label("User Status")->class('form-control')->data($filter),
    //         Dropdown::make('bydate')->label("Sort By Date")->class('form-control')->data($bydate),
    //     ];
    // }

}