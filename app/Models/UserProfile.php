<?php

namespace App\Models;

use Auth;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    use HasFactory;
    protected $table = 'profiles';

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'dob' => 'date',
        
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uid',
        'ucid',
        'plan_id',
        'plan_duration',
        'plan_expiry',
        'email',
        'avatar',
        'display_name',
        'company_name',
        'company_logo',
        'first_name',
        'last_name' ,
        'phone',
        'website',
        'address',
        'city',
        'state',
        'country',
        'status',        
    ];

    /**
     *  Setup model event hooks
    */
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            //
            if(Auth::guard('web')->check()){
                $model->ucid = Auth::user()->ucid;
            }
        });
    }

    public function getFullAddressAttribute(){
        return ucwords("{$this->address}, {$this->city}, {$this->state} - {$this->country}");
    }

    public function getFullNameAttribute(){
        return ucwords("{$this->first_name} {$this->last_name}");
    }

    public function getAssetAvatarAttribute(){
        return asset('assets/avatars').'/'.$this->avatar;
    }

    public function getAssetLogoAttribute(){
        return asset('assets/logos').'/'.$this->company_logo;
    }

    public function getShortNameAttribute(){
        $last = substr($this->last_name,0, 1);
        return ucwords("{$this->first_name} {$last}.");
    }
    
    public function scopeCompanyEmployees($query){
        return $query->where('ucid', Auth::user()->ucid);//->whereNotIn('uid',[Auth::user()->id]);
    }

    public function user(){
        return $this->hasOne('App\Models\User','id','uid');
    }

    public function plan(){
        return $this->hasOne('App\Models\Plans','id','plan_id');
    }

}
