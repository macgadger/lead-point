<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrialRequest extends Model
{
    use HasFactory;
    protected $table = 'trial_request';

    public $fillable = [
        'name','email','phone',
    ];

}
