<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class QuickLead extends Model
{
    use HasFactory;
    protected $table = 'quick_leads';

    public $fillable = [
        'name','phone','email','inquiry_for','price_quoted'
    ];

    /**
     *  Setup model event hooks
    */
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            //
            $model->ucid = Auth::user()->ucid;
            $model->created_by = Auth::user()->profile->id;
        });
    }

    public function createdBy(){
        return $this->hasOne('App\Models\UserProfile','id','created_by');
    }
    
}
