<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Timeline extends Model
{
    use HasFactory;
    public $table = "timeline";

    public $fillable = [
        'lead_id','next_followup','followup_type','notes','status','comment'
    ];

    /**
     *  Setup model event hooks
     */
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            //
            $model->created_by = Auth::user()->profile->id;
            $model->ucid       = Auth::user()->ucid;
        });
    }

    public function lead(){
        //
        return $this->hasOne('App\Models\Leads','id','lead_id');
    }

    public function createdBy(){
        //
        return $this->hasOne('App\Models\UserProfile','id','created_by');
    }
    
}
