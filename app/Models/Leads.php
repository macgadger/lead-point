<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Leads extends Model
{
    use HasFactory;

    public $fillable = [
        'inquiry_for','duration','price_quoted','source','last_followup',
        'next_followup','followup_type','notes',
        'assign_to','status','created_by',
        'converted_at','generated_at',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'next_followup' => 'datetime:Y-m-d H:i',
        'dob' => 'date',
        'generated_at' => 'date:Y-m-d',
        'converted_at' => 'date:Y-m-d',
    ];

    protected $statuses = array(
        '1' => 'Follow Up',
        '2' => 'Converted',
        '3' => 'Delay',
        '4' => 'Dead',
    );

    protected $priorities = array(
        '1' => 'hot',
        '2' => 'warm',
        '3' => 'cold',
    );

    /**
     *  Setup model event hooks
     */
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            //
            $model->ucid         = Auth::user()->ucid;
            $model->generated_at = date('Y-m-d');
            $model->created_by   = Auth::user()->profile->id;
        });        
    }

    public function profile(){
        return $this->hasOne('App\Models\LeadProfile','lead_id','id');
    }

    public function agent(){
        return $this->hasOne('App\Models\UserProfile','id','agent_id');
    }

    public function sources(){
        return $this->hasOne('App\Models\Sources','id','source');
    }

    public function getStatusAttribute($value){
        return $this->statuses[$value];
    }

    public function getStatusTitleAttribute(){
        return $this->statuses[$this->status];
    }

    public function getPriorityAttribute($value){
        return $this->priorities[$value];
    }
    
    public function scopeCompanyLeads($query){
        return $query->where('ucid', Auth::user()->ucid);
    }

    public function scopeToday($query){
        return $query->whereDate('next_followup',Carbon::today()->format('Y-m-d'));
    }    

    public function scopeActive($query){
        return $query->where('status',1);
    }

    public function scopeHot($query){
        return $query->where('priority',1);
    }

    public function scopePending($query){
        return $query->whereDate('next_followup','<', Carbon::today()->format('Y-m-d'))->where('status',1);
    }

    public function scopeConverted($query){
        return $query->where('status',2);
    }

    public function scopeDead($query){
        return $query->where('status', 4);
    }

    public function scopeAll($query){
        return $query->whereIn('status',[1,2,3,4]);
    }

    public function timeline(){
        return $this->hasMany('App\Models\Timeline','lead_id','id')->orderBy('id','desc');
    }

    public function createdBy(){
        return $this->hasOne('App\Models\UserProfile','id','created_by');
    }

}
