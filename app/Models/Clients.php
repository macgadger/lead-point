<?php

namespace App\Models;

use Auth;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Clients extends Model
{
    use HasFactory;

    protected $fillable = [
        'name' ,
        'phone',
        'alt_phone',
        'company_name',
        'dob',
        'email',
        'address',
        'city',
        'state',
        'country',
        'status',
    ];

    /**
     *  Setup model event hooks
    */
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            //
            $model->ucid = Auth::user()->ucid;
        });
    }

    public function scopeCompanyClients($query){
        //
        return $query->where('ucid', Auth::user()->ucid);
    }

}