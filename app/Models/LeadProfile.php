<?php

namespace App\Models;

use Auth;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LeadProfile extends Model
{
    use HasFactory;
    
    protected  $primaryKey = 'lead_id';

    public $fillable = [
        'name','phone','alt_phone','email','dob','address','city','state','country','tags'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'dob' => 'date',
        
    ];

    /**
     *  Setup model event hooks
     */
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            //
            $model->ucid         = Auth::user()->ucid;
            $model->created_by   = Auth::user()->id;
        });
    }

    public function getFullAddressAttribute(){
        return ucwords("{$this->address}, {$this->city}, {$this->state} - {$this->country}");
    }

    public function getFullNameAttribute($value)
    {
        return ucwords("{$this->name}");
    }
    
}
