<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Messages extends Model
{
    use HasFactory;

    public $fillable = [
        'sent_by','sent_to','subject','message', 'is_read'
    ];

    // protected $casts = [
    //     'next_followup' => 'datetime:Y-m-d H:i',
    //     'dob' => 'date',
    //     'generated_at' => 'date:Y-m-d',
    //     'converted_at' => 'date:Y-m-d',
    // ];

    public function sentby(){
        return $this->hasOne('App\Models\UserProfile','id','sent_by');
    }

    public function sentto(){
        return $this->hasOne('App\Models\UserProfile','id','sent_to');
    }

}
