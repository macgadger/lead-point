-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 23, 2021 at 03:57 PM
-- Server version: 8.0.23-0ubuntu0.20.04.1
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `curvet`
--

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` int NOT NULL,
  `ucid` varchar(255) NOT NULL,
  `company` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `alt_phone` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `address` text NOT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `country` varchar(100) NOT NULL,
  `status` int NOT NULL DEFAULT '1',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `ucid`, `company`, `name`, `phone`, `alt_phone`, `email`, `dob`, `address`, `city`, `state`, `country`, `status`, `created_at`) VALUES
(11, 'abc-123', NULL, 'deepika', '769202218', NULL, 'deepikamca@yahoo.com', NULL, 'vip road', 'zirakpur', 'punjab', 'india', 1, '2021-03-15 07:17:48'),
(12, 'abc-123', NULL, 'manpreet singh', '9872541365', NULL, 'macgadger@gmail.com', NULL, 'vip road', 'zirakpur', 'punjab', 'india', 1, '2021-03-15 07:17:48'),
(13, 'abc-123', NULL, 'balvir chahal', '9875463254', NULL, 'info@a2itsoft.com', '1991-03-27', '#34 SAS Nagar', 'Mohali', 'Punjab', 'India', 1, '2021-03-22 11:35:06');

-- --------------------------------------------------------

--
-- Table structure for table `leads`
--

CREATE TABLE `leads` (
  `id` int NOT NULL,
  `ucid` varchar(255) NOT NULL,
  `agent_id` int NOT NULL,
  `inquiry_for` text NOT NULL,
  `price_quoted` int NOT NULL,
  `duration` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `source` int NOT NULL,
  `next_followup` timestamp NULL DEFAULT NULL,
  `last_followup` date NOT NULL,
  `followup_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `tags` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `priority` int NOT NULL DEFAULT '2',
  `notes` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `status` int NOT NULL,
  `created_by` int NOT NULL,
  `generated_at` date DEFAULT NULL,
  `converted_at` date DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `leads`
--

INSERT INTO `leads` (`id`, `ucid`, `agent_id`, `inquiry_for`, `price_quoted`, `duration`, `source`, `next_followup`, `last_followup`, `followup_type`, `tags`, `priority`, `notes`, `status`, `created_by`, `generated_at`, `converted_at`, `created_at`) VALUES
(1, 'A2IT-175E-8136', 2, 'web development', 12000, '6 months', 6, '2021-02-26 17:51:00', '2021-03-01', 'call', NULL, 2, 'joined somewhere else', 4, 2, '2021-02-25', NULL, '2021-02-25 17:53:22'),
(2, 'A2IT-175E-8136', 2, 'web development', 15000, '6 months', 1, '2021-03-09 11:30:00', '2021-03-09', 'call', NULL, 2, 'joined in jammu..', 4, 2, '2021-02-25', NULL, '2021-02-25 17:57:54'),
(5, 'A2IT-175E-8136', 2, 'PHP Laravel', 15000, NULL, 1, '2021-02-28 10:00:00', '2021-03-01', 'walkin', NULL, 2, 'fake', 4, 2, '2021-02-27', NULL, '2021-02-27 11:57:40'),
(6, 'A2IT-175E-8136', 3, 'Manual Testing', 12000, '3 months', 1, '2021-03-04 11:36:00', '2021-03-03', 'call', NULL, 2, 'Not interested for training now. Internship in Digital marketing', 4, 3, '2021-03-01', NULL, '2021-03-01 11:36:41'),
(7, 'A2IT-175E-8136', 2, 'web technology', 18000, '6 months', 4, '2021-03-04 15:42:00', '2021-03-04', 'meeting', NULL, 2, 'want stipend internship in python', 4, 2, '2021-03-01', NULL, '2021-03-01 12:01:40'),
(8, 'A2IT-175E-8136', 2, 'aUTOCAD', 4500, '6 WEEKS', 4, '2021-03-22 11:57:00', '2021-03-09', 'whatsapp', NULL, 2, 'college classes starts from 25 march, so Training postponed to may or june.. in between send random details.', 1, 2, '2021-03-01', NULL, '2021-03-01 14:14:13'),
(9, 'A2IT-175E-8136', 3, 'java Cert.', 4000, '6 month', 1, '2021-03-27 17:37:00', '2021-03-12', 'call', NULL, 2, 'call not picked', 1, 3, '2021-03-01', NULL, '2021-03-01 15:40:11'),
(10, 'A2IT-175E-8136', 3, 'Cyber Security', 12000, '3M', 3, '2021-03-13 09:31:00', '2021-03-15', 'walkin', NULL, 2, 'Delay for some reason. He was ready for the payment but don\'t know why he is delaying now', 3, 3, '2021-03-01', NULL, '2021-03-01 16:09:31'),
(11, 'A2IT-175E-8136', 3, 'CCNA', 10500, '2M', 1, '2021-03-05 17:48:00', '2021-03-05', 'call', NULL, 2, 'Registered ccna@10500. 1 month course', 2, 3, '2021-03-01', '2021-03-05', '2021-03-01 16:30:55'),
(12, 'A2IT-175E-8136', 3, 'Advance java, Android', 18000, '6M', 1, '2021-03-22 17:07:00', '2021-03-15', 'walkin', NULL, 2, 'Will start classes for android soon. Waiting for his group of friends', 1, 3, '2021-03-01', NULL, '2021-03-01 16:38:34'),
(13, 'A2IT-175E-8136', 2, 'web designing', 12000, '3 months', 6, '2021-03-10 10:31:00', '2021-03-10', 'call', NULL, 2, 'call back in may', 3, 2, '2021-03-01', NULL, '2021-03-01 17:59:33'),
(14, 'A2IT-175E-8136', 2, 'networking& Ethical Hacking', 18000, '3 months', 6, '2021-03-24 17:29:00', '2021-03-17', 'walkin', NULL, 2, 'followups done by gurjeet mam', 1, 2, '2021-03-01', NULL, '2021-03-01 18:12:48'),
(15, 'A2IT-175E-8136', 3, 'CCNA', 10800, '1 Month', 1, '2021-03-03 17:51:00', '2021-03-03', 'call', NULL, 2, 'Registration done CCNA @10800 one time payment', 2, 3, '2021-03-02', '2021-03-03', '2021-03-02 10:30:05'),
(16, 'A2IT-175E-8136', 3, 'S/w Industry Certificate', 5000, '6M', 1, '2021-03-25 15:26:00', '2021-03-17', 'call', NULL, 2, 'Will think about this', 1, 3, '2021-03-02', NULL, '2021-03-02 11:50:54'),
(17, 'A2IT-175E-8136', 3, 'Autocad / CE', 6000, '2 months', 1, '2021-07-02 09:32:00', '2021-03-15', 'call', NULL, 2, 'Final semester classes going on. Will do training after sem in July or august', 1, 3, '2021-03-03', NULL, '2021-03-03 12:31:12'),
(18, 'A2IT-175E-8136', 3, 'Python + data Science', 8000, '6 month', 1, '2021-03-03 15:42:00', '2021-03-03', 'call', NULL, 2, 'Registered python@8000. will continue in DS @15000', 2, 3, '2021-03-03', '2021-03-03', '2021-03-03 15:43:15'),
(19, 'A2IT-175E-8136', 3, 'Python + data science', 8000, '6 month', 1, '2021-03-03 15:46:00', '2021-03-03', 'call', NULL, 2, 'registered python@8000. will continue in DS@15000', 2, 3, '2021-03-03', '2021-03-03', '2021-03-03 15:46:56'),
(20, 'A2IT-175E-8136', 3, 'Python + DS', 8000, '6 month', 1, '2021-03-03 15:48:00', '2021-03-03', 'call', NULL, 2, 'registered python@8000. will continue in DS@15000', 2, 3, '2021-03-03', '2021-03-03', '2021-03-03 15:48:39'),
(21, 'A2IT-175E-8136', 3, 'Core java', 10000, '2months', 1, '2021-03-12 10:20:00', '2021-03-12', 'walkin', NULL, 2, 'Joined somewhere else', 4, 3, '2021-03-03', NULL, '2021-03-03 15:55:52'),
(22, 'A2IT-175E-8136', 3, 'core java', 10000, '2 months', 1, '2021-03-12 10:20:00', '2021-03-12', 'walkin', NULL, 2, 'Joined somewhere else', 4, 3, '2021-03-03', NULL, '2021-03-03 15:57:31'),
(23, 'A2IT-175E-8136', 3, 'Python', 12000, '3months', 1, '2021-03-26 12:20:00', '2021-03-18', 'call', NULL, 2, 'switch off', 1, 3, '2021-03-03', NULL, '2021-03-03 16:51:46'),
(24, 'A2IT-175E-8136', 3, 'PHP', 18000, '6 month', 6, '2021-03-04 17:46:00', '2021-03-04', 'call', NULL, 2, 'Looking for Internship only because 6 month training done in BCA from solitaire infosys', 4, 3, '2021-03-03', NULL, '2021-03-03 17:47:00'),
(25, 'A2IT-175E-8136', 3, 'CCNA', 10000, '2 months', 1, '2021-03-26 09:09:00', '2021-03-11', 'call', NULL, 2, 'Not sure', 1, 3, '2021-03-04', NULL, '2021-03-04 11:55:01'),
(26, 'A2IT-175E-8136', 3, 'PHP', 12000, '6month', 1, '2021-03-27 14:58:00', '2021-03-09', 'call', NULL, 2, 'Will think. Not sure yet', 1, 3, '2021-03-04', NULL, '2021-03-04 12:45:25'),
(27, 'A2IT-175E-8136', 3, 'php', 0, '6 month', 1, '2021-03-25 15:49:00', '2021-03-17', 'call', NULL, 2, 'Switch off', 1, 3, '2021-03-04', NULL, '2021-03-04 15:07:51'),
(28, 'A2IT-175E-8136', 3, 'java + android', 18000, '6 month', 1, '2021-03-22 12:00:00', '2021-03-19', 'call', NULL, 2, 'call not picked', 1, 3, '2021-03-04', NULL, '2021-03-04 17:36:06'),
(29, 'A2IT-175E-8136', 3, 'MERN stack', 24000, '4month', 1, '2021-03-23 15:36:00', '2021-03-15', 'call', NULL, 2, 'Trainer not available', 1, 3, '2021-03-05', NULL, '2021-03-05 11:18:18'),
(30, 'A2IT-175E-8136', 3, 'Networking', 0, '6month', 1, '2021-03-27 10:20:00', '2021-03-12', 'call', NULL, 2, 'call not picked. Whatsapp message sent on 8059273732', 1, 3, '2021-03-05', NULL, '2021-03-05 11:51:59'),
(31, 'A2IT-175E-8136', 3, 'Networking', 0, '6M', 1, '2021-06-21 12:52:00', '2021-03-05', 'call', NULL, 2, 'He\'s currently working somewhere and now busy in projects. Will contact after June for classes', 1, 3, '2021-03-05', NULL, '2021-03-05 12:53:10'),
(32, 'A2IT-175E-8136', 3, 'Networking', 0, '6M', 1, '2021-04-02 12:01:00', '2021-03-19', 'call', NULL, 2, 'call not picked', 1, 3, '2021-03-05', NULL, '2021-03-05 13:09:40'),
(33, 'A2IT-175E-8136', 2, 'python- data science and AI', 18000, '6 month', 3, '2021-04-01 13:04:00', '2021-03-05', 'call', NULL, 2, '6 month training of B.Tech.. may be visit in 2nd week of april.. 18000 discounted price for complete module of PYTHON@35000.', 1, 2, '2021-03-05', NULL, '2021-03-05 13:14:50'),
(34, 'A2IT-175E-8136', 3, 'CCNA + CCNP', 24000, '5M', 1, '2021-03-11 13:12:00', '2021-03-09', 'walkin', NULL, 2, 'Registered CCNA+CCNP@24K', 2, 3, '2021-03-05', '2021-03-09', '2021-03-05 15:15:26'),
(35, 'A2IT-175E-8136', 3, 'Industry', 4000, '6M', 1, '2021-03-25 16:18:00', '2021-03-11', 'call', NULL, 2, 'He found training in industry free of cost', 4, 3, '2021-03-05', NULL, '2021-03-05 16:04:37'),
(36, 'A2IT-175E-8136', 3, 'Core java + android', 18000, '6 months', 1, '2021-04-03 12:02:00', '2021-03-19', 'call', NULL, 2, 'call not picked', 1, 3, '2021-03-05', NULL, '2021-03-05 17:57:41'),
(37, 'A2IT-175E-8136', 3, 'Netwoking', 24000, '6month', 1, '2021-03-22 09:34:00', '2021-03-06', 'call', NULL, 2, 'Not sure yet. he checked the videos of rajeev sir and quite impressed with it. Planning to join in the month of april', 1, 3, '2021-03-06', NULL, '2021-03-06 09:36:21'),
(38, 'A2IT-175E-8136', 2, 'Ethical hacking', 18000, '5 months', 3, '2021-03-20 12:05:00', '2021-03-06', 'call', NULL, 2, 'inquired for ethical hacking.. want to start classes in april. sllybus semt for CCNA nd CEH', 1, 2, '2021-03-06', NULL, '2021-03-06 12:07:12'),
(39, 'A2IT-175E-8136', 3, 'CCNA', 12000, '45 days', 1, '2021-03-27 17:57:00', '2021-03-12', 'call', NULL, 2, 'call not picked', 1, 3, '2021-03-06', NULL, '2021-03-06 12:54:42'),
(40, 'A2IT-175E-8136', 3, 'Python', 3500, '6M', 1, '2021-03-06 13:18:00', '2021-03-06', 'call', NULL, 2, 'Registered python certificate@3500', 2, 3, '2021-03-06', '2021-03-06', '2021-03-06 13:18:04'),
(41, 'A2IT-175E-8136', 3, 'Web designing', 12000, '2M', 1, '2021-03-25 12:04:00', '2021-03-19', 'call', NULL, 2, 'call not picked', 1, 3, '2021-03-08', NULL, '2021-03-08 13:11:55'),
(42, 'A2IT-175E-8136', 3, 'Networking', 3500, '6 m', 1, '2021-03-10 09:36:00', '2021-03-10', 'whatsapp', NULL, 2, 'Registered for certificate project only', 2, 3, '2021-03-08', '2021-03-10', '2021-03-08 17:43:23'),
(43, 'A2IT-175E-8136', 3, 'Web designing', 12000, '2M', 1, '2021-03-17 09:28:00', '2021-03-17', 'call', NULL, 2, 'Joined somewhere else for MERN stack', 4, 3, '2021-03-08', NULL, '2021-03-08 18:02:08'),
(44, 'A2IT-175E-8136', 3, 'CEH', 12000, '3M', 1, '2021-03-20 09:27:00', '2021-03-15', 'call', NULL, 2, 'Will visit this saturday', 1, 3, '2021-03-09', NULL, '2021-03-09 09:43:24'),
(45, 'A2IT-175E-8136', 3, 'Full stack with java', 0, '6 M', 3, '2021-03-20 10:01:00', '2021-03-09', 'call', NULL, 2, 'Will discuss with family and finalize. Training in April', 1, 3, '2021-03-09', NULL, '2021-03-09 10:01:16'),
(46, 'A2IT-175E-8136', 2, 'AutoCad Civil', 6000, '45 Days', 4, '2021-03-10 10:12:00', '2021-03-10', 'call', NULL, 2, '3000 PAID AT TIME OF REGISTERATION', 2, 2, '2021-03-09', '2021-03-10', '2021-03-09 10:13:13'),
(47, 'A2IT-175E-8136', 2, 'AutoCad', 6000, '45 days', 4, '2021-03-10 10:14:00', '2021-03-10', 'call', NULL, 2, '3000 PAID AT TIME OF REGISTERATION', 2, 2, '2021-03-09', '2021-03-10', '2021-03-09 10:15:17'),
(48, 'A2IT-175E-8136', 2, 'AutoCad', 6000, '45 Days', 4, '2021-03-10 10:17:00', '2021-03-10', 'call', NULL, 2, 'admission done.. 3000 paid.. 3000 will pay after joining for industry training..', 2, 2, '2021-03-09', '2021-03-10', '2021-03-09 10:18:08'),
(49, 'A2IT-175E-8136', 3, 'Web designing', 10000, '2M', 1, '2021-03-11 10:46:00', '2021-03-11', 'call', NULL, 2, 'Joined somewhere else', 4, 3, '2021-03-09', NULL, '2021-03-09 10:46:21'),
(50, 'A2IT-175E-8136', 3, 'Web designing', 10000, '2M', 1, '2021-03-12 10:17:00', '2021-03-12', 'call', NULL, 2, 'Joined somewhere else in derabassi', 4, 3, '2021-03-09', NULL, '2021-03-09 12:02:34'),
(51, 'A2IT-175E-8136', 3, 'FInance or marketing', 3000, '6M', 1, '2021-03-22 12:05:00', '2021-03-19', 'call', NULL, 2, 'Switch off from past 2 days', 1, 3, '2021-03-09', NULL, '2021-03-09 14:46:53'),
(52, 'A2IT-175E-8136', 3, 'Web designing', 12000, '2M', 1, '2021-03-25 15:26:00', '2021-03-17', 'call', NULL, 2, 'not sure yet', 1, 3, '2021-03-10', NULL, '2021-03-10 09:44:54'),
(53, 'A2IT-175E-8136', 3, 'MERN stack', 2500, '6M', 1, '2021-03-31 15:32:00', '2021-03-16', 'call', NULL, 2, 'Will contact by own soon', 1, 3, '2021-03-10', NULL, '2021-03-10 09:47:25'),
(54, 'A2IT-175E-8136', 3, 'CCNA', 10000, '2M', 1, '2021-05-04 10:41:00', '2021-03-10', 'call', NULL, 2, 'Will join in June for the 2-month training. After that can go for CEH in a 6-month internship', 1, 3, '2021-03-10', NULL, '2021-03-10 10:42:21'),
(55, 'A2IT-175E-8136', 3, 'not sure yet', 0, '6 month', 1, '2021-09-10 11:38:00', '2021-03-10', 'call', NULL, 2, 'Training in Jan 2022 6 month', 1, 3, '2021-03-10', NULL, '2021-03-10 11:38:26'),
(56, 'A2IT-175E-8136', 2, 'Networking', 18000, '6 months', 4, '2021-05-31 11:55:00', '2021-03-10', 'call', NULL, 2, '6 month training in 7th or 8th sem. confirm once in may', 1, 2, '2021-03-10', NULL, '2021-03-10 11:56:19'),
(57, 'A2IT-175E-8136', 2, 'networking& CyberSecurity', 18000, '6 months', 4, '2021-05-31 11:58:00', '2021-03-10', 'call', NULL, 2, '6 month training in 7th or 8th sem.. confirm once in may', 1, 2, '2021-03-10', NULL, '2021-03-10 11:59:04'),
(58, 'A2IT-175E-8136', 3, 'CCNP', 0, '4M', 1, '2021-05-22 13:11:00', '2021-03-10', 'call', NULL, 2, 'Will join after MCA completion in the month of June or July', 1, 3, '2021-03-10', NULL, '2021-03-10 13:11:45'),
(59, 'A2IT-175E-8136', 3, 'CCNA', 12000, '2M', 1, '2021-04-16 15:28:00', '2021-03-10', 'call', NULL, 2, 'Will join at the end of april', 1, 3, '2021-03-10', NULL, '2021-03-10 15:28:37'),
(60, 'A2IT-175E-8136', 3, 'Digital market', 0, '6W', 1, '2022-04-04 16:46:00', '2021-03-10', 'call', NULL, 2, '1st year student', 1, 3, '2021-03-10', NULL, '2021-03-10 16:47:05'),
(61, 'A2IT-175E-8136', 3, 'CCNA', 0, NULL, 1, '2021-09-10 16:55:00', '2021-03-10', 'call', NULL, 2, 'training in Jan 2022', 1, 3, '2021-03-10', NULL, '2021-03-10 16:54:55'),
(62, 'A2IT-175E-8136', 3, 'not sure', 0, NULL, 1, '2021-09-09 16:57:00', '2021-03-10', 'call', NULL, 2, 'Training in jan 2022', 1, 3, '2021-03-10', NULL, '2021-03-10 16:56:21'),
(63, 'A2IT-175E-8136', 3, 'CCNA', 0, NULL, 1, '2021-06-18 17:07:00', '2021-03-10', 'call', NULL, 2, 'after diploma', 1, 3, '2021-03-10', NULL, '2021-03-10 17:06:20'),
(64, 'A2IT-175E-8136', 2, 'NETWORKING AND ETHICAL HACKING', 18000, '4 MONTHS', 1, '2021-04-05 14:25:00', '2021-03-11', 'whatsapp', NULL, 2, 'not sure about classes right now..\r\nnumber shared with rajeev sir', 1, 2, '2021-03-10', NULL, '2021-03-10 18:36:00'),
(65, 'A2IT-175E-8136', 2, 'react JS', 15000, '2 months', 6, '2021-03-15 12:50:00', '2021-03-11', 'call', NULL, 2, 'Call back after confimation from sukhdarshan sir.', 1, 2, '2021-03-11', NULL, '2021-03-11 12:58:23'),
(66, 'A2IT-175E-8136', 2, 'Autocad', 3000, '6 weeks', 6, '2021-05-22 13:36:00', '2021-03-11', 'call', NULL, 2, '6 weeks training will start in june..', 1, 2, '2021-03-11', NULL, '2021-03-11 13:41:49'),
(67, 'A2IT-175E-8136', 2, 'civil softwares', 3500, '6 week', 4, '2021-05-17 13:43:00', '2021-03-11', 'call', NULL, 2, 'seems bit intersted but doubtfull due to no confirmation about training in college.. \r\nautocad 3500, revit & Staad Pro- 5000, 3DS MAX- 8000', 1, 2, '2021-03-11', NULL, '2021-03-11 13:45:13'),
(68, 'A2IT-175E-8136', 3, 'PLC', 0, '6W', 1, '2021-03-27 14:27:00', '2021-03-11', 'call', NULL, 2, 'Will think about this', 1, 3, '2021-03-11', NULL, '2021-03-11 14:27:23'),
(69, 'A2IT-175E-8136', 3, 'PLC', 0, '6W', 1, '2021-03-27 14:28:00', '2021-03-11', 'call', NULL, 2, 'Will finalize and call back', 1, 3, '2021-03-11', NULL, '2021-03-11 14:28:48'),
(70, 'ZCC -E5B0-8C47', 5, 'Core & Adv. Python', 5000, '6 weeks', 6, '2021-03-13 11:17:00', '2021-03-12', 'call', NULL, 2, 'not confirmed yet', 1, 5, '2021-03-11', NULL, '2021-03-11 15:50:03'),
(71, 'ZCC -E5B0-8C47', 5, 'web Development with python', 18000, '6 months', 1, '2021-03-13 15:52:00', '2021-03-11', 'call', NULL, 2, '6 month training in 7th or 8th sem.', 1, 5, '2021-03-11', NULL, '2021-03-11 15:54:01'),
(72, 'ZCC -E5B0-8C47', 5, 'web Development with python', 18000, '6 months', 1, '2021-03-13 15:52:00', '2021-03-11', 'call', NULL, 2, '6 month training in 7th or 8th sem.', 1, 5, '2021-03-11', NULL, '2021-03-11 15:54:03'),
(73, 'ZCC -E5B0-8C47', 5, 'networing', 18000, '6 months', 4, '2021-03-15 15:56:00', '2021-03-11', 'call', NULL, 2, 'plan to visit after 15 march', 1, 5, '2021-03-11', NULL, '2021-03-11 15:56:58'),
(74, 'ZCC -E5B0-8C47', 5, 'networing', 18000, '6 months', 4, '2021-03-15 15:56:00', '2021-03-11', 'call', NULL, 2, 'plan to visit after 15 march', 1, 5, '2021-03-11', NULL, '2021-03-11 15:56:58'),
(75, 'A2IT-175E-8136', 3, 'Automation', 4500, '6W', 1, '2021-04-02 16:20:00', '2021-03-11', 'call', NULL, 2, 'Will confirm soon', 1, 3, '2021-03-11', NULL, '2021-03-11 16:21:00'),
(76, 'A2IT-175E-8136', 2, 'WEB DESIGNING', 10000, '2 MONTHS`', 1, '2021-03-15 14:39:00', '2021-03-12', 'walkin', NULL, 2, 'visited today.. intersted to join but confused about technology..', 1, 2, '2021-03-11', NULL, '2021-03-11 16:42:31'),
(77, 'A2IT-175E-8136', 3, 'CNC Miling', 6000, '6W', 1, '2021-03-25 17:15:00', '2021-03-18', 'call', NULL, 2, 'Trainer not available', 1, 3, '2021-03-12', NULL, '2021-03-12 14:36:30'),
(78, 'A2IT-175E-8136', 3, 'Not sure yet', 4500, '6W', 1, '2021-04-16 15:25:00', '2021-03-12', 'call', NULL, 2, 'Will finalize the course and let us know. The first priority is Stipend internship', 1, 3, '2021-03-12', NULL, '2021-03-12 15:25:46'),
(79, 'A2IT-175E-8136', 3, 'not Sure', 0, '6M', 1, '2021-06-19 15:29:00', '2021-03-12', 'call', NULL, 2, 'Training in 7th sem. Current sem 6th', 1, 3, '2021-03-12', NULL, '2021-03-12 15:29:26'),
(80, 'A2IT-175E-8136', 3, 'Not sure', 0, '6M', 1, '2021-06-12 15:31:00', '2021-03-12', 'call', NULL, 2, 'Training in 7th sem. Current sem is 6th', 1, 3, '2021-03-12', NULL, '2021-03-12 15:31:06'),
(81, 'A2IT-175E-8136', 3, 'Not sure', 0, '6M', 1, '2021-09-02 15:34:00', '2021-03-12', 'call', NULL, 2, 'Currently in 6th sem.', 1, 3, '2021-03-12', NULL, '2021-03-12 15:34:16'),
(82, 'A2IT-175E-8136', 3, 'Java', 0, '6M', 1, '2021-09-13 16:11:00', '2021-03-12', 'call', NULL, 2, 'Will join a2it in the future for java course', 1, 3, '2021-03-12', NULL, '2021-03-12 16:11:01'),
(83, 'A2IT-175E-8136', 2, 'CEH', 18000, '2months', 4, '2021-05-15 16:21:00', '2021-03-12', 'call', NULL, 2, 'training in June or in 4th sem.', 1, 2, '2021-03-12', NULL, '2021-03-12 16:23:34'),
(84, 'A2IT-175E-8136', 3, 'not sure', 4500, '6W', 1, '2021-04-15 16:58:00', '2021-03-12', 'call', NULL, 2, 'Will think after exams', 1, 3, '2021-03-12', NULL, '2021-03-12 16:59:10'),
(85, 'A2IT-175E-8136', 3, 'CCNA', 12000, '6M', 1, '2021-09-13 17:33:00', '2021-03-12', 'call', NULL, 2, 'Training in 8th sem', 1, 3, '2021-03-12', NULL, '2021-03-12 17:32:54'),
(86, 'A2IT-175E-8136', 2, 'networing', 12000, '2 months', 1, '2021-03-15 14:48:00', '2021-03-13', 'walkin', NULL, 2, 'will come on monday for demo classes, after that will complete admission process.', 1, 2, '2021-03-12', NULL, '2021-03-12 18:20:12'),
(87, 'A2IT-175E-8136', 3, 'basic computers', 5000, '1month', 6, '2021-03-22 17:09:00', '2021-03-18', 'walkin', NULL, 2, 'Will start classes from Monday', 1, 2, '2021-03-13', NULL, '2021-03-13 10:27:42'),
(88, 'A2IT-175E-8136', 2, 'web designing', 8000, '2 months', 1, '2021-04-05 11:33:00', '2021-03-16', 'call', NULL, 2, 'looking for some part time job... only after that will join classes..', 1, 2, '2021-03-13', NULL, '2021-03-13 12:19:30'),
(89, 'A2IT-175E-8136', 3, 'Python', 7000, '6M', 1, '2021-03-16 15:35:00', '2021-03-16', 'call', NULL, 2, 'Python with Django 6 month training', 2, 3, '2021-03-15', '2021-03-16', '2021-03-15 10:29:37'),
(90, 'A2IT-175E-8136', 3, 'Networking', 8000, '6M', 1, '2021-03-19 17:14:00', '2021-03-19', 'call', NULL, 2, 'Need online classes', 2, 3, '2021-03-15', '2021-03-19', '2021-03-15 11:15:37'),
(91, 'A2IT-175E-8136', 3, 'Basic computers', 5000, '1M', 1, '2021-03-16 13:41:00', '2021-03-16', 'call', NULL, 2, 'Basic computers. Main focus on excel. But need practice time more', 2, 3, '2021-03-15', '2021-03-16', '2021-03-15 13:42:31'),
(92, 'A2IT-175E-8136', 3, 'CNC ME', 18000, '3M', 1, '2021-03-22 15:25:00', '2021-03-17', 'walkin', NULL, 2, 'call not picked', 1, 3, '2021-03-15', NULL, '2021-03-15 15:34:14'),
(93, 'A2IT-175E-8136', 3, 'DBMS', 6000, '45 days', 1, '2021-03-15 17:03:00', '2021-03-15', 'meeting', NULL, 2, 'Registered for dbms. May b he can enroll for web designing as well', 2, 3, '2021-03-15', '2021-03-15', '2021-03-15 17:05:50'),
(94, 'A2IT-175E-8136', 3, 'Project file', 4000, '6W', 1, '2021-03-16 14:48:00', '2021-03-18', 'call', NULL, 2, 'Files issue.', 4, 3, '2021-03-16', NULL, '2021-03-16 14:48:40'),
(95, 'A2IT-175E-8136', 3, 'Autocad', 4000, '6W', 1, '2021-03-22 15:41:00', '2021-03-19', 'call', NULL, 2, 'call not picked', 1, 3, '2021-03-16', NULL, '2021-03-16 15:01:21'),
(96, 'A2IT-175E-8136', 3, 'HR or DM', 7000, '6W', 1, '2021-04-30 15:10:00', '2021-03-16', 'call', NULL, 2, 'Will think and decide in future', 1, 3, '2021-03-16', NULL, '2021-03-16 15:09:53'),
(97, 'A2IT-175E-8136', 3, 'PHP', 6000, '1M', 1, '2021-03-25 15:37:00', '2021-03-19', 'call', NULL, 2, 'call not picked', 1, 3, '2021-03-16', NULL, '2021-03-16 17:47:37'),
(98, 'A2IT-175E-8136', 3, 'Web designing', 8000, '6W', 1, '2021-03-22 17:17:00', '2021-03-18', 'call', NULL, 2, 'Visit on moday for sure. Will also try to come tomorrow after getting free', 1, 3, '2021-03-17', NULL, '2021-03-17 13:16:56'),
(99, 'A2IT-175E-8136', 3, 'Python', 18000, '6M', 1, '2021-03-22 15:16:00', '2021-03-17', 'walkin', NULL, 2, 'Visited today. suggested to go for networking due to USA visa', 1, 3, '2021-03-17', NULL, '2021-03-17 14:48:46'),
(100, 'A2IT-175E-8136', 3, 'python with DS', 20000, '5M', 1, '2021-03-26 15:43:00', '2021-03-17', 'call', NULL, 2, 'Will call back soon for the process', 1, 3, '2021-03-17', NULL, '2021-03-17 15:44:07'),
(101, 'A2IT-175E-8136', 3, 'Finace', 4000, '6W', 1, '2021-03-22 17:49:00', '2021-03-17', 'call', NULL, 2, 'Will contact soon for the training purpose', 1, 3, '2021-03-17', NULL, '2021-03-17 17:49:39'),
(102, 'A2IT-175E-8136', 3, 'Basic computers', 5000, '45Days', 1, '2021-03-22 15:40:00', '2021-03-19', 'call', NULL, 2, 'call not connected', 1, 3, '2021-03-18', NULL, '2021-03-18 15:20:00'),
(103, 'A2IT-175E-8136', 3, 'Basic + Tax', 7000, '3M', 1, '2021-03-22 17:11:00', '2021-03-18', 'walkin', NULL, 2, 'Will start classes from Monday', 1, 3, '2021-03-18', NULL, '2021-03-18 17:10:54'),
(104, 'A2IT-175E-8136', 3, 'CCNA', 10000, '2M', 1, '2021-03-22 17:57:00', '2021-03-18', 'call', NULL, 2, 'Currently in Jammu. Will visit soon for training. may b on Monday', 1, 3, '2021-03-18', NULL, '2021-03-18 17:57:23'),
(105, 'A2IT-175E-8136', 3, 'Networking', 4500, '6W', 1, '2021-04-15 14:03:00', '2021-03-19', 'call', NULL, 2, 'Interested in networking. Basic details shared', 1, 3, '2021-03-19', NULL, '2021-03-19 14:04:18'),
(106, 'A2IT-175E-8136', 3, 'CEH', 10000, '2M', 1, '2021-03-26 15:31:00', '2021-03-19', 'call', NULL, 2, 'Will finalize soon for the training', 1, 3, '2021-03-19', NULL, '2021-03-19 15:31:32'),
(107, 'A2IT-175E-8136', 3, 'Advance java', 18000, '5M', 1, '2021-03-20 15:34:00', '2021-03-19', 'call', NULL, 2, 'Will visit tomorrow regarding training inquiry', 1, 3, '2021-03-19', NULL, '2021-03-19 15:34:21'),
(108, 'A2IT-175E-8136', 3, 'HR', 8000, '2m', 1, '2021-03-23 15:51:00', '2021-03-19', 'call', NULL, 2, 'Will discuss with family. Currently in Ambala', 1, 3, '2021-03-19', NULL, '2021-03-19 15:51:11'),
(109, 'A2IT-175E-8136', 3, 'Cyber security', 10000, '6W', 1, '2021-04-15 16:50:00', '2021-03-19', 'call', NULL, 2, 'Will contact for training in april', 1, 3, '2021-03-19', NULL, '2021-03-19 16:50:13'),
(110, 'A2IT-175E-8136', 3, 'Java', 4000, '6W', 1, '2021-04-15 17:39:00', '2021-03-19', 'call', NULL, 2, 'Will finalize in April either she has to pursue training or go for non-attending procedure', 1, 3, '2021-03-19', NULL, '2021-03-19 17:42:31');

-- --------------------------------------------------------

--
-- Table structure for table `lead_profiles`
--

CREATE TABLE `lead_profiles` (
  `id` int NOT NULL,
  `ucid` varchar(255) NOT NULL,
  `lead_id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `alt_phone` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `address` text NOT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `country` varchar(100) NOT NULL,
  `status` int NOT NULL DEFAULT '1',
  `created_by` int NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `lead_profiles`
--

INSERT INTO `lead_profiles` (`id`, `ucid`, `lead_id`, `name`, `phone`, `alt_phone`, `email`, `dob`, `address`, `city`, `state`, `country`, `status`, `created_by`, `created_at`) VALUES
(1, 'A2IT-175E-8136', 1, 'Aditya', '7876255530', NULL, NULL, NULL, 'mohali', 'mohali', 'Punjab', 'India', 1, 1, '2021-02-25 17:53:22'),
(2, 'A2IT-175E-8136', 2, 'geeta', '8219060598', NULL, NULL, NULL, 'jammu', 'mohali', 'Punjab', 'India', 1, 1, '2021-02-25 17:57:54'),
(5, 'A2IT-175E-8136', 5, 'yashawee', '9874525697', NULL, 'mac@mac.com', NULL, 'VIP Road', 'Zirakpur', 'Punjab', 'India', 1, 1, '2021-02-27 11:57:40'),
(6, 'A2IT-175E-8136', 6, 'poonam', '7018675419', NULL, NULL, NULL, 'pgc Sec 11', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-01 11:36:41'),
(7, 'A2IT-175E-8136', 7, 'rachna', '7018534374', NULL, NULL, NULL, 'sector- 37', 'chANDIGARH', 'Chandigarh', 'India', 1, 1, '2021-03-01 12:01:40'),
(8, 'A2IT-175E-8136', 8, 'Mahtab Akram', '8294204487', NULL, 'mahtabakram600@gmail.com', NULL, 'Vill-chandardipa po-jogiyara ps-jale dist-darbhanga bihar', 'BIHAR', 'Bihar', 'India', 1, 1, '2021-03-01 14:14:13'),
(9, 'A2IT-175E-8136', 9, 'Danish', '7006561842', NULL, NULL, NULL, 'Mewar College', 'jammu', 'Jammu and Kashmir', 'India', 1, 2, '2021-03-01 15:40:11'),
(10, 'A2IT-175E-8136', 10, 'Fabien Fabo', '9501678857', NULL, NULL, NULL, 'old student', 'Punjab', 'Punjab', 'India', 1, 2, '2021-03-01 16:09:31'),
(11, 'A2IT-175E-8136', 11, 'Rohit', '7888605843', NULL, NULL, NULL, 'accenture', 'mohali', 'Punjab', 'India', 1, 2, '2021-03-01 16:30:55'),
(12, 'A2IT-175E-8136', 12, 'Sandeep', '9518638025', '7206719043', NULL, NULL, 'UGI', 'mohali', 'Punjab', 'India', 1, 2, '2021-03-01 16:38:34'),
(13, 'A2IT-175E-8136', 13, 'Megha', '9501965998', NULL, NULL, NULL, 'Chandigarh', 'chandigarh', 'Chandigarh', 'India', 1, 1, '2021-03-01 17:59:33'),
(14, 'A2IT-175E-8136', 14, 'Neha', '9034186660', NULL, NULL, NULL, 'mohali', 'MOHALI', 'Chandigarh', 'India', 1, 1, '2021-03-01 18:12:49'),
(15, 'A2IT-175E-8136', 15, 'Karanveer', '9988098069', NULL, NULL, NULL, 'Working', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-02 10:30:05'),
(16, 'A2IT-175E-8136', 16, 'Mandeep', '9417350677', NULL, NULL, NULL, 'Data', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-02 11:50:54'),
(17, 'A2IT-175E-8136', 17, 'Rajesh', '9815548447', NULL, NULL, NULL, 'SUS Diploma 6th sem', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-03 12:31:12'),
(18, 'A2IT-175E-8136', 18, 'Shallu', '8219346437', NULL, NULL, NULL, 'vaishno college', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-03 15:43:15'),
(19, 'A2IT-175E-8136', 19, 'Danish', '9149647932', NULL, NULL, NULL, 'vaishno college', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-03 15:46:56'),
(20, 'A2IT-175E-8136', 20, 'Rekha', '6006629289', NULL, NULL, NULL, 'vaishno college', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-03 15:48:39'),
(21, 'A2IT-175E-8136', 21, 'Jatin', '8699851447', NULL, NULL, '2021-08-03', 'IET Bhaddal', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-03 15:55:52'),
(22, 'A2IT-175E-8136', 22, 'Deepak', '9876943810', NULL, NULL, NULL, 'IET Bhaddal', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-03 15:57:31'),
(23, 'A2IT-175E-8136', 23, 'Sandeep', '6392579712', NULL, NULL, NULL, 'Online training', 'Zirakpur', 'Punjab', 'India', 1, 2, '2021-03-03 16:51:46'),
(24, 'A2IT-175E-8136', 24, 'jyoti', '7807325700', NULL, NULL, NULL, 'HP university', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-03 17:47:00'),
(25, 'A2IT-175E-8136', 25, 'Baldev', '9592260150', NULL, NULL, NULL, 'Adesh college / EE', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-04 11:55:01'),
(26, 'A2IT-175E-8136', 26, 'Ajaypal', '8054857774', NULL, NULL, NULL, 'Adesh /CSE', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-04 12:45:25'),
(27, 'A2IT-175E-8136', 27, 'Sharanpreet kaur', '9855646837', NULL, NULL, NULL, 'adesh fdk / cse', 'mohali', 'Punjab', 'India', 1, 2, '2021-03-04 15:07:51'),
(28, 'A2IT-175E-8136', 28, 'Rahil', '7780829699', NULL, NULL, NULL, 'UGI', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-04 17:36:06'),
(29, 'A2IT-175E-8136', 29, 'Aditya Jha', '7492884310', NULL, NULL, NULL, 'C.D.L.S.I.E.T PANNIWALA MOTA SIRSA', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-05 11:18:18'),
(30, 'A2IT-175E-8136', 30, 'Ajay', '7357306119', '8059273732', NULL, NULL, 'Kurukshetra University Kurukshetra', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-05 11:51:59'),
(31, 'A2IT-175E-8136', 31, 'Arpit shrivas', '8289031484', NULL, NULL, NULL, 'Chandigarh university/ working now', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-05 12:53:10'),
(32, 'A2IT-175E-8136', 32, 'Aakrit Kaushal', '7018965940', NULL, NULL, NULL, 'Government colleage of commerce and business Administration', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-05 13:09:40'),
(33, 'A2IT-175E-8136', 33, 'vikas', '9199214427', NULL, NULL, NULL, 'chandigarh University', 'chandigarh', 'Punjab', 'India', 1, 1, '2021-03-05 13:14:50'),
(34, 'A2IT-175E-8136', 34, 'Shivam', '7009278981', NULL, NULL, NULL, 'USA Visa', 'mohali', 'Punjab', 'India', 1, 2, '2021-03-05 15:15:26'),
(35, 'A2IT-175E-8136', 35, 'Prince', '8793758072', NULL, NULL, NULL, 'SUS ME', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-05 16:04:37'),
(36, 'A2IT-175E-8136', 36, 'Shivani', '7888532245', NULL, NULL, NULL, 'PGC SEC 42', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-05 17:57:41'),
(37, 'A2IT-175E-8136', 37, 'Malik imtiaz', '9797250624', NULL, NULL, NULL, 'J&K', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-06 09:36:21'),
(38, 'A2IT-175E-8136', 38, 'harish', '8307235749', NULL, NULL, NULL, 'Revari, Gurgaon', 'Revari', 'Haryana', 'India', 1, 1, '2021-03-06 12:07:12'),
(39, 'A2IT-175E-8136', 39, 'Gopal Pandey', '8909942100', NULL, NULL, NULL, 'Working / online', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-06 12:54:42'),
(40, 'A2IT-175E-8136', 40, 'vaishali', '8219849591', NULL, NULL, NULL, 'Vaishno college', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-06 13:18:04'),
(41, 'A2IT-175E-8136', 41, 'Abhinav', '6239908332', NULL, NULL, NULL, 'Passout', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-08 13:11:55'),
(42, 'A2IT-175E-8136', 42, 'Rehan', '9113455788', '9523025485', NULL, NULL, 'Mewar Uni / BCA', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-08 17:43:23'),
(43, 'A2IT-175E-8136', 43, 'Darpan Sharma', '9877050383', NULL, NULL, NULL, 'Just dial', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-08 18:02:08'),
(44, 'A2IT-175E-8136', 44, 'Haresh', '9284747826', NULL, NULL, NULL, 'Working night shift', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-09 09:43:24'),
(45, 'A2IT-175E-8136', 45, 'Manisha', '8572042771', NULL, NULL, NULL, 'GJU Hisar MCA', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-09 10:01:16'),
(46, 'A2IT-175E-8136', 46, 'Dharmender Rana', '8894479877', NULL, 'rdharmender09@gmail.com', NULL, 'V.P.O: Preena, tehsil&district: Chamba, pin code: 176311, H.P.', 'Chamba', 'Himachal Pradesh', 'India', 1, 1, '2021-03-09 10:13:13'),
(47, 'A2IT-175E-8136', 47, 'Sunil Kumar', '8219337739', NULL, 'kumarsunil88383@gmail.com', NULL, 'Village: Ridi, Post office: Kachhera tehsil: Jaisinghpur, kangra H.P. , 176076', 'Kangra', 'Himachal Pradesh', 'India', 1, 1, '2021-03-09 10:15:17'),
(48, 'A2IT-175E-8136', 48, 'Aniket', '7807256052', NULL, 'choudharyaniket082@gmail.com', NULL, 'Village: bharoon, post office : bindraban, tehsil: palampur district: kangra , pin code: 176061 Himachal Pradesh', 'Palampur', 'Himachal Pradesh', 'India', 1, 1, '2021-03-09 10:18:08'),
(49, 'A2IT-175E-8136', 49, 'Nisha', '7625902052', NULL, NULL, NULL, 'Offline', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-09 10:46:21'),
(50, 'A2IT-175E-8136', 50, 'Vishwash', '7582000065', NULL, NULL, NULL, 'Working in 7 phase', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-09 12:02:34'),
(51, 'A2IT-175E-8136', 51, 'Tahir Majeed Gojri', '6006388301', NULL, NULL, NULL, 'BBA Mewar Uni', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-09 14:46:53'),
(52, 'A2IT-175E-8136', 52, 'Ameen Khan', '8351092510', NULL, NULL, NULL, 'MCA/ sant baba bhag singh uni', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-10 09:44:54'),
(53, 'A2IT-175E-8136', 53, 'Anuj', '7837819360', NULL, NULL, NULL, 'CT college', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-10 09:47:25'),
(54, 'A2IT-175E-8136', 54, 'Harpreet Kaur', '6283438119', NULL, NULL, NULL, 'DAV Institute of engineering and technology', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-10 10:42:21'),
(55, 'A2IT-175E-8136', 55, 'Pooja Thakur', '9816289101', NULL, NULL, NULL, 'HP uni/ MCA', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-10 11:38:26'),
(56, 'A2IT-175E-8136', 56, 'dhananjai Dogra', '7696658263', NULL, 'dhananjaidogra5@gmail.com', NULL, 'Jalandhar', 'Jalandhar', 'Punjab', 'India', 1, 1, '2021-03-10 11:56:19'),
(57, 'A2IT-175E-8136', 57, 'Avneet Kaur', '8968245004', NULL, 'avneetkaur2365@gmail.com', NULL, 'Sant Baba Bhag singh university, Hoshiarpur', 'Hoshiarpur', 'Punjab', 'India', 1, 1, '2021-03-10 11:59:04'),
(58, 'A2IT-175E-8136', 58, 'Jatin Gautam', '7087625453', NULL, NULL, NULL, 'IET Bhaddal MCA final', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-10 13:11:45'),
(59, 'A2IT-175E-8136', 59, 'Abdul Azeem', '7347542296', NULL, NULL, NULL, 'Working', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-10 15:28:37'),
(60, 'A2IT-175E-8136', 60, 'Akshit Bansal', '7888500941', NULL, NULL, NULL, 'Chitkara	2020-2024/', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-10 16:47:05'),
(61, 'A2IT-175E-8136', 61, 'Pooja chauhan', '8295176953', NULL, NULL, NULL, 'SD college', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-10 16:54:55'),
(62, 'A2IT-175E-8136', 62, 'Manakmeet singh', '7347624475', NULL, NULL, NULL, 'Global clg Asr.', 'Moahli', 'Punjab', 'India', 1, 2, '2021-03-10 16:56:21'),
(63, 'A2IT-175E-8136', 63, 'Vivek Kumar', '+916202675670', NULL, NULL, NULL, 'Diploma cse SLIET', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-10 17:06:20'),
(64, 'A2IT-175E-8136', 64, 'ABHIJEET', '7986832321', NULL, NULL, NULL, 'PUNJAB UNIVERSITY', 'CHANDIGARH', 'Punjab', 'India', 1, 1, '2021-03-10 18:36:00'),
(65, 'A2IT-175E-8136', 65, 'munish', '9569525026', NULL, NULL, NULL, 'kharar.', 'kharar', 'Punjab', 'India', 1, 1, '2021-03-11 12:58:23'),
(66, 'A2IT-175E-8136', 66, 'arhaan', '6395737274', NULL, NULL, NULL, 'Saharanpur', 'saharanpur', 'Himachal Pradesh', 'India', 1, 1, '2021-03-11 13:41:50'),
(67, 'A2IT-175E-8136', 67, 'asim Khan', '7217475129', NULL, NULL, NULL, 'Saharanpur', 'saharanpur', 'Himachal Pradesh', 'India', 1, 1, '2021-03-11 13:45:13'),
(68, 'A2IT-175E-8136', 68, 'Rajiv', '6207327382', NULL, NULL, NULL, 'Mewar Uni / EE', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-11 14:27:23'),
(69, 'A2IT-175E-8136', 69, 'Govind Singh', '6376549754', NULL, NULL, NULL, 'Mewar uni / EE', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-11 14:28:48'),
(70, 'ZCC -E5B0-8C47', 70, 'aniket', '6280469744', NULL, NULL, NULL, 'B.tech- CSE, 4th Sem, Chandigarh University', 'kharar', 'Punjab', 'India', 1, 3, '2021-03-11 15:50:03'),
(71, 'ZCC -E5B0-8C47', 71, 'mahommad asif', '8556981828', NULL, NULL, NULL, 'B.TECH - CSE, 6th Sem, Chandigarh University.', 'kharar', 'Punjab', 'India', 1, 3, '2021-03-11 15:54:01'),
(72, 'ZCC -E5B0-8C47', 72, 'mahommad asif', '8556981828', NULL, NULL, NULL, 'B.TECH - CSE, 6th Sem, Chandigarh University.', 'kharar', 'Punjab', 'India', 1, 3, '2021-03-11 15:54:03'),
(73, 'ZCC -E5B0-8C47', 73, 'shailendra kumar', '6361189369', NULL, NULL, NULL, 'B.Tech. - CSE, 6th Sem, CU', 'Kharar', 'Punjab', 'India', 1, 3, '2021-03-11 15:56:58'),
(74, 'ZCC -E5B0-8C47', 74, 'shailendra kumar', '6361189369', NULL, NULL, NULL, 'B.Tech. - CSE, 6th Sem, CU', 'Kharar', 'Punjab', 'India', 1, 3, '2021-03-11 15:56:58'),
(75, 'A2IT-175E-8136', 75, 'Prakash', '7632060727', NULL, NULL, NULL, 'Mewar uni / EE', 'mohali', 'Punjab', 'India', 1, 2, '2021-03-11 16:21:00'),
(76, 'A2IT-175E-8136', 76, 'POOJA', '8580407974', NULL, NULL, NULL, 'BILASPUR, HIMACHAL PRADESH\r\nBCA PASSOUT', 'BILASPUR', 'Himachal Pradesh', 'India', 1, 1, '2021-03-11 16:42:31'),
(77, 'A2IT-175E-8136', 77, 'Dinesh', '9779473453', NULL, NULL, NULL, 'Mechanical', 'mohali', 'Punjab', 'India', 1, 2, '2021-03-12 14:36:30'),
(78, 'A2IT-175E-8136', 78, 'Vassu Changotra', '8708389863', NULL, NULL, NULL, 'PU Campus', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-12 15:25:46'),
(79, 'A2IT-175E-8136', 79, 'Damanjeet Kaur Pawar', '7719501872', NULL, NULL, NULL, 'baba bagh Singh university', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-12 15:29:26'),
(80, 'A2IT-175E-8136', 80, 'Avneet kaur', '8968245004', NULL, NULL, NULL, 'baba bagh singh university', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-12 15:31:06'),
(81, 'A2IT-175E-8136', 81, 'Ayush Ratnam Sinha', '9199743403', NULL, NULL, NULL, 'CGC LANDRA', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-12 15:34:16'),
(82, 'A2IT-175E-8136', 82, 'Ansh Raheja', 'Ansh Raheja', NULL, NULL, NULL, 'CGC LANDRA / 6th sem', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-12 16:11:01'),
(83, 'A2IT-175E-8136', 83, 'Savreet kaur', '9729447497', NULL, NULL, NULL, '1st year, Masters in forensic science, LPU, Phagwara', 'Phagwara', 'Punjab', 'India', 1, 1, '2021-03-12 16:23:34'),
(84, 'A2IT-175E-8136', 84, 'AMAN KUMAR', '9097387512', NULL, NULL, NULL, 'SLIET / CSE /4th sem', 'mohali', 'Punjab', 'India', 1, 2, '2021-03-12 16:59:11'),
(85, 'A2IT-175E-8136', 85, 'Tamanaa', '9501216584', NULL, NULL, NULL, 'Sant Baba bhag singh university / 6th sem', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-12 17:32:54'),
(86, 'A2IT-175E-8136', 86, 'rahul', '9781039744', NULL, NULL, NULL, 'Toronto. currently in india', 'MOHALI', 'Punjab', 'India', 1, 1, '2021-03-12 18:20:12'),
(87, 'A2IT-175E-8136', 87, 'Anushka', '6396562525', NULL, NULL, NULL, 'CGC Landra', 'Kharar', 'Punjab', 'India', 1, 1, '2021-03-13 10:27:42'),
(88, 'A2IT-175E-8136', 88, 'ejaz khan', '9041667864', NULL, 'ekkabeerkhan@gmail.com', '1991-08-05', 'LPU, living in Mohali', 'Ludhiana', 'Punjab', 'India', 1, 1, '2021-03-13 12:19:30'),
(89, 'A2IT-175E-8136', 89, 'rajesh', '9672203455', NULL, NULL, NULL, 'Mewar Uni /BCA', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-15 10:29:37'),
(90, 'A2IT-175E-8136', 90, 'Giriraj', '9602023486', NULL, NULL, NULL, 'Mewar Uni / BCA', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-15 11:15:37'),
(91, 'A2IT-175E-8136', 91, 'Avneet kaur', '9872526645', NULL, NULL, NULL, 'Father\'s contact number', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-15 13:42:31'),
(92, 'A2IT-175E-8136', 92, 'Amarjeet', '8837677124', NULL, NULL, NULL, 'Working/call center', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-15 15:34:14'),
(93, 'A2IT-175E-8136', 93, 'Ajay Grewal', '7876577476', NULL, NULL, NULL, 'Passout', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-15 17:05:50'),
(94, 'A2IT-175E-8136', 94, 'abdullah hemant', '8951810067', NULL, NULL, NULL, 'CU / MBA', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-16 14:48:40'),
(95, 'A2IT-175E-8136', 95, 'Vaishali Khulbe', '9306372814', NULL, NULL, NULL, 'Bachelor of Architecture (B.Arch)	CCET !st sem', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-16 15:01:21'),
(96, 'A2IT-175E-8136', 96, 'Hayena Gupta', '7011459386', NULL, NULL, NULL, 'Bachelor of Management Studies (B.M.S.)\r\nShaheed Rajguru College Of Applied Sciences For Women 2020 - 2023', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-16 15:09:53'),
(97, 'A2IT-175E-8136', 97, 'Rohit', '7986325435', NULL, NULL, NULL, 'Online inquiry', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-16 17:47:37'),
(98, 'A2IT-175E-8136', 98, 'Amit Kumar', '9736690763', NULL, NULL, NULL, 'Passout', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-17 13:16:56'),
(99, 'A2IT-175E-8136', 99, 'Davinder', '9878714661', NULL, NULL, NULL, 'Passout', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-17 14:48:46'),
(100, 'A2IT-175E-8136', 100, 'Harshpreet Singh', '7018227148', NULL, NULL, NULL, 'Website', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-17 15:44:07'),
(101, 'A2IT-175E-8136', 101, 'Sahil', '8295410961', NULL, NULL, NULL, 'Galaxy global college Ambala MBA', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-17 17:49:39'),
(102, 'A2IT-175E-8136', 102, 'Lovepreet', '7009294136', NULL, NULL, NULL, 'Passout', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-18 15:20:00'),
(103, 'A2IT-175E-8136', 103, 'Dishti Gupta', '9070167888', NULL, NULL, NULL, 'CGC Landran MBA final', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-18 17:10:54'),
(104, 'A2IT-175E-8136', 104, 'Pranav', '9086359388', NULL, NULL, NULL, 'Passout 2020', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-18 17:57:23'),
(105, 'A2IT-175E-8136', 105, 'Nitish', '6002857330', NULL, NULL, NULL, 'CU BCA 4th sem', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-19 14:04:18'),
(106, 'A2IT-175E-8136', 106, 'Jashan', '8295649826', NULL, NULL, NULL, 'Passout', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-19 15:31:32'),
(107, 'A2IT-175E-8136', 107, 'Mohit Joshi', '8171883371', NULL, NULL, NULL, 'Passout', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-19 15:34:21'),
(108, 'A2IT-175E-8136', 108, 'neel', '7008729211', NULL, NULL, NULL, 'MBA 2020 passout', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-19 15:51:11'),
(109, 'A2IT-175E-8136', 109, 'Arnav', '7065290777', NULL, NULL, NULL, 'CU BCA 4th sem', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-19 16:50:13'),
(110, 'A2IT-175E-8136', 110, 'Ruchika', '7352255391', NULL, NULL, NULL, 'CU BCA  4th sem', 'Mohali', 'Punjab', 'India', 1, 2, '2021-03-19 17:42:31');

-- --------------------------------------------------------

--
-- Table structure for table `masters`
--

CREATE TABLE `masters` (
  `id` int NOT NULL,
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `display_name` varchar(100) NOT NULL,
  `avatar` varchar(255) NOT NULL,
  `first_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `last_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `status` int NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int NOT NULL,
  `sent_by` int NOT NULL,
  `sent_to` int NOT NULL,
  `lead_id` int DEFAULT NULL,
  `subject` text NOT NULL,
  `message` text NOT NULL,
  `is_read` int DEFAULT '0',
  `status` int NOT NULL DEFAULT '1',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint UNSIGNED NOT NULL,
  `model_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_permissions`
--

INSERT INTO `model_has_permissions` (`permission_id`, `model_type`, `model_id`) VALUES
(12, 'App\\Models\\User', 21),
(13, 'App\\Models\\User', 21),
(14, 'App\\Models\\User', 21),
(16, 'App\\Models\\User', 21),
(12, 'App\\Models\\User', 22),
(13, 'App\\Models\\User', 22),
(14, 'App\\Models\\User', 22),
(12, 'App\\Models\\User', 25),
(13, 'App\\Models\\User', 25),
(16, 'App\\Models\\User', 25),
(12, 'App\\Models\\User', 26),
(14, 'App\\Models\\User', 26);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint UNSIGNED NOT NULL,
  `model_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(10, 'App\\Models\\User', 1),
(12, 'App\\Models\\User', 2),
(12, 'App\\Models\\User', 3),
(10, 'App\\Models\\User', 20),
(12, 'App\\Models\\User', 21),
(12, 'App\\Models\\User', 22),
(12, 'App\\Models\\User', 24),
(12, 'App\\Models\\User', 25),
(12, 'App\\Models\\User', 26);

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(12, 'create leads', 'web', '2021-03-13 07:23:09', '2021-03-13 07:23:09'),
(13, 'edit leads', 'web', '2021-03-13 07:23:09', '2021-03-13 07:23:09'),
(14, 'assign leads', 'web', '2021-03-13 07:23:09', '2021-03-13 07:23:09'),
(15, 'delele leads', 'web', '2021-03-13 07:23:09', '2021-03-13 07:23:09'),
(16, 'view leads', 'web', '2021-03-13 07:25:00', '2021-03-13 07:25:00');

-- --------------------------------------------------------

--
-- Table structure for table `plans`
--

CREATE TABLE `plans` (
  `id` int NOT NULL,
  `name` varchar(100) NOT NULL,
  `leads_limit` int NOT NULL,
  `users_limit` int NOT NULL,
  `status` int NOT NULL DEFAULT '1',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `plans`
--

INSERT INTO `plans` (`id`, `name`, `leads_limit`, `users_limit`, `status`, `created_at`) VALUES
(1, 'gold', 1000, 10, 1, '2021-03-20 05:28:20');

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE `profiles` (
  `id` int NOT NULL,
  `uid` int NOT NULL,
  `ucid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `plan_id` int DEFAULT NULL,
  `target` int DEFAULT NULL,
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `avatar` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'default',
  `display_name` varchar(100) DEFAULT NULL,
  `company_logo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT 'logo-default.jpg',
  `company_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `first_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `last_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `phone` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `dob` date DEFAULT NULL,
  `website` varchar(100) DEFAULT NULL,
  `address` tinytext NOT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `country` varchar(100) NOT NULL,
  `status` int NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`id`, `uid`, `ucid`, `plan_id`, `target`, `email`, `avatar`, `display_name`, `company_logo`, `company_name`, `first_name`, `last_name`, `phone`, `dob`, `website`, `address`, `city`, `state`, `country`, `status`, `created_at`) VALUES
(1, 1, 'A2IT-175E-8136', 1, NULL, 'info@a2itsoft.com', 'default', 'A2IT', 'logo-default.jpg', 'A2IT', 'A2IT', 'Ltd.', '9872551010', NULL, NULL, 'C-124, LTOP, Industrial Area, Phase 8 Mohali', 'mohali', 'punjab', 'india', 1, '2021-03-22 13:45:45'),
(2, 2, 'A2IT-175E-8136', 1, NULL, 'hr@a2itsoft.com', 'default', NULL, 'logo-default.jpg', NULL, 'simranjit', 'kaur', '7415151523', NULL, NULL, 'C-124, LTOP, Industrial Area, Phase 8 Mohali', 'mohali', 'Punjab', 'India', 1, '2021-03-22 13:51:52'),
(3, 3, 'A2IT-175E-8136', 1, NULL, 'gurjeet@a2itsoft.com', 'default', NULL, 'logo-default.jpg', NULL, 'gurjeet', 'kaur', '7814141400', NULL, NULL, 'C-124, LTOP, Industrial Area, Phase 8 Mohali', 'mohali', 'Punjab', 'India', 1, '2021-03-22 13:58:35');

-- --------------------------------------------------------

--
-- Table structure for table `quick_leads`
--

CREATE TABLE `quick_leads` (
  `id` int NOT NULL,
  `ucid` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `phone` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `inquiry_for` text NOT NULL,
  `price_quoted` int DEFAULT NULL,
  `status` int NOT NULL DEFAULT '1',
  `created_by` int NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `quick_leads`
--

INSERT INTO `quick_leads` (`id`, `ucid`, `name`, `phone`, `email`, `inquiry_for`, `price_quoted`, `status`, `created_by`, `updated_at`) VALUES
(6, 'A2IT-175E-8136', 'sandeep sarai', '9872570890', 'macgadger@gmail.com', 'Java and phython', 15000, 1, 1, '2021-03-23 04:18:28');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(10, 'admin', 'web', '2021-03-13 07:23:09', '2021-03-13 07:23:09'),
(12, 'agent', 'web', '2021-03-13 07:25:00', '2021-03-13 07:25:00');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint UNSIGNED NOT NULL,
  `role_id` bigint UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(12, 10),
(13, 10),
(14, 10),
(15, 10),
(16, 10),
(12, 12),
(16, 12);

-- --------------------------------------------------------

--
-- Table structure for table `sources`
--

CREATE TABLE `sources` (
  `id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `status` int NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sources`
--

INSERT INTO `sources` (`id`, `name`, `slug`, `status`, `created_at`) VALUES
(1, 'call', 'call', 1, '2021-03-14 06:38:10'),
(2, 'email', 'email', 1, '2021-03-14 06:38:10'),
(3, 'website', 'website', 1, '2021-03-14 06:38:10'),
(4, 'social network', 'social-network', 1, '2021-03-14 06:38:10'),
(5, 'newspaper', 'newspaper', 0, '2021-03-14 06:38:10'),
(6, 'adverstising', 'advertising', 1, '2021-03-14 06:38:10'),
(13, 'sulekha', 'sulekha', 1, '2021-03-19 03:40:51');

-- --------------------------------------------------------

--
-- Table structure for table `timeline`
--

CREATE TABLE `timeline` (
  `id` int NOT NULL,
  `ucid` varchar(100) NOT NULL,
  `lead_id` int NOT NULL,
  `next_followup` datetime NOT NULL,
  `followup_type` varchar(100) NOT NULL,
  `notes` text NOT NULL,
  `status` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'created',
  `comment` text,
  `created_by` int NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `trial_request`
--

CREATE TABLE `trial_request` (
  `id` int NOT NULL,
  `name` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `status` int DEFAULT '1',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `trial_request`
--

INSERT INTO `trial_request` (`id`, `name`, `phone`, `email`, `status`, `created_at`) VALUES
(2, 'Deepika', '09814000555', 'deepika.mca88@gamil.com', 1, '2021-03-23 10:17:24'),
(3, 'Manpreet Singh', '07696200218', 'macgadger@gmail.com', 1, '2021-03-23 10:18:12');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int NOT NULL,
  `ucid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `status` int NOT NULL DEFAULT '1',
  `last_seen` timestamp NULL DEFAULT NULL,
  `last_login_ip` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ucid`, `email`, `password`, `status`, `last_seen`, `last_login_ip`, `created_at`) VALUES
(1, 'A2IT-175E-8136', 'info@a2itsoft.com', '$2y$10$qfIEK6foqW06Tzs78/sZYOBPKAf7e81PQWLrrWGz.9XqHp7iHIqhy', 1, '2021-03-23 10:26:32', '127.0.0.1', '2021-03-13 07:35:46'),
(2, 'A2IT-175E-8136', 'hr@a2itsoft.com', '$2y$10$/UW19e/ETcCCk22lc53XEufrSkX16j1nbUba3FknoLufQFRDG5uIu', 1, NULL, NULL, '2021-03-22 13:51:52'),
(3, 'A2IT-175E-8136', 'gurjeet@a2itsoft.com', '$2y$10$aZ.pT8e1wYRSungKXs90GuBsKtdq3QjMdZGWw3ESX0UG6CNELu4RW', 1, NULL, NULL, '2021-03-22 13:58:35');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leads`
--
ALTER TABLE `leads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lead_profiles`
--
ALTER TABLE `lead_profiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `masters`
--
ALTER TABLE `masters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_guard_name_unique` (`name`,`guard_name`);

--
-- Indexes for table `plans`
--
ALTER TABLE `plans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uid` (`uid`);

--
-- Indexes for table `quick_leads`
--
ALTER TABLE `quick_leads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_guard_name_unique` (`name`,`guard_name`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `sources`
--
ALTER TABLE `sources`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `timeline`
--
ALTER TABLE `timeline`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trial_request`
--
ALTER TABLE `trial_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `leads`
--
ALTER TABLE `leads`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;

--
-- AUTO_INCREMENT for table `lead_profiles`
--
ALTER TABLE `lead_profiles`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;

--
-- AUTO_INCREMENT for table `masters`
--
ALTER TABLE `masters`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `plans`
--
ALTER TABLE `plans`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `quick_leads`
--
ALTER TABLE `quick_leads`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `sources`
--
ALTER TABLE `sources`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `timeline`
--
ALTER TABLE `timeline`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `trial_request`
--
ALTER TABLE `trial_request`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
