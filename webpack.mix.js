const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/spa-bootstrap.js', 'public/js/spa-bootstrap.js')
    .js('resources/js/app.js', 'public/js')
    .sass('resources/sass/plugins.scss', 'public/css/plugins.css')
    .sass('resources/sass/styles.scss', 'public/css/styles.css')
    .sourceMaps();
