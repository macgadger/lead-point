<?php

use App\Models\TrialRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
*/

Route::get('/', function () {
    return view('frontend.pages.home');
})->name('home');

Route::get('/about', function () {
    return view('frontend.pages.about');
})->name('about');

Route::get('/features', function () {
    return view('frontend.pages.features');
})->name('features');

Route::get('/pricing', function () {
    return view('frontend.pages.pricing');
})->name('pricing');

Route::get('/contact', function () {
    return view('frontend.pages.contact');
})->name('contact');

Route::get('/book-a-trail', function () {
    return view('frontend.pages.trial');
})->name('trial');

Route::post('/book-trail', function (Request $request) {
    //
    $validator = Validator::make($request->all(),[
        'name'    => 'required',
        'phone'   => 'required',
        'email'   => 'required',
    ]);

    if($validator->fails()){
        return redirect()->back()->withErrors($validator)->withInput();
    }

    TrialRequest::create([
        'name' => $request->name,
        'email' => $request->email,
        'phone' => $request->phone,
    ]);

    return redirect()->back()->with('success','Thankyou showing Intrest in our free Trial. Our representive will call you back to assit with your trial.');
})->name('trial.book');
