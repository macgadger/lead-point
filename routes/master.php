<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Master Routes
|--------------------------------------------------------------------------
*/
Route::group(['middleware' => ['auth:master']],function()
{
    Route::get('dashboard', 'MasterController@index')->name('master.dashboard');
    Route::get('filter/{type}', 'MasterController@filter')->name('master.filter');
    Route::resources([
        'client'   => ClientsController::class,
        'plans'    => PlansController::class,
    ]);
});