<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| App Routes
|--------------------------------------------------------------------------
*/
Route::group(['middleware' => 'auth'], function()
{
    Route::get('/', 'AppController@index');
    Route::get('dashboard', 'AppController@dashboard')->name('dashboard');
    // Route::get('admin-settings', 'AppController@admin')->name('admin.settings');

    Route::group(['prefix'=>'reports','as'=>'reports.'], function(){
        Route::get('/','ReportController@index')->name('index');
        Route::get('generate','ReportController@generate')->name('generate');
    });

    Route::group(['prefix'=>'leads','as'=>'leads.'], function()
    {
        Route::match(['GET','POST'],'leads/{id}/followup' ,'LeadController@addFollowup')->name('followup');
        Route::match(['GET','POST'],'leads/{id}/timeline' ,'LeadController@leadTimeline')->name('timeline');
        Route::match(['GET','POST'],'leads/{id}/status'   ,'LeadController@leadStatus')->name('status');
        Route::post('leads/quick','LeadController@leadQuick')->name('quick');
        Route::get('leads/{id}/move','LeadController@moveQuick')->name('move');
        Route::get('sort-leads', 'LeadController@sortLeads')->name('sort');
    });

    Route::group(['prefix'=>'filter','as'=>'filters.'], function(){
        Route::get('/{type}/{role?}', 'FilterController@filter')->name('index');
    });
    
    Route::group(['prefix'=>'profile','as'=>'profile.'], function(){
        Route::get('{id}','ProfileController@show')->name('view');
        Route::post('update/{id}','ProfileController@update')->name('update');
        Route::post('update/{id}/password','ProfileController@password')->name('password');
        Route::post('employees/{id}/password','ProfileController@updateEmployeePassword')->name('employee.password');
        Route::get('employees/online','ProfileController@onlineEmployee')->name('employee.online');
        Route::get('employees/access/{id}/{type}','ProfileController@accessEmployee')->name('employee.access');
    });

    Route::group(['prefix'=>'messages','as'=>'messages.'], function(){
        Route::get('/', 'MessageController@index')->name('index');
        Route::get('new/{id?}', 'MessageController@create')->name('create');
        Route::post('send', 'MessageController@send')->name('send');
        Route::get('show/{id}', 'MessageController@show')->name('show');
    });

    Route::resources([
        'leads'     => LeadController::class,
        'calendar'  => CalendarController::class,
        'clients'   => ClientsController::class,
        'employee'  => UserController::class,
        'lead-sources' => SourceControler::class,
        'quick-leads'  => QuickLeadController::class,
    ]);

});
