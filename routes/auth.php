<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Authentication Routes
|------------------------------------------------------  --------------------
*/
Route::get('signin', 'AuthController@signin')->name('account.login');
Route::post('auth/login', 'AuthController@authenticate')->name('account.auth');
Route::get('signout','AuthController@signout')->name('account.logout');

Route::match(['get','post'],'reset-password/{id?}','AuthController@reset')->name('account.forgot');
Route::post('auth/reset', 'AuthController@resetPass')->name('account.reset');

Route::get('master', 'AuthController@masterSignin')->name('master.login');
Route::post('master/login', 'AuthController@masterAuthenticate')->name('master.auth');
Route::get('master/signout','AuthController@masterSignout')->name('master.logout');              